function [y] = oae_logsweep2(varargin)
%  LOGSWEEP create logsweep
%
%   y = logsweep(F1s, F1e, T);
%       F1s, F1e are starting and ending frequency vectors, respectively.
%       T is total sweep duration
%       
%   e.g.
%       y = logsweep(1000,2000,2);               % log sweep
%       y = logsweep([1000 2000],[2000 4000],2); % 2-primaries (DPOAE) log sweep
%
%


% if nargin ~= 3 | mod(nargin,2) == 0,   % not sure what the logic is here
%     error('myApp:argChk', 'Wrong number of input arguments.');
% end
if length(varargin{1}) ~= length(varargin{2}),
    error('myApp:argChk', 'Vectors must be the same lengths.');
end

% defaults
%fs = 44100;
ramp = 0.001;

for i=1:nargin,
    if strcmp(varargin{i},'fs'),
        fs = varargin{i+1};
    end
end

F1s = varargin{1};
F1e = varargin{2};
T = varargin{3};
fs = varargin{4};
% time vector
t = 0:1/fs:T;

%create log sweep
y = zeros(1, length(t));

for i=1:length(F1s),
    f1 = F1s(i);
    f2 = F1e(i);
    gamma = log(f2/f1)/T;
    Phi0 = 2*pi*f1/gamma;
    %phi1 = ( Phi0 .* (exp(gamma .* t)-1) );
    phi1 = ( Phi0 .* (exp(gamma .* t)-1) );
    %y(i,:) = y(i,:)-sin(phi1-1.85); %initial phase -1.85 used to match RecordAppX 
    y = y+cos(phi1);
end


% % prepare ramp for stimulus
% n = length(y);                          % number of samples
% dr = ramp;                              % ramp time
% nr = floor(fs * dr);
% ramp = sin(linspace(0, pi/2, nr)).^2;
% ramp = [ramp, ones(1, n - nr * 2), fliplr(ramp)]';
% 
% y = y.*ramp;

