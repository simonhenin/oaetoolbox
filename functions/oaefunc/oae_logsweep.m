function [y, phi] = oae_logsweep(varargin)
%  LOGSWEEP create logsweep
%
%   y = logsweep(F1s, F1e, T, fs);
%       F1s, F1e are starting and ending frequency vectors, respectively.
%       T is total sweep duration
%       
%   e.g.
%       y = logsweep(1000,2000,2, 44100);               % log sweep
%       y = logsweep([1000 2000],[2000 4000],2,44100); % 2-primaries (DPOAE) log sweep
%
%


% if nargin ~= 3 | mod(nargin,2) == 0,   % not sure what the logic is here
%     error('myApp:argChk', 'Wrong number of input arguments.');
% end
if length(varargin{1}) ~= length(varargin{2}),
    error('myApp:argChk', 'Vectors must be the same lengths.');
end


for i=1:nargin,
    if strcmp(varargin{i},'fs'),
        fs = varargin{i+1};
    end
end

F1s = varargin{1};
F1e = varargin{2};
T = varargin{3};
fs = varargin{4};
% time vector
t = 0:1/fs:T;

%create log sweep
y = zeros(length(F1s), length(t));

for i=1:length(F1s),
    f1 = F1s(i);
    f2 = F1e(i);
    gamma = log(f2/f1)/T;
    Phi0 = 2*pi*f1/gamma;
    phi(i,:) = ( Phi0 .* (exp(gamma .* t)-1) );
    %phi1 = ( Phi0 .* (exp(gamma .* t)) );
    %phi1 = Phi0 * (f2/f1).^(t/T);
    y(i,:) = cos(phi(i,:));
end


