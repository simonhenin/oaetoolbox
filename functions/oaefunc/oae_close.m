function OAE = oae_close(OAE, SEQUENCE)

oae_log('User close request...attmepting to shutdown');

OAE(SEQUENCE).playback = false;
clear all; close all;
