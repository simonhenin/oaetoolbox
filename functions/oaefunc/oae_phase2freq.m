function f = oae_phase2freq(phi, fs)
% f = oae_phase2freq(phi, fs)
%   convert instantaneous phase to frequency

% 7/19/18 - SH, added function
f = diff(phi*fs)./(2*pi);