function OAE = oae_artifactrejection(OAE, SEQUENCE)
% Analyze button callback

% check for valid AR callback function
if isempty(OAE(SEQUENCE).artifactrejectionCallback) || strcmp(OAE(SEQUENCE).artifactrejectionCallback,''),
    oae_error('No artifact rejection module selected');
end

%autosave
if OAE(SEQUENCE).autosave,
    save(OAE(SEQUENCE).filename, 'OAE');
end
% setup a new analysis object for this new dataset
OAE(SEQUENCE) = OAE(SEQUENCE).newDataset();

% run an analysis
OAE(SEQUENCE) = OAE(SEQUENCE).runArtifactRejection();

% update the dropdowns
oae_update_datasets(OAE, SEQUENCE);
oae_update_analyses(OAE, SEQUENCE);

% % call the cmd to reset variables
% module = findobj('tag', 'analysis.parameters');
% edit = findobj(module, 'style', 'edit');
% cmd = get(edit(1), 'callback');
% eval('base', cmd);

end