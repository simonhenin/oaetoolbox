function oae_measure_system_delay(hObject, event, obj, handle)


T = 0.5;
fs = obj.fs;
OutputChannels = obj.OutputChannels;
InputChannels = obj.InputChannels;

if (isempty(obj.fs) || isempty(OutputChannels) || isempty(InputChannels) ),
    oae_error('Please configure your device!');
else
    f = oae_make_dialog('small');
    pos = get(f, 'position'); w = 280; h = 60;
    %uicontrol(f, 'style', 'text', 'units', 'pixels', 'position', [(pos(3)-w)/2 (pos(4)-h)/2 w h], 'string', 'Please place your probe into a cavity.');
    oae_uicomponent(f, 'style', 'text', 'units', 'normalized', 'horizontalalignment', 'center', 'position', [0.1 0.25 0.9 0.65], 'string', 'Steps: 1) Set up your "System Calibration" module, 2) Place the OAE probe in a 2cc cavity or cutoff syringe and press "Begin",  3) Please measure your system delay each day');
    w = 180; h = 20;
    %uicontrol(f, 'style', 'pushbutton', 'units', 'pixels', 'string', 'Begin', 'position', [(pos(3)-w)/2 30 w h], 'callback', {@estimate_delay, obj, handle, f});
    oae_uicomponent(f, 'style', 'pushbutton', 'units', 'normalized', 'string', 'Begin', 'position', [0.3 0.05 0.4 0.2], 'callback', {@estimate_delay, obj, handle, f});
end

end



function estimate_delay(hObject, event, obj, handle, f)

set(hObject, 'enable', 'off', 'string', 'Running...');
pause(0.01);
T = 0.5;
fs = obj.fs;
OutputChannels = obj.OutputChannels;
InputChannels = obj.InputChannels;
t=0:1/fs:0.75; y = zeros(1, length(t));

%put in three arbitrary clicks
tms = 0.045;
tc = 0:1/fs:tms;
c1 = ramp(chirp(tc, 8000, tms, 7000,'logarithmic'), fs, 0.005);
c2 = ramp(chirp(tc, 7000, tms, 8000,'logarithmic'), fs, 0.005);
c3 = ramp(chirp(tc, 1000, tms, 3000,'logarithmic'), fs, 0.005);
i = 100;
cl = length(c1);
y(i:i+cl-1) = c1;
i = i+cl+2053;
y(i:i+cl-1) = c2;
i = i+cl+4000;
y(i:i+cl-1) = c3;
y = 10^(75/20)*2e-5.*y;
y = [y zeros(1, 10000)];  % adding some extra padding after the chirp, to make sure the input buffer actually sees the stimulus.

h = oae_waitbar('string', 'Recording...');
% why are we running a calibration here? Not sure, commenting out for
% now. SH 1/14/14
%obj = obj.runCalibration();
obj.systemDelay = 0;
y2 = obj.makeCustomRecording(y', 1, 24, false, 0, []);  % SH, 5/16/17, changed applyCalibration flag to false. This shouldn't be applied during system delay check
y2 = mean(y2);
[C, lags] = xcorr(y,y2);
%[C, lags] = my_xcorr(y,y2);
loc = find(C==max(C));
est = abs(lags(loc)); % this value is correct, since est can theorectically be 0. Indexing of arrays starts at 1, so removing the delay is y(1+est), which accounts for the 1 sample difference.

figure;
subplot(211); hold on;
plot(y./max(y), '-r');
plot(y2./max(y2), 'k');
legend('played', 'recorded');
title(sprintf('Max. Correlation = %i samples', est)); axis tight;
subplot(212);
plot(lags./fs, C);
title('Sample cross-correlation'); axis tight;

close(h); % close the recording waitbar
close(f); % close the system delay window

% set system delay & run handle callback to set
handle = findobj(findobj('tag', handle), 'tag', 'systemDelay');
set(handle, 'string', num2str(est));
evalin('caller', get(handle, 'callback'));


end
