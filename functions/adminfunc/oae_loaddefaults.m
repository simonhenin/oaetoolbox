function modules = oae_loaddefaults(modules_loaded, autosave)
%
% * Load/configure defaults
% ----------------------------------------
%
% 07/15/16 - function added to handle to default housekeeping (SH)

if nargin < 2,
    autosave = 0;
end

sep = filesep; 
defaults = [];
userpth = oae_getuserdefaultspath;
systempth = oae_getsystempath;
icons_path = [systempth 'icons'];

defaults_file = [systempth oae_getuid '.defaults.mat'];
defaults_obj = [systempth oae_getuid '.obj.mat'];


if autosave,
    % call to save to file
    modules = modules_loaded;
    save(defaults_file, 'modules');
    assignin('base', 'modules', modules);
    return;
end

if ~exist(defaults_file, 'file')

    % copy default object + defaults
    if isdeployed,
        % copy files in function\adminfunc
        pth = [ctfroot sep 'functions' sep 'adminfunc' sep];   %C:\HearID\5_1_SwOAE\libswoae_mcr\functions\adminfunc\
    else
        pth = [pwd sep 'functions' sep 'adminfunc' sep];
    end
    
    % create a defaults file with loaded modules
    if ~exist('modules_loaded', 'var'),
        oae_error('Could not find modules to initialize the program'); % this shouldn't ever happen
    else
      modules = modules_loaded;  
    end
    save(defaults_file, 'modules');
    
%     % make a copy of the @controller class properties
%     system_obj = [pth 'system_obj.mat'];
%     copyfile(system_obj, defaults_obj, 'f');
    
%     system_cal = [pth 'system.calibration.parameters_defaults.mat'];
%     copyfile(system_cal, defaults_cal, 'f');
%     
%     system_ar = [pth 'system.artifactrejection.parameters_defaults.mat'];
%     copyfile(system_ar, defaults_ar, 'f');
%     
%     system_pcal = [pth 'system.probemicCalibration.parameters_defaults.mat'];
%     copyfile(system_pcal, defaults_pcal,'f');

%     system_icon = [pth 'icons'];   
%     copyfile(system_icon, icons_path, 'f');
%     load(defaults_file);
end
load(defaults_file);

% if modules are passed to this function, then reconcile the two sets 
if exist('modules_loaded', 'var'),
    % this is startup, so have OAE object load properties from default object file
    evalin('base', sprintf('OAE = OAE.loadSettings(''%s'');', defaults_obj));
    
    % check if any modules unloaded from system (e.g. folder deleted)
    deleted = [];
    for i=1:length(modules),
        % find module based on uid
        idx = find([modules_loaded.uid] == modules(i).uid);
        if isempty(idx),
            % module folder no longer exists, so delete from modules
            deleted(end+1) = i; 
        end
    end
    modules(deleted) = [];
    
    % check for any new modules loaded
    for i=1:length(modules_loaded),
        % find module based on uid
        idx = find([modules.uid] == modules_loaded(i).uid);
        if isempty(idx),
            if isfield(modules, 'parameters')
                modules_loaded(i).parameters = [];
            end
            modules(end+1) = modules_loaded(i);
        end
    end
    
    % resave defaults file
    save(defaults_file, 'modules');
end 

end