function oae_deletedefaults(reset_type)
%
% 07/18/16 removed parameter defaults (e.g. calibration, AR). This is now handled using
% modules structure which is saved in the defaults file -SH
% added a try catch to use a diffrent version of oaetoolbox.m that looks
% for p files in subdirectories instead of directories.
%
% SH, 04/14/17 -  added reset_type (1 - hard reset; 2 - soft reset (keep protocols))
sep = filesep; defaults = [];
if isdeployed,
    icons_path = [pth sep 'functions' sep 'defaults' sep 'icons'];
    pth = oae_getuserdefaultspath;
else
    pth = [pwd sep 'functions' sep 'defaults' sep];
    icons_path = [pth 'icons'];  %C:\HearID\5_1_SwOAE\libswoae_mcr\functions\defaults\
end
if ~exist(pth, 'dir')
    mkdir(pth);
end
defaults_file = [pth oae_getuid '.defaults.mat'];
defaults_obj = [pth oae_getuid '.obj.mat'];
%     defaults_cal = [pth oae_getuid '.calibration.parameters_defaults.mat'];
%     defaults_ar = [pth oae_getuid '.artifactrejection.parameters_defaults.mat'];


resp = oae_questdlg('Resetting your system will reset all previously configured default/calibration values. Are you sure you would like to proceed?', {'Yes', 'Cancel'});
if isempty(resp)|resp == 2,
    return;
end

% complete reset or keep protocols
if reset_type == 1,
    if exist(defaults_file, 'file'),
        delete(defaults_file);
    end
else
    % go through protocols and set all protocols as non-default
    load(defaults_file);
    for j=1:length(modules),
        if ~isempty(modules(j).protocols),
            [modules(j).protocols.default] = deal([]);
        end
    end
    save(defaults_file);
end


if exist(defaults_obj, 'file'),
    delete(defaults_obj);
end
%     if exist(defaults_cal, 'file'),
%         delete(defaults_cal);
%     end
%     if exist(defaults_ar, 'file'),
%         delete(defaults_ar);
%     end
try
    evalin('base', 'oaetoolbox');
catch
    evalin('base', 'run_oaetoolbox');
end

end