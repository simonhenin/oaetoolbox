function oae_addparameters(handle, uilist, uigeom, varargin)
%
% add uilist to paramters panel
% handle can either be actual uipanel handle, or paramters tag

% check is valid graphics handle, else try to detemine the handle from the
% tag

if ~ishandle(handle),
    handle_name = handle;
    handle = findobj('tag', handle);
else
    handle_name = get(handle, 'tag');
end


children = get(handle, 'children');
for i=1:length(children),
    if ~iscell(children(i)),
        delete(children(i));
    end
end
pause(0.1); drawnow;


% parameters panel setup
% ----------------------

% remove any empty entries
emptyCells = cellfun(@isempty,uigeom);
uigeom(emptyCells) = [];
emptyCells = cellfun(@isempty,uilist);
uilist(emptyCells) = [];

rowheight = 0.07; spacing = 0.01; border=0.015; topborder = 0.05; fontsize = 9;

for n=1:2:length(varargin),
    switch varargin{n}
        case 'rowheight'
            rowheight = varargin{n+1};
        case 'spacing'
            spacing = varargin{n+1};
        case 'topborder'
            topborder = varargin{n+1};
        case 'fontsize'
            fontsize = varargin{n+1};
    end
end

% do a rowheight check. Needs to be a minimum of 20px for proper display of
% dropdowns and edit boxes
set(handle, 'units', 'pixels');
pos = get(handle, 'position');
if pos(4)*rowheight < 15,
    rowheight = 20/pos(4);
end
set(handle, 'units', 'normalized');

% set up geometry
[max_size, max_index] = max(cellfun('size', uigeom, 2));
numelements = size([uigeom{:}],2);
geometry = ones(numelements,4);
counter = 1;
xpos = border; ypos = 1-topborder-border;
totalwidth = 1 - border- (spacing*max_size);
for i=1:length(uigeom),
    geom = uigeom{i};
    uis = {uilist{counter:counter+length(geom)-1}};
    
    % check for modified rowheight
    tmp_row_height = rowheight;
    for j=1:length(uis),
        ui = [uis{j}];
        pop = [];
        for k=1:length(ui),
            if ~iscell(ui{k})&~isa(ui{k}, 'function_handle')&strfind(ui{k}, 'rowheight'),
                rowheight = ui{k+1};
                pop = k:k+1;
            end
        end
        if ~isempty(pop)
            ui(pop) = [];
            uilist{counter+j-1} = ui;
        end
    end
    
    ypos = ypos-rowheight-spacing;
    for j=1:length(geom),
        geometry(counter,:) = [xpos ypos geom(j)*(totalwidth/max_size) rowheight];
        counter = counter+1;
        xpos = xpos + (geom(j)* (totalwidth/max_size)) + spacing;
    end
    xpos = border;
    rowheight = tmp_row_height;
end

block = get(get(handle, 'parent'), 'tag');
block_module_value = get(findobj('tag', [block '.modules']), 'value');
names = get(findobj('tag', [block '.modules']), 'string');
block_module_name = names{block_module_value};
defaults.module = block_module_name; setCallback = false;

type = get(handle, 'type'); counter = 1;
switch type,
    case 'uipanel'
        for i=1:length(uilist),
            list = uilist{i};
            geom = geometry(i,:);
            %h = uicontrol(handle, 'units', 'normalized', 'fontsize', fontsize, 'horizontalalignment', 'left', 'position', geom, list{:});
            h = oae_uicomponent(handle, 'position', geom, list{:});
            tag = get(h, 'tag');
            style = get(h, 'style');
            
            if ~isempty(tag) && isempty(regexp(tag, '.protocols')),
                val = NaN;
                userdata = [];
                %eval(['defaults.' tag '=' 'val' ';']);
                
                defaults.parameters(counter).value = val;
                defaults.parameters(counter).tag = tag;
                defaults.parameters(counter).usedata = userdata;
            end
            if strcmp(style, 'edit') && ~setCallback,
                % we're assuming edit boxes are user input with default
                % cmd. Therefore, we'll use this to set module's default
                % cmd for later use.
                callback = get(h, 'callback');
                if ~isempty(callback),
                    set(handle, 'userdata', callback);
                    setCallback = true;
                end
            end
        end
end
drawnow;

% TODO: need to fix

% % check/load defaults
% % ------------------
% defaults_file = [oae_getuserdefaultspath oae_getuid '.' handle_name '_defaults.mat'];
% if exist(defaults_file, 'file')
%     load(defaults_file);
%     if strcmp(defaults.module, block_module_name),
%         %defaults = rmfield(defaults, 'module');
%         params = defaults.parameters;
%         for i=1:length(params),
%             tag = params(i).tag;
%             if ~strcmp(tag, 'protocol'),
%                 val = params(i).value;
%                 userdata = params(i).userdata;
%                 if ~isnan(val),
%                     h = findobj('tag', tag);
%                     style = get(h, 'style');
%                     cmd = get(h, 'callback');
%                     switch style
%                         case {'popupmenu'}
%                             try
%                                 oae_selectpopupvalue(h,val, 'userdata', userdata);
%                             catch
%                                 oae_error('default configuration could not be loaded.');
%                                 break;
%                             end
%                         case {'checkbox'}
%                             try
%                                 set(h,'Value', val);
%                             catch
%                                 oae_error('default configuration could not be loaded.');
%                                 break;
%                             end
%                         case {'pushbutton'}
%                             try
%                                 %do nothing - no need to change the value
%                                 %of a pushbutton. 
%                             catch
%                                 oae_error('default configuration could not be loaded.');
%                                 break;
%                             end
%                         otherwise
%                             try
%                                 oae_setvalue(h,val, 'userdata', userdata);
%                             catch
%                                 oae_error('default configuration could not be loaded.');
%                                 break;
%                             end
%                     end
%                     try
%                         evalin('caller', cmd);  
%                     catch
%                         oae_error('default module settings could not be loaded!');
%                         break;
%                         %% todo: overwrite defaults?
%                     end
%                 end
%             end
%             
%         end
%     end
% else
%     %save(defaults_file, 'defaults');
% end






end