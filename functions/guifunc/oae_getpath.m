function pth = oae_getpath(folder)

if isdeployed
    pth = [ctfroot filesep 'functions' filesep folder filesep];
else
    stack = dbstack;
    sep = filesep;
    file = stack(3).file;
    parts = [explode(fileparts(which(file)), sep)];
    
    if isunix,
        pth = [sep implode(parts(1:end-2), sep) sep 'functions' sep folder sep];
    else
        pth = [implode(parts(1:end-2), sep) sep 'functions' sep folder sep];
    end
end


end
