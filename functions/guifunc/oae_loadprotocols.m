function oae_loadprotocols(handle, protocols)
%
% 
% Wrapper function to load protocols into dropdown menu specified in tag
% In addition, this function will check/create new protocols
%
% called in modules using oae_loadprotocols('tag_of_dropdown')

% check if override default protocol
% label = []; filename = []; protocols = [];
% for i=1:2:length(varargin),
%     switch lower(varargin{i}),
%         case 'label'
%             label = varargin{i+1};
%         case 'filename'
%             filename = varargin{i+1};
%         case 'protocols'
%             protocols = varargin{i+1};    
%     end
% end
if nargin == 1,
    protocols = [];
end

% add save new...to protocols
if isempty(protocols),
    protocols.label = '------';
    protocols(2).label = 'Save new...';
    protocols(2).default = 1;
%     protocols(2).module = handle;
else
    protocols(length(protocols)+1).label = '------';
    protocols(length(protocols)+1).label = 'Save new...';
    protocols(length(protocols)+1).label = 'Edit...';
%     for i=1:length(protocols),
%         protocols(i).module = handle;
%     end
end

%keep it in the userdata field
protocol_names = {protocols(:).label};
if length(protocols) <= 1,
    popup = findobj('tag', handle);
    children = get(get(popup, 'parent'), 'children');
    for i=1:length(children),
        if (children(i) ~= popup),
            style = get(children(i), 'style');
            ntag = get(children(i), 'tag');
            switch style,
                case {'edit' 'popupmenu'}
                    eval(['protocols.' ntag '=' '0' ';']);
            end
        end
    end
    protocols.default = 1;
    %protocols.module = handle;
end

default_idx = find([protocols.default] == 1);
protocol_names = {'Select a protocol' protocol_names{:}};
set(handle, 'string', protocol_names);
set(handle, 'userdata', protocols);
if isempty(default_idx),
    set(handle, 'value', 1);
else
    set(handle, 'value', default_idx+1);
end

% %load default protocol
% if ~isempty(label),
%     def = find(strcmp({protocols(:).label}, label));
%     protocol = protocols(def);
% else
%     
%     def = find([protocols(:).default]);
%     if ~isempty(def)
%         protocol = protocols(def);
%     end
% end
% fields = fieldnames(protocols);

% for j=1:length(fields),
%     value = getfield(protocol, fields{j});
%     if ~isempty(value),
%         h = findobj('tag', fields{j});
%         if ~isempty(h),
%             switch get(h, 'style'),
%                 case 'popupmenu'
%                     val = find(strcmp(get(h, 'string'), value));
%                     if isempty(val), val = 1; end
%                     set(h, 'value', val);
%                 otherwise
%                     set(h, 'string', value);
%             end
%         end
%     end
% end

% set value to default protocol & reset callback
%set(findobj('tag', tag), 'value', def);
set(handle, 'callback', @oae_update_protocol);
oae_update_protocol(handle, []);

end
