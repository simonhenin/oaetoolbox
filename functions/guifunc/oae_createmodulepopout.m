function oae_createmodulepopout(hObject, eventdata)

%get OAE object from the workspace
modules = evalin('base', 'modules');
tag = get(hObject, 'tag');
tags = explode(tag, '.');
tag = tags{1};
if isprop(hObject, 'Label'),
    name = get(hObject, 'Label');
else
    name = tag;
end
fig = oae_popout(tag, name);
oae_loadmodules(fig, modules, {tag});

end