function oae_update_protocol(hObject, eventdata)
% 10/5/12 Updated to handle protocol files. Added multiple checks and
% options for Unix, Windows, and Mac. 
val = get(hObject,'Value')-1;
protocols = get(hObject,'userdata');
menu = get(hObject, 'tag');

% probably need to make this work better
module_panel = get(get(hObject, 'parent'), 'parent');
module_uid = get(module_panel, 'userdata');
modules = oae_loaddefaults;
module_idx = find([modules.uid] == module_uid);

cmd = [];
openWindow = findobj('tag', 'procotoleditor');
if ~isempty(openWindow),
    delete(openWindow);
end

% save new protocol
% -----------------
if val,
    if strcmp(protocols(val).label, '------'),
        set(hObject, 'value', 1);
    elseif strcmp(protocols(val).label, 'Save new...'),
        disp('saving new protocol...');
        
        prompt = {'Enter protocol name:'}; dlg_title = ''; num_lines = 1; def = {'new protocol'};
        label = inputdlg(prompt,dlg_title,num_lines,def);
        if strcmp(label, '')                              % isempty handles cancel and '' handles no text entered
            oae_error('protocol name cannot be empty');
            return;
        elseif isempty(label) 
            return;
        end
     
        
        dummy = find(strcmp({protocols(:).label}, 'no saved protocols'));
        if ~isempty(dummy), protocols(dummy) = []; end
        % remove ----, Save new, Edit
        index = find(strcmp({protocols.label}, 'Select a protocol') | strcmp({protocols.label}, 'Save new...') | strcmp({protocols.label}, 'Edit...') | strcmp({protocols.label}, '------'));
        protocols(index) = [];
        
        nextProtocol = length(protocols) + 1;
        if ~isempty(protocols),
            [protocols.default] = deal(0);         % set all protocols to default 0
        else
            protocols = [];
        end
        protocols(nextProtocol).default = 1;   % sets the new protocol as default
        handle = get(hObject, 'parent');
        % find protocol values in parent
        % ------------------------------
        %fields = fieldnames(protocols);
        fields = get(handle, 'children');
        for j=1:length(fields),
            %h = findobj(handle, 'tag', fields{j});
            h = fields(j);
            name = get(h, 'tag');
            style = get(h, 'style');
            ctag = get(h, 'tag');
            if ~strcmp(style,'pushbutton') & ~strcmp(style,'text') & ~strcmp(ctag, menu),
                value = get(h, 'string');
                type = get(h, 'userdata');
                if strcmp(style, 'popupmenu'),
                    value = value(get(h, 'value'));
                    value = value{:};
                end
                if strcmp(type, 'int'),
                    value = str2double(value);
                end
                protocols = setfield(protocols, {nextProtocol}, name, value);
            end
        end
        
        protocols(nextProtocol).label = label{:};

        modules(module_idx).protocols = protocols;
        oae_loaddefaults(modules, 1); % autosave modules
        
        % reload protocols, but specify the new protocol, label, as "selected"
        % -----------------------------------------------------------
        oae_loadprotocols(hObject, protocols);
        
        % call the callback specified in userdata
        % ---------------------------------------
    elseif strcmp(protocols(val).label, 'Edit...'),
        % edit protocols
        set(hObject, 'value', 1);
        index = find(strcmp({protocols.label}, 'Save new...') | strcmp({protocols.label}, 'Edit...') | strcmp({protocols.label}, '------'));
        protocols(index) = [];
        h = figure('numbertitle', 'off','resize', 'off', 'Toolbar', 'none','Menubar', 'none', 'tag', 'procotoleditor', 'color', get(0, 'DefaultUicontrolBackgroundColor'));
        oae_uicomponent(h, 'style', 'listbox', 'string', {protocols(:).label}, 'position', [0.05 0.1 0.4 0.8], 'tag', 'selectedprotocol', 'min', 1, 'max', 1, 'userdata', protocols, 'callback', {@oae_edit_protocols, 'display'});
        oae_uicomponent(h, 'style', 'text', 'string', '', 'position', [0.48 0.1 0.3 0.8], 'enable', 'on', 'tag', 'protocolseditor_info');
        %uicontrol(h, 'style', 'pushbutton', 'string', 'Edit', 'position', [0.05 0.01 0.18 0.08], 'enable', 'off');
        oae_uicomponent(h, 'style', 'pushbutton', 'string', 'Delete', 'position', [0.05 0.01 0.4 0.08], 'callback', {@oae_edit_protocols, 'delete', module_idx}, 'userdata', hObject);
        
    else, % load the protocol
        protocol = protocols(val);
        handle = get(hObject, 'Parent');
        
        fields = fieldnames(protocol);
        for j=1:length(fields),
            value = getfield(protocol, fields{j});
            h = findobj('tag', fields{j});
            if ~isempty(h),
                style = get(h, 'style');
                switch style,
                    case 'popupmenu'
                        val = find(strcmp(get(h, 'string'), value));
                        if isempty(val), val = 1; end
                        set(h, 'value', val);
                    otherwise
                        set(h, 'string', value);
                end
                if isempty(cmd),
                    cmd = get(h, 'callback');
                end
            end
            
            if j == length(fields),
                hgfeval(get(h,'Callback'));
            end
        end
        % make this protocol the default
        val = get(hObject,'Value')-1;
        [protocols.default] = deal(0); % set them all to 0
        protocols(val).default = 1;
         % remove ----, Save new, Edit
        index = find(strcmp({protocols.label}, 'Select a protocol') | strcmp({protocols.label}, 'Save new...') | strcmp({protocols.label}, 'Edit...') | strcmp({protocols.label}, '------'));
        protocols(index) = [];
        modules(module_idx).protocols = protocols;
        oae_loaddefaults(modules, 1); % autosave modules
    end
    
    % evaluate protocol's callback
    % ----------------------------
    if ~isempty(cmd),
        evalin('base', cmd);
    end
end

end
