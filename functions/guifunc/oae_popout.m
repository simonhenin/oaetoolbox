function fig = oae_popout(tag, name)

%load oae_settings;
paperposition   = [18 180 800 432]; 
WINMINX         = 17;
WINMAXX         = 360;
WINYDEC			= 13;
NBLINES         = 16;
WINY		    = WINYDEC*NBLINES;
BORDERINT       = 4;
BORDEREXT       = 10;
%BGCOLOR         = [0.7020    0.7020    0.7020];
BGCOLOR         = [0.9    0.9    0.9];
COLOR           = [1 1 1];
FONTSIZE        = 8;
FONTNAME        = 'Arial';
SIZE            = [340 600];

set(0, 'Units', 'Pixels');
scrsz = get(0,'MonitorPositions');
scrsz = scrsz(1,:);

fig_tag = 'popout';
fig     = findobj('tag', fig_tag);

if isempty(fig),
    fig = figure('Units','pixels', ...
        'PaperPosition', paperposition, ...
        'PaperUnits','points', ...
        'numbertitle', 'off', ...
        'Units', 'Pixels', ...
        'Position', [scrsz(3)/2-SIZE(1)/2 scrsz(4)/2-SIZE(2)/2 SIZE(1) SIZE(2)], ...
        'Tag', fig_tag, ...
        'Toolbar', 'none', ...
        'Menubar', 'none', ...
        'visible', 'on', ...
        'CloseRequestFcn', @oae_close_window, ...
        'Userdata', {[] []});
else
    figure(fig);
    clf;
end
paramtag = [tag '.parameters'];
panel = uipanel('title', name, 'units', 'normalized', 'position', [0.01 0.01 0.98 0.98], 'tag', tag, 'bordertype', 'none', 'fontweight', 'bold');
uicontrol(panel, 'Units', 'normalized',  'style', 'popupmenu', 'string', {'Select'}, 'position', [0.01 0.85 0.5 0.1], 'tag', [tag '.modules'], 'callback', @oae_dropdown_callback);
uicontrol(panel, 'Units', 'normalized',  'style', 'pushbutton', 'string', {'Apply'}, 'position', [0.51 0.01 0.48 0.07], 'fontsize', 9, 'tag', 'save_button', 'callback', @oae_savedefaults, 'visible', 'off');
uipanel(panel, 'Title', 'Parameters', 'units', 'normalized', 'Position',[0.01 .1 0.98 .75], 'tag', paramtag, 'bordertype', 'none');

end