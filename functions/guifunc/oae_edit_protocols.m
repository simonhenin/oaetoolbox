function oae_edit_protocols(hObject, event, action, module_idx)


selected = get(findobj('tag', 'selectedprotocol'), 'value');
protocols = get(findobj('tag', 'selectedprotocol'), 'userdata');
modules = oae_loaddefaults;


if strcmp(action, 'delete'),
    
    protocols(selected) = [];
    modules(module_idx).protocols = protocols;
    oae_loaddefaults(modules, 1); % autosave modules
    set(findobj('tag', 'selectedprotocol'), 'userdata', protocols, 'string', {protocols(:).label}, 'value', 1);
    oae_loadprotocols(get(event.Source, 'userdata'), protocols); %origin protocol ui handle stored in delete buttons 'userdata' field

elseif strcmp(action, 'display'),
   str = sprintf('Protocol Information \n\n');
   protocol = protocols(selected);
   protocol = rmfield(protocol, 'default');
%    protocol = rmfield(protocol, 'filename');
%    protocol = rmfield(protocol, 'module');
   fields = fieldnames(protocol);
   for j=1:length(fields),
       value = getfield(protocol, fields{j});
       str = [str sprintf('%s: \t %s\n', fields{j}, value(:))];
   end
   set(findobj('tag', 'protocolseditor_info'), 'string', str);     
   
end

end