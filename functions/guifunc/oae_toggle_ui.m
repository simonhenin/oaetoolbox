function oae_toggle_ui(handle, varargin)

children = findall(handle, 'type', 'uicontrol');
states = {'off' 'on'};
if nargin < 2,
    s = [];
else
    s = varargin{1}+1;
end

for i=1:length(children),
    child = children(i);
    
    type = get(child, 'style');
    switch type,
        case {'edit', 'pushbutton', 'popupmenu'}
            if isempty(s),
                en = get(child, 'enable');
                if strcmp(en, 'on'), s = 1; end
            end
            set(child, 'enable', states{s});
    end        
end

