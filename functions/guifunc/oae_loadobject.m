function OAE = oae_loadobject(OAE, SEQUENCE, figure_handle)


seq_textbox = findobj(figure_handle, 'tag', 'sequence');

sequences = {};
for i=1:size(OAE,2),
    OAE(i).fig = figure_handle;
    if (OAE(i).saveToFile && isempty(OAE(i).dataFileNames)), 
        % Something went wrong. We don't have any filenames to load, so
        % we'll need to abort.
        continue; 
    end
    protocol = '';
    if isfield(OAE(i), 'label') & ~isempty(OAE(i).label),
        protocol = sprintf('%s', OAE(i).label);
    elseif ~isempty(OAE(i).protocol)&~strcmpi(OAE(i).protocol, 'select a protocol'),
        protocol = sprintf('%s (%s)', OAE(i).protocol, OAE(i).paradigmLabel);
    else
        protocol = sprintf('%s', OAE(i).paradigmLabel);
    end
    sequences = {sequences{:}, sprintf('%s', protocol)};
end

if isempty(sequences),
    oae_error('No data to load');
    close all;
else, 
    set(seq_textbox, 'string', sequences);
    OAE = oae_update_analysis_sequence(OAE, SEQUENCE, SEQUENCE);
end

end