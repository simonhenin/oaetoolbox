function figh = oae_makegui()

load oae_settings;

SIZE(2) = SIZE(2)-BORDERBOTTOM;
set(0, 'Units', 'Pixels');
scrsz = get(0,'ScreenSize');
scrsz = scrsz(1,:); % make sure it only appears on primary monitor

% javax.swing.UIManager.setLookAndFeel('com.jgoodies.looks.plastic.Plastic3DLookAndFeel');
% javax.swing.UIManager.setLookAndFeel('com.apple.laf.AquaLookAndFeel');
% javax.swing.UIManager.setLookAndFeel('javax.swing.plaf.nimbus.NimbusLookAndFeel');
% javax.swing.UIManager.setLookAndFeel('com.sun.java.swing.plaf.motif.MotifLookAndFeel');

% BGCOLOR = get(0, 'defaultFigureColor');
% % set(0,'DefaultFigureColor',BGCOLOR);
set(0,'DefaultUipanelBackgroundColor',get(0,'DefaultFigureColor'));
set(0,'DefaultUicontrolBackgroundColor',get(0,'DefaultFigureColor'));
% % set(0,'DefaultUipanelHighlightColor',HIGHLIGHTCOLOR);
% % set(0,'DefaultUipanelShadowColor',SHADOWCOLOR);
set(0,'DefaultUicontrolUnits','Normalized');
set(0,'DefaultUicontrolHorizontalAlignment','Left');
set(0,'DefaultUicontrolFontsize',FONTSIZE);


figh = figure('Units','pixels', ...
    'PaperPosition',paperposition, ...
    'PaperUnits','points', ...
    'name', [ 'OAEToolBox v' prgversion ], ...
    'numbertitle', 'off', ...
    'resize', 'on', ...
    'Units', 'Pixels', ...
    'Position', [(scrsz(3)-SIZE(1))/2 (scrsz(4)-SIZE(2))/2 SIZE(1) SIZE(2)], ...%'PaperPositionMode', 'auto',...
    'Tag','OAEToolbox', ...
    'Toolbar', 'none', ...
    'Menubar', 'none', ...
    'visible', 'on', ...
    'Userdata', {[] []},...
    'CloseRequestFcn', 'oae_quit');


try,
    set(figh, 'NextPlot','new');
catch, end;


% % add background image
ha = axes('units','normalized','position',[-0.01 0.02 0.15 0.15*1.75]);
[A, map, alpha] = imread([oae_getsystempath() 'icons' filesep 'CUNY-GC-logo.png']);
h = imshow(A, map, 'Parent', ha,'InitialMagnification',100);
set(h, 'AlphaData', alpha);

% Menus
menu_file = uimenu('Label', 'File', 'tag', 'file');
%menu_edit = uimenu('Label', 'Edit', 'tag', 'edit');
menu_edit = uimenu('Label', 'Edit', 'tag', 'daq_menu');
menu_daq = uimenu('Label', 'Input/Output', 'tag', 'daq_menu');
% menu_analyze = uimenu('Label', 'Analysis', 'tag', 'analyze');
menu_calibration = uimenu('Label', 'Calibration', 'tag', 'calibration_menu');
menu_tools = uimenu('Label', 'Tools', 'tag', 'tools');
menu_help = uimenu('Label', 'Help', 'tag', 'help');
menu_help_modules = uimenu(menu_help,'Label','Modules', 'tag', 'menu_help_modules'); 


%menu_sequence = oae_addmenuitem(menu_file, 'Sequences', '', 'on');
oae_addmenuitem(menu_file, 'Load sequence...', 'oae_loadsequence(OAE, SEQUENCE);', 'on');
oae_addmenuitem(menu_file, 'Save sequence...', 'OAE = oae_savesequence(OAE, SEQUENCE);', 'on');
reset_menu = oae_addmenuitem(menu_help, 'Restore defaults', '', 'on');
oae_addmenuitem(reset_menu, 'Reset to default system', 'oae_deletedefaults(1);', 'on'); % hard reset, pass 1
oae_addmenuitem(reset_menu, 'Reset, but keep protocols', 'oae_deletedefaults(2);', 'on');   % soft reset, pass 2

oae_addmenuitem(menu_tools, 'Extras', @oae_createmodulepopout, 'on', 'tag', 'extras');
if ~isdeployed,
    oae_addmenuitem(menu_file, 'Load analysis...', 'OAE = oae_loadanalysisobject(OAE(SEQUENCE).datapath);', 'on');
end
% oae_addmenuitem(menu_analyze, 'Artifact Rejection', @oae_createmodulepopout, 'on', 'tag', 'artifactrejection');
oae_addmenuitem(menu_calibration, 'Settings', @oae_createmodulepopout, 'on', 'tag', 'calibration');
%oae_addmenuitem(menu_calibration, 'FPL Cavity Calibration', @oae_createmodulepopout, 'on', 'tag', 'calibration');
%% Quit
oae_addmenuitem(menu_file, 'Quit', 'oae_quit', 'on', 'Accelerator', 'W');


oae_addmenuitem(menu_daq, 'Configure', @oae_createmodulepopout, 'on', 'tag', 'daq');
oae_addmenuitem(menu_edit, 'Configuration', 'OAE = oae_otb_options(OAE, SEQUENCE);', 'on', 'tag', 'otb_config');

vpad = 0.02; % TODO: make this part of oae_settings defaults

%% Panels Setup
% basic subject configuration, (make into a module? Probably not needed,
% just basic stuff)
INFO = uipanel('title','Patient Information','units', 'normalized', 'position', [0.01 1-0.5+vpad 0.32 0.5-vpad], 'tag', 'pt_info', 'fontsize', UIPANEL_FONT_SIZE);
uicontrol(INFO, 'style', 'text', 'units', 'normalized', 'string', 'Patient ID: ', 'fontweight', 'normal', 'position', [0.01 0.85 0.2 0.1], 'callback', 'OAE = oae_update_patient_info(OAE, SEQUENCE);', 'fontsize', FONTSIZE);
uicontrol(INFO, 'style', 'edit', 'units', 'normalized', 'string', '', 'fontweight', 'normal', 'position', [0.25 0.87 0.3 0.08], 'tag', 'pt_ID', 'callback', 'OAE = oae_update_patient_info(OAE, SEQUENCE);', 'fontsize', FONTSIZE);
uicontrol(INFO, 'style', 'text', 'units', 'normalized', 'string', 'Test Ear:', 'fontweight', 'normal', 'position', [0.01 0.72 0.2 0.1], 'callback', 'OAE = oae_update_patient_info(OAE, SEQUENCE);', 'fontsize', FONTSIZE);
uicontrol(INFO, 'style', 'popupmenu', 'units', 'normalized', 'string', {'Left', 'Right'}, 'value', 1, 'fontweight', 'normal', 'position', [0.25 0.72 0.3 0.1], 'tag', 'pt_ear', 'callback', 'OAE = oae_update_patient_info(OAE, SEQUENCE);', 'fontsize', FONTSIZE);
uicontrol(INFO, 'style', 'text', 'units', 'normalized', 'string', 'DOB:', 'fontweight', 'normal', 'position', [0.01 0.59 0.2 0.1], 'callback', 'OAE = oae_update_patient_info(OAE, SEQUENCE);', 'fontsize', FONTSIZE);
uicontrol(INFO, 'style', 'edit', 'units', 'normalized', 'string', '', 'value', 1, 'fontweight', 'normal', 'position', [0.25 0.61 0.3 0.08], 'tag', 'pt_DOB', 'callback', 'OAE = oae_update_patient_info(OAE, SEQUENCE);', 'fontsize', FONTSIZE);
uicontrol(INFO, 'style', 'text', 'units', 'normalized', 'string', 'Sex:', 'fontweight', 'normal', 'position', [0.01 0.46 0.2 0.1], 'callback', 'OAE = oae_update_patient_info(OAE, SEQUENCE);', 'fontsize', FONTSIZE);
uicontrol(INFO, 'style', 'popupmenu', 'units', 'normalized', 'string', {'Male', 'Female'}, 'value', 1, 'fontweight', 'normal', 'position', [0.25 0.46 0.3 0.1], 'tag', 'pt_sex', 'callback', 'OAE = oae_update_patient_info(OAE, SEQUENCE);', 'fontsize', FONTSIZE);

uicontrol(INFO, 'style', 'text', 'units', 'normalized', 'string', 'Notes:', 'fontweight', 'normal', 'position', [0.01 0.35 0.2 0.1]);
uicontrol(INFO, 'style', 'edit', 'units', 'normalized', 'string', '', 'fontweight', 'normal', 'max', 10, 'position', [0.01 0.1 0.98 0.25], 'string', '', 'tag', 'pt_notes', 'callback', 'OAE = oae_update_patient_info(OAE, SEQUENCE);');

% Panel to enable modification of current I/O configuration
IO = uipanel('title','Audio Configuration','units', 'normalized', 'position', [0.01 1-0.69+vpad 0.32 0.25-vpad], 'tag', 'daq_config', 'fontsize', UIPANEL_FONT_SIZE);
uicontrol(IO, 'style', 'text', 'units', 'normalized', 'string', 'Driver: ', 'fontweight', 'bold', 'position', [0.01 0.72 0.2 0.25]);
uicontrol(IO, 'style', 'text', 'units', 'normalized', 'tag', 'daq_config.driver', 'string', '', 'position', [0.17 0.72 0.65 0.25]);
uicontrol(IO, 'style', 'text', 'units', 'normalized', 'string', 'Device:', 'fontweight', 'bold', 'visible', 'off','position', [0.01 0.55 0.2 0.25]);
uicontrol(IO, 'style', 'text', 'units', 'normalized', 'tag', 'daq_config.device_name', 'string', '', 'visible', 'off','position', [0.17 0.55 0.65 0.25]);
uicontrol(IO, 'style', 'text', 'units', 'normalized', 'string', 'Input Channels: ', 'fontweight', 'bold', 'visible', 'off','position', [0.01 0.32 0.4 0.25]);
uicontrol(IO, 'style', 'popupmenu', 'units', 'normalized', 'tag', 'daq_config.input_channels', 'string', {''}, 'position', [0.4 0.33 0.2 0.25], 'visible', 'off','callback', 'OAE = oae_update_io_config(OAE, SEQUENCE, 1);');
uicontrol(IO, 'style', 'text', 'units', 'normalized', 'string', 'Output Channels: ', 'fontweight', 'bold', 'visible', 'off','position', [0.01 0.05 0.4 0.25]);
uicontrol(IO, 'style', 'popupmenu', 'units', 'normalized', 'tag', 'daq_config.output_channels', 'string', {''}, 'position', [0.4 0.05 0.2 0.25], 'visible', 'off','callback', 'OAE = oae_update_io_config(OAE, SEQUENCE, 1);');
uicontrol(IO, 'style', 'pushbutton', 'units', 'normalized', 'string', 'Configure Device', 'fontweight', 'normal', 'visible', 'on', 'position', [0.64 0.06 0.35 0.26], 'callback', @oae_createmodulepopout, 'tag', 'daq');

pos = get(IO, 'position');
pd = uipanel('title','Paradigm', 'units', 'normalized', 'position', [0.34 1-0.999+vpad 0.32 0.999-vpad], 'tag', 'paradigm', 'fontsize', UIPANEL_FONT_SIZE);
pdd = oae_uicomponent(pd, 'Units', 'normalized',  'style', 'popupmenu', 'string', {'Select a paradigm'}, 'position', [0.02 0.85 0.65 0.1],'tag', 'paradigm.modules', 'callback', @oae_dropdown_callback, 'KeyPressFcn', '', 'KeyReleaseFcn', '');
oae_uicomponent(pd, 'Units', 'normalized',  'style', 'pushbutton', 'string', {'Save as Default'}, 'position', [0.69 0.905 0.29 0.05], 'fontsize', 10, 'tag', 'save_button', 'callback', @oae_savedefaults);
pdp = uipanel(pd, 'Title', 'Parameters', 'Position',[0.02 .01 0.96 .8], 'tag', 'paradigm.parameters', 'bordertype', 'none');

% sequence panel
pos = get(pd, 'position');
seq = uipanel('title', 'Sequence', 'units', 'normalized', 'position', [0.67 1-0.999+vpad 0.32 0.999-vpad], 'tag', 'sequence', 'fontsize', UIPANEL_FONT_SIZE);
seq_textbox = oae_uicomponent(seq, 'style', 'text', 'units', 'normalized', 'position', [0.01 0.05 0.94 0.9], 'String', 'Saved Sequences:', 'horizontalalignment', 'left', 'tag', 'sequence.list');

%start button
cmd = '[OAE SEQUENCE] = oae_start(OAE,SEQUENCE);';
pos = get(seq, 'position');
oae_uicomponent(figh, 'style', 'pushbutton', 'String', 'Start', 'tag', 'start_sequences', 'fontsize', 12, 'fontweight', 'bold', 'units', 'normalized', 'position', [0.68 0.05 0.3 0.065], 'callback', cmd, 'enable', 'off');
set(figh, 'userdata', {[] []}, 'visible', 'on');