function oae_callmodule(handle)
%
% call a module's default callback
%

if ~ishandle(handle),
    handle_name = handle;
    handle = findobj('tag', handle);
else
    handle_name = get(handle, 'tag');
end

if ~isempty(handle),
    % let's get the callback
    callback = get(handle, 'userdata');
    try
        evalin('base', callback);
    catch
        disp('no default module callback');
    end
end

end