function oae_loadsequence(OAE, SEQUENCE)


[filename,pathname] = uigetfile('*.seq','Select the seq-file');
if isequal(filename,0) || isequal(pathname,0)
   disp('User selected Cancel...');
else
   %first we need to get the figure handle before we overwrite the object
   handle = OAE(1).fig;
   load(fullfile(pathname,filename), '-mat');
   if isempty(OAE),
       oae_error({'Error loading sequence file.', 'file appears to be empty.'});
   else
    SEQUENCE = size(OAE,2);
    for j=1:size(OAE,2),
        OAE(j).fig = handle;
    end
    %OAE(SEQUENCE) = controller();                    %sequence is always one ahead
    assignin('base', 'OAE', OAE);
    assignin('base', 'SEQUENCE', SEQUENCE);
    evalin('base', 'oae_update_sequence();');
   end
end


