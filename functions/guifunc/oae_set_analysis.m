function OAE = oae_set_analysis(OAE, SEQUENCE)

h = findobj('tag', 'settings.window');
settings = findobj(h, {'style', 'edit', '-or', 'style', 'checkbox', '-or', 'style', 'popupmenu'});
current_analysis_index = [];

for i=1:length(settings),
    %style = get(settings(i), 'style');
   tag = get(settings(i), 'tag');
   val = oae_getvalue(h, tag, 'string'); 
   if strcmp(tag, 'Xlimits'),
       val = str2double(strsplit(val));
   end
   if isnan(val), val = []; end
   
   current_analysis = OAE(SEQUENCE).currentIndex; %[dataset index]
   %eval(['OAE(SEQUENCE).analysisObj.' tag ' = ''' val ''';']);
%    evalin('base', ['OAE(SEQUENCE).analysisObj.' tag ' = ''' val ''';']);
    OAE(SEQUENCE).datasets(current_analysis(1)).analysisObj(current_analysis(2)).(tag) = val;
end

% save setting for next time
user_defaults_folder = oae_getuserdefaultspath;
%props = OAE(SEQUENCE).analysisObj(OAE(SEQUENCE).analysisIndex).saveoptions();
props = OAE(SEQUENCE).analysisObj(OAE(SEQUENCE).current_dataset).saveoptions();
%props = OAE(SEQUENCE).analysisObj(OAE(SEQUENCE)).saveoptions();
save([user_defaults_folder oae_getuid '_analysisObj.mat'], 'props'); 

% update the plots
if ~isempty(current_analysis)
    evalin('base', 'OAE = oae_update_plot(OAE, SEQUENCE);');
end


end