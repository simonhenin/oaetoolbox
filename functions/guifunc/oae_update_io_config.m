function OAE = oae_update_io_config(OAE, SEQUENCE, update)
% update main window audio configuration settings
% set(handle, 'units', 'pixels');
% pos = get(handle, 'position');
h = findobj('tag', 'daq_config');

if ~isempty(h),
    
    driver = findobj('tag', 'daq_config.driver');
    defaults = oae_loaddefaults();
    idx = find(strcmp({defaults.block}, 'daq') & [defaults.is_default] == 1);
    
    if isempty(idx),
        %show empty configuration
        set(driver, 'string', 'Audio module is not configured');
    elseif exist('update', 'var'),
        % update device settings
        in = findobj('tag', 'daq_config.input_channels');
        out = findobj('tag', 'daq_config.output_channels');
        
        value = get(in, 'value');
        vals = get(in, 'string');
        nInput = str2num(vals{value});
        
        previous_nInput = OAE(SEQUENCE).nInput;
        if nInput ~= previous_nInput,
            InputChannels = OAE(SEQUENCE).InputChannels;
            if (InputChannels ~= [1:previous_nInput]),
                oae_error('Specific input channels have been selected. Overriding these channel selections and setting to default input channels.');
            end
            OAE(SEQUENCE).nInput = nInput;
            OAE(SEQUENCE).InputChannels = 1:nInput;
            
            ii = find(strcmp({defaults(idx).parameters.tag}, 'numInputChannels'));
            defaults(idx).parameters(ii).value = value;
        end
        
        
        value = get(out, 'value');
        vals = get(out, 'string');
        nOutput = str2num(vals{value});
        
        previous_nOutput = OAE(SEQUENCE).nOutput;
        if nOutput ~= previous_nOutput,
            OutputChannels = OAE(SEQUENCE).OutputChannels;
            if (OutputChannels ~= [1:previous_nOutput]),
                oae_error('Specific output channels have been selected. Overriding these channel selections and setting to default output channels.');
            end
            OAE(SEQUENCE).nOutput = nOutput;
            OAE(SEQUENCE).OutputChannels = 1:nOutput;
            
            % update the OutPutDevices vector to match number of outputs
            if ~isempty(OAE(SEQUENCE).OutputDevices),
                OAE(SEQUENCE).OutputDevices = repmat(OAE(SEQUENCE).OutputDevices(1), 1, nOutput);
            else
                oae_error('There was a problem updating the I/O device settings. Please manage the device settings in the configuration menu.');
            end
            
            ii = find(strcmp({defaults(idx).parameters.tag}, 'numOutputChannels'));
            defaults(idx).parameters(ii).value = value;
%             defaults(idx).parameters(ii).string = cellstr(num2str(OAE(SEQUENCE).OutputChannels'));
%             defaults(idx).parameters(ii).userdata = OAE(SEQUENCE).OutputChannels;
        end
        
%         pth = oae_getuserdefaultspath;
%         defaults_file = [pth oae_getuid '.defaults.mat'];
%         modules = defaults;
%         save(defaults_file, 'modules');

          % autosave defaults
          oae_loaddefaults(defaults, 1);
            
    else
        set(driver, 'string', defaults(idx).label);
        c = get(h,'Children');
        
        set(c, 'visible', 'on');
        
        % device name
        device_name = OAE(SEQUENCE).recordingDeviceName;
        set(findobj('tag', 'daq_config.device_name'), 'string', device_name);
        
        in = findobj('tag', 'daq_config.input_channels');
        out = findobj('tag', 'daq_config.output_channels');
        
        % pull the number of channels from default modules and then set the
        % controller
        ii = find(strcmp({defaults(idx).parameters.tag}, 'numInputChannels'));
        OAE(SEQUENCE).nInputAvailable = length(defaults(idx).parameters(ii).string); %length(defaults(idx).parameters(ii).userdata);
        OAE(SEQUENCE).nInput = defaults(idx).parameters(ii).value;
        
        nInputAvailable = OAE(SEQUENCE).nInputAvailable;
        nInput = OAE(SEQUENCE).nInput;
        str = strread(num2str([1:nInputAvailable]),'%s');
        selected = 1;
        if isempty(str),
            str = {''};
        else
            if ~isempty(nInput),
                if ischar(nInput), nInput  = str2num(nInput); end
                selected = find([1:nInputAvailable] == nInput);
            end
        end
        set(in, 'string', str, 'value', selected, 'visible', 'on');
        
        ii = find(strcmp({defaults(idx).parameters.tag}, 'numOutputChannels'));
        OAE(SEQUENCE).nOutputAvailable = length(defaults(idx).parameters(ii).string); %length(defaults(idx).parameters(ii).userdata);
        OAE(SEQUENCE).nOutput = defaults(idx).parameters(ii).value;
        
        nOutputAvailable = OAE(SEQUENCE).nOutputAvailable;
        nOutput = OAE(SEQUENCE).nOutput;
        if ischar(nOutput), nOutput  = str2num(nOutput); end
        str = strread(num2str([1:nOutputAvailable]),'%s');
        selected = 1;
        if isempty(str),
            str = {''};
        else
            if ~isempty(nOutput),
                selected = find([1:nOutputAvailable] == nOutput);
            end
        end
        set(out, 'string', str, 'value', selected, 'visible', 'on');
    end
    
end