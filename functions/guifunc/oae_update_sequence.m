function oae_update_sequence(hObject, eventdata, handles, removeSeq)

if ~exist('removeSeq', 'var')|isempty(removeSeq)
    removeSeq = 0;
end

%get OAE object from the workspace
OAE = evalin('caller', 'OAE');
SEQUENCE = evalin('caller', 'SEQUENCE');

% validate the sequence first
if SEQUENCE
    errors = OAE(SEQUENCE).validateSequence(SEQUENCE);
else
    % no sequences left in queue, delete all controls and turn off the
    % start button
    errors = 1;
    seq = findobj('tag', 'sequence');
    controls = findobj(seq, {'type', 'uicontrol','-or', 'type', 'hgjavacomponent'},'-and', '-not', 'tag','sequence.list');
    for i=1:length(controls)
        delete(controls(i));
    end
    % turn off the start button
    set(findobj('tag', 'start_sequences'), 'enable', 'off');
    
    % add new sequence
    SEQUENCE = 1;
    OAE(SEQUENCE) = controller();
    assignin('base', 'OAE', OAE);
    assignin('base', 'SEQUENCE', SEQUENCE);
    
end



if ~errors
    
    OAE(SEQUENCE).validSequence = 1;
    if ~removeSeq
        % add new sequence
        SEQUENCE = SEQUENCE + 1;
        OAE(SEQUENCE) = OAE(SEQUENCE-1).copyobj();
        OAE(SEQUENCE).validSequence = [];
    else
        OAE(SEQUENCE).validSequence = [];  
    end
    
    %add sequence to sequence panel
    %seq_textbox = findobj(OAE(1).fig, 'tag', 'sequence.list');
    seq = findobj('tag', 'sequence');
    %delete all current uicontrols
    controls = findobj(seq, {'type', 'uicontrol','-or', 'type', 'hgjavacomponent'},'-and', '-not', 'tag','sequence.list');
    for i=1:length(controls)
        delete(controls(i));
    end
    
    string = {'Saved Sequences:'};
    string = [];
    x = 0.05; y = 0.8; xx = 0.8;
    for j=1:size(OAE,2)-1
        protocol = '';
        if ~isempty(OAE(j).label)
            protocol = OAE(j).label;
        elseif (~isempty(OAE(j).protocol))
            protocol = [OAE(j).paradigmLabel ' (' OAE(j).protocol ')'];
        else
            protocol = [OAE(j).paradigmLabel];
        end
%         if ~isempty(protocol)&~strcmp(protocol,'')
%             string(j).value = sprintf('%s', protocol);
%         else
%             string(j).value = {sprintf('%s', 'default')};
%         end
        OAE(j).label = cellstr(protocol);
        l=oae_uicomponent(seq, 'style', 'edit', 'string', sprintf('(%i)', j), 'units', 'normalized', 'position', [x y 0.08 0.05], 'enable', 'off');
        h=oae_uicomponent(seq, 'style', 'edit', 'string', OAE(j).label, 'units', 'normalized', 'position', [x+0.08 y 0.62 0.05], 'callback', {@oae_update_sequence_label, j});
        c = get(0,'DefaultFigureColor');
        
        jEditbox = findjobj(l);
        jEditbox.Border = [];
        jEditbox.setBackground(java.awt.Color(c(1), c(2), c(3)-0.001)); % hack
        jEditbox.setDisabledTextColor(java.awt.Color(0,0,0));
        jEditbox = findjobj(h);
        jEditbox.Border = [];
        jEditbox.setBackground(java.awt.Color(c(1), c(2), c(3)-0.001)); % hack
        jbh = handle(jEditbox,'CallbackProperties');
        set(jbh, 'FocusGainedCallback',{@oae_sequence_hasfocus, 1});
        set(jbh, 'FocusLostCallback',{@oae_sequence_hasfocus, 0});
        
        cmd = sprintf('[OAE SEQUENCE] = oae_remove_sequence(OAE, SEQUENCE, %i); oae_update_sequence([], [], [], true);', j);
        %oae_icon(seq, 'delete', 'position', [xx y-0.01 0.1 0.08], 'callback', cmd);
        oae_icon(seq, 'delete', 'position', [xx y-0.01 0.1 0.05], 'callback', cmd);
        y = y-0.08;
    end
    % turn on the start button
    set(findobj('tag', 'start_sequences'), 'enable', 'on');
    
  
    %set(seq_textbox, 'string', {string(:).value});
    
    %ok, let's put them back into the workspace
    assignin('base', 'OAE', OAE);
    assignin('base', 'SEQUENCE', SEQUENCE);
    
    % Finally, let's call I/O and paradigm module callbacks to ensure
    % everything is up-to-date
    oae_callmodule('paradigm.parameters');
end
