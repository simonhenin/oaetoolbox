function sndinfo = extractsndinfo(opts_string)
%
%
% sndinfo = extractsndinfo(usrpath, file)
%
% REVISED 12/21/09
%
opts = opts_string;

sndinfo.F1s = extract_param(opts, 'F1s');
sndinfo.F2s = extract_param(opts, 'F2s');
sndinfo.F1e = extract_param(opts, 'F1e');
sndinfo.F2e = extract_param(opts, 'F2e');
sndinfo.T = extract_param(opts, 'T');
sndinfo.gain = extract_param(opts, 'gain');
sndinfo.L1 = extract_param(opts, 'L1');
sndinfo.L2 = extract_param(opts, 'L2');
sndinfo.ratio = extract_param(opts, 'ratio');
%sndinfo.fs = fs;



function val = extract_param(str, param)
% extract paramater from string
% Copyright (c) 2009, Simon Henin
if nargin ~= 2,
    error('myApp:argChk', 'Wrong number of input arguments');
end

val = regexprep(regexp(str, [param '=[0-9.]{1,}'], 'match', 'once'), [param '='], '');
if ischar(val), val = str2num(val); end
if ~isnumeric(val), val = str2num(val{:}); end