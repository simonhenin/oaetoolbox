function  [rms noise] = bpFilterRMS(x, fc_low, fc_high, order, fs )
% designed to filter noise but could work for other signals, if you want to
% keep the filtering zero phase, use FILTFILT instead of FILTER.
[ b , a ] = butter(order, [ fc_low, fc_high ] /( fs /2 ));  % because we are using filtfilt- the order of the filter is doubled so "order-1" is used to get the correct order         % second order filter with cutoffs defined by the freq difference between on frame above and below, space gets larger as we go up in f2
            % can use filtfilt to get zero phase but may slow things down a
            % bit and noise is noise. If you use filtfilt do order/2
            % because the order is applied twice
            noise = filter( b, a, full(x));       % bandpass filter the noise - this is a zero phase filter applied both in the forward and reverse directions (order is doubled)
            rms = sqrt(mean(noise.^2));  
            if 0  % check filter shape for artifacts
                figure(10108)
                BW = [floor(fc_low) floor(fc_high)]
                [H, F] = my_freqz(b, a, max(size(x)));
                F = (F*fs)/(2*pi);   % convert radians per sample to frequency
                semilogx(F, 20*log10(abs(H)));
                ylim([-20 0]);
                xlim([200 10000]);
                pause(0.01)
            end