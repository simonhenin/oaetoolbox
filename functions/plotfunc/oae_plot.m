function oae_plot(OAE, SEQUENCE)
%
% plotting
% 8/21/12 JJH - fixed xlim error when using multiple buffers
% 04/02/18 SH - Added SOAE plotting, resolved issue with plots without stds, plotting many NaNs, Added Xlimits from settings
% 03/17/19 JJH - added if statements to so that if sfoae_supp are measured,
% only one condition will be plotted as we're only interestd in the
% residuals)
% 08/04/19 JJH - Increased the marker sizes for tones (e.g. discrete
% DPOAEs)
%find the plotting panel
handle = findobj('tag', 'results');
axis1 = findobj('tag', 'axis_1');
axis2 = findobj('tag', 'axis_2');
cla(axis1); cla(axis2);
panel = findobj('tag', 'components');
buffers = findobj('tag', 'bufferspanel');
conditions = findobj('tag', 'conditionspanel');
if ~isempty(panel), 
    c = get(panel, 'children'); 
    for i=1:length(c),
        if ishandle(c(i)),
            delete(c(i)); refresh; drawnow;
        end
    end
    if ~isempty(panel) & ishandle(panel),
        delete(panel);  
    end
    if ~isempty(buffers) & ishandle(buffers),
    delete(buffers); end
    if ~isempty(conditions) & ishandle(conditions),
        delete(conditions);
    end
    refresh;
end
%if ~isempty(panel), delete(panel); refresh; drawnow; pause(0.5); end
if isempty(OAE), return; end

%get the current analysis object
obj = OAE(SEQUENCE).getCurrentAnalysisObject();
pos = get(axis1, 'position');

%set up check box panels
top = 0.92; left = 0.08;
panel = uipanel(handle, 'title', 'Components', 'units', 'normalized', 'position', [left top pos(3) 0.08], 'tag', 'components');
top = top - 0.072;
if (obj.nBuffer > 1),
    bufferspanel = uipanel(handle, 'title', 'Buffers', 'units', 'normalized', 'position', [left top pos(3)/2 0.07], 'tag', 'bufferspanel');
    left = 0.05+pos(3)/2;
end
if (obj.nCondition > 1) && ~contains(obj.paradigmType,'suppressor')  % added by JJH
    conditionspanel = uipanel(handle, 'title', 'Conditions', 'units', 'normalized', 'position', [left top pos(3)/2 0.07], 'tag', 'conditionspanel');
end
if ( (obj.nBuffer > 1) || (obj.nCondition > 1) )
    top = top - 0.082;
end

numlabels = length(obj.labels);
cmd = 'OAE = oae_update_plot(OAE, SEQUENCE);';
for i=1:numlabels,
    checked = 0;
    if (obj.labelsOn(i)), checked = 1; end
    oae_uicomponent(panel, 'Units', 'normalized',  'style', 'checkbox', 'value', checked, 'string', obj.labels{i} , 'position', [(i-1)*0.1+0.02 0.1 0.98*(1/(numlabels)) 0.8], 'tag', obj.labels{i}, 'callback', cmd);
end

checkboxHeight = 0.95; fontwidth = 10; left = 0.01;
if obj.nBuffer > 1,
    set(bufferspanel, 'units', 'pixels');
    panelpos = get(bufferspanel, 'position'); 
    width = panelpos(3);
    % figure out how many rows we'll need
    rows = 1;
    for i=1:obj.nBuffer,
        boxwidth = (length(obj.bufferLabels{i})*fontwidth - 0.1)./(width);
        if left+boxwidth > 1,
            rows = rows+1;
            left = 0.01;
        end
        left = left+boxwidth;
    end
   
    if rows > 1,
        panelpos(2) = panelpos(2)-panelpos(4)*(rows-1);
        panelpos(4) = panelpos(4)*rows;
        set(bufferspanel, 'position', panelpos);
    end
    checkboxHeight = checkboxHeight./rows;
    set(bufferspanel, 'units', 'normalized');
    left = 0.01; top = 0.975-checkboxHeight;
    for i=1:obj.nBuffer,
        checked = 0;
        if (obj.buffersOn(i)), checked = 1; end
        boxwidth = (length(obj.bufferLabels{i})*fontwidth - 0.1)./(width);
        if left+boxwidth > 1,
            left = 0.01; top = top-checkboxHeight;
        end
        oae_uicomponent(bufferspanel, 'Units', 'normalized',  'style', 'checkbox','value', checked, 'string', obj.bufferLabels{i} , 'position', [left top boxwidth checkboxHeight], 'tag', obj.bufferLabels{i}, 'callback', cmd);
        left = left+boxwidth;
    end
    if rows > 3,
        % there are too row to allow control of all of them. Disable control.
        % Note: checkboxes need to exist as some other functions check
        % their state, so just turn off panel visibility.
        set(bufferspanel, 'visible', 'off');
    end
end

left = 0.01; fontwidth = 16;
if obj.nCondition > 1 && ~contains(obj.paradigmType,'suppressor') % added by JJH for sfoae_supp (only need to plot 1 condition
    set(conditionspanel, 'units', 'pixels');
    panelpos = get(conditionspanel, 'position'); width = panelpos(3);
    set(conditionspanel, 'units', 'normalized');
    for i=1:obj.nCondition,
        checked = 0;
        if (obj.conditionsOn(i)), checked = 1; end
        top = top-checkboxHeight-0.012;
        boxwidth = (length(obj.conditionLabels{i})*fontwidth)./(width);
        oae_uicomponent(conditionspanel, 'Units', 'normalized',  'style', 'checkbox', 'value', checked, 'string', obj.conditionLabels{i} , 'position', [left 0.1 boxwidth 0.8], 'tag', obj.conditionLabels{i}, 'callback', cmd);
        left = left+boxwidth;
    end
end



%colors = varycolor(length(obj.labelsOn)); 
colors = parula(length(obj.labelsOn));
fmin = NaN; fmax = NaN;
marker_style = '.'; discrete_analysis = false;
marker_size = 14;
if strcmp(obj.paradigmType, 'tone'),
    linestyles = repmat({'none'}, obj.nBuffer, 1);
    residual_linestyle = 'none';
    discrete_analysis = true;
else
    linestyles = oae_linestyles(obj.nBuffer);
    residual_linestyle = '-.';
end

num_lines = 0;
leg_lines = [];
for i=1:obj.nBuffer,
    nCondition = obj.nCondition;
    if contains(obj.paradigmType, 'suppressor')  % added by jjh 3/17/19
        nCondition = 1;
    end
    for j=1:nCondition
        if (obj.buffersOn(i) && obj.conditionsOn(j)),
            f = obj.f(:, obj.labelsOn, i, j);
            db = obj.db(:, obj.labelsOn, i, j);
            
            if ~isempty(obj.db_std),
                stddev = obj.db_std(:, obj.labelsOn, i, j);
            else
                stddev = [];
            end
            
            switch lower(obj.phaseWrap)
                case{'wrapped'}
                    phi = obj.phi(:, obj.labelsOn, i, j);
                case{'unwrapped'}
                    phi = obj.phiUnwrap(:, obj.labelsOn, i, j);
                otherwise
                    disp('no phase wrapping specified plotting "wrapped" phase')
            end
            axes(axis1);
            colormap(colors);
            
            if strcmp(obj.paradigmType, 'tone'),
                % discrete analysis requires analysis points not to be
                % connected with lines
                for ll=1:length(f),
                    line( f(ll), db(ll), 'linestyle', linestyles{i}, 'linestyle', '-', 'marker', marker_style, 'color', colors(ll,:),'MarkerSize', marker_size);
                end
            else
                line( f, db, 'linestyle', linestyles{i}, 'linestyle', '-', 'marker', marker_style, 'MarkerSize', marker_size);
            end
            
            axes(axis2);
            colormap(colors);
            if strcmp(obj.paradigmType, 'tone'),
                line( f, phi , 'linestyle', linestyles{i}, 'marker', marker_style,'MarkerSize', marker_size);
            else
                line( f, phi , 'linestyle', linestyles{i}, 'marker', marker_style);  % using agressive unwrapping tolerance (150 deg) as used in NSEOAE
            end
            % add residual, if it exists
            if ~isempty(obj.residual)
                axes(axis1);
                residual = obj.residual(:, obj.labelsOn, i, j); 
                axes(axis1);
                res = line( f, 20*log10(residual), 'linestyle', residual_linestyle, 'marker', marker_style, 'color', [0.7 0.7 0.7], 'tag', 'residual');
            end
        
            num_lines = num_lines + sum(obj.labelsOn);
            fmin = min(min(f), fmin);
            fmin = min(fmin);  % returns a single element which otherwise causes an error for xlim when fmin is [1 x 2]. 
            fmax = max(max(f), fmax);
            fmax = max(fmax);  % returns a single element
            
            % add std
            if ~isempty(stddev) & (sum(isnan(stddev))~=length(stddev)),
                axes(axis1);
                for ll=1:length(f),
                    line([f(ll) f(ll)], [db(ll)-stddev(ll) db(ll)+stddev(ll)], 'linestyle', '-');
                end
            end
        end 
    end
end

% legend control & scaling control
% --------------------------------
if num_lines,
    axes(axis1);
    legend_str = {obj.labels{obj.labelsOn} 'Noise Floor'};
    legend(legend_str);
    legend('show');
    
    % generate nicer xticks
    switch obj.Xscale
        case {'log'}
            xtick = [250 500 1000 2000 4000 8000 16000];
            
            xlim = [fmin-fmin*0.1 fmax+fmax*0.1];
        otherwise
            tickinterval = 1000;
            xtick = floor((min(f)/tickinterval))*tickinterval:tickinterval:floor((max(f)/tickinterval))*tickinterval;
            xlim = [fmin-50 fmax+50];
    end
    xticklabel = num2str(xtick(:));
    if ~isempty(obj.Xlimits),
        xlim = obj.Xlimits;
    end
    
    set(axis1, 'tag', 'axis_1', 'xscale', obj.Xscale, 'yscale', obj.Yscale, 'xtick', xtick, 'xticklabel', xticklabel, 'xlim', xlim);
    set(axis2, 'tag', 'axis_2', 'xscale', obj.Xscale, 'yscale', obj.Yscale, 'xtick', xtick, 'xticklabel', xticklabel, 'xlim', xlim);
    refresh; drawnow;
else
    set(axis1, 'tag', 'axis_1');
    set(axis2, 'tag', 'axis_2');
end