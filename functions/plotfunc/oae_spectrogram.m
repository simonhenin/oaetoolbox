function OAE = oae_spectrogram(OAE, SEQUENCE)
% This function is useful for assessing how clean (artifact free) the average is.
% updated to work with current analysis methods - JJH Jan 20-2017
% SH 09/26/18 - Recalculated spectrogram properties
%               Added user-selectable spectrogram properties
%               Minor fixes to mutliple buffer sequences
% 03/14/2019 JJH, Minor updates to allow for dynamic sampling rates and increased option to every 500 Hz. Also
% changed labels to reflect units for Fmin, Fmax, and dB.  
fs = OAE(SEQUENCE).fs;
try
    data = OAE(SEQUENCE).analysisObj.avg; % get all averages for this analysis object
    sz = size(data, 1);
    win=hann(2.^nextpow2(round(sz/32)));  % use a hann window to minimize edge effects
    NFFT=length(win);
    overlap = round(length(win)-length(win)*0.1);
    
    nffts = 2.^[7:18]; % possible values
    nffts( find( nffts > sz )) = []; % remove too long
    nfft_val = find(length(win) == nffts);
    if isempty(nfft_val)
        nfft_val = 2;
    end
    overlaps = 10:5:90;
    overlap_val = find(overlaps == 90);
    
    mins = 0:-5:-70;
    maxs = 0:5:100;
    clim = [-40 70];
    
    fmins = [0:0.5:round(fs/1000/4)];  % kHz
    fmaxs = [round(fs/1000/4):0.5:round(fs/1000/2,2)]; % kHz
    flim = [0 20];
    
    Inan = find(isnan(data)); % TODO, find out why there are NAN's I saw them with SF  - 3/14/19 I have been hearing dropped samples in my SF.
    data(Inan) = 0; ypos = 0;
    h = findobj('tag', 'analysis.window');
    set(h, 'units', 'normalized');
    sz = get(h, 'position');
    for p = 1: size(data,3)
        for n = 1: size(data,2)
            x = data(:, n, p);
            figure('Name','OTB Spectrogram','NumberTitle','off', 'units', 'normalized', 'position', [sz(1)+ypos sz(2)+(sz(4)/2)-ypos (sz(3)*0.75) (sz(3)*0.75)], 'UserData', x);
            % add some parameters
            uicontrol('style', 'text', 'string', 'NFFT', 'position', [0.05 0.9 0.05 0.08]);
            uicontrol('style', 'popupmenu', 'string', cellstr(num2str(nffts')), 'value', nfft_val, 'position', [0.1 0.9 0.15 0.08], 'tag', 'nfft', 'callback', @displaySpec, 'UserData', fs);
            
            uicontrol('style', 'text', 'string', 'Overlap (%)', 'position', [0.27 0.9 0.1 0.08]);
            uicontrol('style', 'popupmenu', 'string', cellstr(num2str(overlaps')), 'value', overlap_val, 'position', [0.35 0.9 0.15 0.08], 'tag', 'overlap', 'callback', @displaySpec);
            
            
            uicontrol('style', 'text', 'string', 'Fmin (kHz)', 'position', [0.03 0.82 0.075 0.08]);
            uicontrol('style', 'popupmenu', 'string', cellstr(num2str(fmins')), 'value', find(fmins == flim(1)), 'position', [0.1 0.82 0.15 0.08], 'tag', 'fmin', 'callback', @displaySpec, 'UserData', fs);
            
            uicontrol('style', 'text', 'string', 'Fmax (kHz)', 'position', [0.27 0.82 0.075 0.08]);
            uicontrol('style', 'popupmenu', 'string', cellstr(num2str(fmaxs')), 'value', find(fmaxs == flim(2)), 'position', [0.35 0.82 0.15 0.08], 'tag', 'fmax', 'callback', @displaySpec);
            
            uicontrol('style', 'text', 'string', 'dB SPL Min', 'position', [0.52 0.82 0.075 0.08]);
            uicontrol('style', 'popupmenu', 'string', cellstr(num2str(mins')), 'value', find(mins == clim(1)), 'position', [0.59 0.82 0.15 0.08], 'tag', 'min', 'callback', @displaySpec);
            
            uicontrol('style', 'text', 'string', 'dB SPL Max', 'position', [0.755 0.82 0.075 0.08]);
            uicontrol('style', 'popupmenu', 'string', cellstr(num2str(maxs')), 'value', find(maxs == clim(2)), 'position', [0.83 0.82 0.15 0.08], 'tag', 'max', 'callback', @displaySpec);
            
            
            axes('position', [0.075 0.1 0.85 0.68]);
            spectrogram(x,win,overlap,NFFT,fs, 'yaxis');
            axis xy
            hh = colorbar;
            title(['Spectrogram (dB SPL) of Buffer: ' int2str(n) ', Condition: ' int2str(p)])
            ylim(flim);
            set(gca, 'CLim', clim, 'fontsize', 12);
            ylabel(hh, 'dB SPL');
            ypos = ypos+0.1;
        end
    end
catch
    oae_dialog('You must create at least 1 average before plotting the spectrogram.');
end


function displaySpec(hObject, event)
% update the spectrogram based on user selections
% March 14, 2019, JJH, made the spectrogram dynamic with sampling rate
% get new values from dropdowns
parent = get(hObject, 'parent');

nfft_ui = findall(parent', 'tag', 'nfft');
nfft = oae_getvalue(parent,'nfft');


overlap = 1-oae_getvalue(parent,'overlap')/100;

clim = [];
clim(1) = oae_getvalue(parent,'min');
clim(2) = oae_getvalue(parent,'max');
flim = [];
flim(1) = oae_getvalue(parent,'fmin');
flim(2) = oae_getvalue(parent,'fmax');

% settings
win = hann(nfft);
overlap = round(nfft - nfft*overlap);

x = get(parent, 'UserData');
fs = get(nfft_ui, 'UserData');
ax = findall(parent, 'type', 'axes');
axes(ax);
ttl = ax.Title.String;
spectrogram(x,win,overlap,nfft,fs, 'yaxis');
set(gca, 'CLim', clim,'fontsize', 12);
ylim(flim);
title(ttl);




