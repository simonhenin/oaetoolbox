function OAE = oae_update_plot(OAE, SEQUENCE)
%
% update analysis plot
% 8/7/2012 - fixed xlimit error, when noting selected
% 8/21/2012 - fixed xlim error when using multiple conditions or buffers
% -JJH
% 04/02/18 - SH - Added SOAE plotting, resolved issue with plots without stds, plotting many NaNs, Added Xlimits from settings
% 03/17/19 - JJH modified so that if sfoae supp is plotted only one
% condition is used - which is the residual
% 08/04/19 JJH - Increased the marker sizes for tones (e.g. discrete
% DPOAEs)
% TODOS:
%   Need to fold this function into oae_plot. It does exactl;y the same
%   thing, so must be a way to merge the two functions
%
h = findobj('tag', 'analysis.window');
if ~isempty(h),
    figure(h);
end

%get the current analysis object
obj = OAE(SEQUENCE).getCurrentAnalysisObject();
%find the plotting panel
handle = findobj('tag', 'results');
axis1 = findobj('tag', 'axis_1');
axis2 = findobj('tag', 'axis_2');

cla(axis1); cla(axis2);

numlabels = length(obj.labels);
%labelsOn = logical(zeros(1,numlabels));
labelsOn = false(1,numlabels);
for i=1:numlabels,
    h = findobj('tag', obj.labels{i}, 'style', 'checkbox');
    isOn = get(h, 'value');
    if (isOn),
            obj.labelsOn(i) = true;
    else
        obj.labelsOn(i) = false;
    end
end
if obj.nBuffer > 1,
    for i=1:obj.nBuffer,
        h = findobj('tag', obj.bufferLabels{i}, 'style', 'checkbox');
        isOn = get(h, 'value');
        if (isOn),
            obj.buffersOn(i) = true;
        else
            obj.buffersOn(i) = false;
        end
    end
end
if obj.nCondition > 1  && ~contains(obj.paradigmType,'suppressor')  % added by JJH
    for i=1:obj.nCondition,
        h = findobj('tag', obj.conditionLabels{i}, 'style', 'checkbox');
        isOn = get(h, 'value');
        if (isOn),
            obj.conditionsOn(i) = true;
        else
            obj.conditionsOn(i) = false;
        end
    end
end

colors = varycolor(length(obj.labelsOn)); fmin = NaN; fmax = NaN;
marker_style = '.';
marker_size = 14;
if strcmp(obj.paradigmType, 'tone'),
    linestyles = repmat({'none'}, obj.nBuffer, 1);
    residual_linestyle = 'none';
else
    linestyles = oae_linestyles(obj.nBuffer);
    residual_linestyle = '-.';
end

legend('off'); num_lines = 0;
for i=1:obj.nBuffer,
    nCondition = obj.nCondition;
    if contains(obj.paradigmType, 'suppressor')  % added by JJH 3/17/19
        nCondition = 1;
    end
    for j=1:obj.nCondition,
        if (obj.buffersOn(i) & obj.conditionsOn(j) & sum(obj.labelsOn)),
            f = obj.f(:, obj.labelsOn, i, j);
            db = obj.db(:, obj.labelsOn, i, j);
            
            if ~isempty(obj.db_std),
                stddev = obj.db_std(:, obj.labelsOn, i, j);
            else
                stddev = [];
            end
            
            switch lower(obj.phaseWrap)
                case{'wrapped'}
                    phi = obj.phi(:, obj.labelsOn, i, j);
                case{'unwrapped'}
                    phi = obj.phiUnwrap(:, obj.labelsOn, i, j);
                otherwise
                    disp('no phase wrapping specified plotting "wrapped" phase')
            end
            axes(axis1);
            colormap(colors);
            
            if strcmp(obj.paradigmType, 'tone'),
                % discrete analysis requires analysis points not to be
                % connected with lines
                for ll=1:length(f),
                    line( f(ll), db(ll), 'linestyle', linestyles{i}, 'linestyle', '-', 'marker', marker_style, 'color', colors(ll,:), 'MarkerSize', marker_size);
                end
            else
                line( f, db, 'linestyle', linestyles{i}, 'linestyle', '-', 'marker', marker_style,'MarkerSize', marker_size);
            end
            
            
            axes(axis2);
            colormap(colors);
            if strcmp(obj.paradigmType, 'tone'),
                line(f, phi, 'linestyle', linestyles{i}, 'marker', marker_style, 'MarkerSize', marker_size );
            else
                line(f, phi, 'linestyle', linestyles{i}, 'marker', marker_style);
            end
            % add residual, if it exists
            f = obj.f(:, obj.labelsOn, i, j); residual = [];
            if ~isempty(obj.residual)
                axes(axis1);
                residual = obj.residual(:, obj.labelsOn, i, j);
                axes(axis1);
                res = line( f, 20*log10(residual), 'linestyle', residual_linestyle, 'marker', marker_style,  'color', [0.7 0.7 0.7], 'tag', 'residual');
            end
            num_lines = num_lines + sum(obj.labelsOn);
            fmin = min(min(min(f)), fmin);
            fmin = min(fmin);  % returns a single element even when multiple buffers or conditions are used, fixes xlim error.
            fmax = max(max(max(f)), fmax);
            fmax = max(fmax);
            
            % add std
            if ~isempty(stddev) & (sum(isnan(stddev))~=length(stddev)),
                axes(axis1);
                for ll=1:length(f),
                    line([f(ll) f(ll)], [db(ll)-stddev(ll) db(ll)+stddev(ll)], 'linestyle', '-', 'color', colors(ll,:));
                end
            end
        end
    end
end

% legend control & scaling control
% ---------------------------------
if num_lines,
    axes(axis1);
    legend_str = {obj.labels{obj.labelsOn} 'Noise Floor'};
    legend(legend_str);
    legend('show');

    switch obj.Xscale
        case {'log'}
            xtick = [250 500 1000 2000 4000 8000 16000];
            xlim = [fmin-fmin*0.1 fmax+fmax*0.1];
        otherwise
            tickinterval = 1000;
            xtick = floor((min(f)/tickinterval))*tickinterval:tickinterval:floor((max(f)/tickinterval))*tickinterval;
            xlim = [fmin-50 fmax+50];
    end
    xticklabel = num2str(xtick(:));
    if ~isempty(obj.Xlimits),
        xlim = obj.Xlimits;
    end
    set(axis1, 'tag', 'axis_1', 'xscale', obj.Xscale, 'yscale', obj.Yscale, 'xtick', xtick, 'xticklabel', xticklabel, 'xlim', xlim);
    set(axis2, 'tag', 'axis_2', 'xscale', obj.Xscale, 'yscale', obj.Yscale, 'xtick', xtick, 'xticklabel', xticklabel, 'xlim', xlim);
    refresh; drawnow;
else
    set(axis1, 'tag', 'axis_1');
    set(axis2, 'tag', 'axis_2');
end
