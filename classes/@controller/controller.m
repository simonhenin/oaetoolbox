classdef controller < hgsetget
    % Detailed explanation goes here
    % Fixed non-realtime updateGUI issue - CUNY GC
    % Fixed PhiF indexing issue that caused an error when using
    % onset delay. - CUNY GC
    %  fixed bug in update gui where findobj returned all
    % rtThreshPatch values for multiple sequences. When sequence >1 findobj
    % returned a cell which caused an error. Fixed by adding specific
    % handle. CUNY GC
    % Added comments - CUNY GC
    % Added pause functionality - CUNY GC
    % Added properties and methods for handling individual file
    % saving to alleviate  out of memory issues  - CUNY GC
    % added property "isCustom" to handle custom recordings (calibrated and uncalibrates) so that files are
    % not generated for calibrations and puretones - CUNY GC
    % added pauser method to deal with buffers and
    % conditions. CUNY GC
    % NOTE 1: If you change a property, you likely need to restore defaults
    % to update it. 
    % Modified probeCheck to throw away first two chirps before
    % performing the average and FFT. CUNY GC
    % 
    % 03/16/17 - SH, added padding to stimulus to account for systemDelay in obj.preprocessing
    % 05/31/17 - JJH, added a realtime nstep (rtNstep) for the sweep by
    % sweep analysis which will cause the LSF frames to overlap. Hardcoded
    % to 10 percent at the moment. Not sure if we want to make it dynamic
    % yet so as to not confuse the user with more parameters than they need
    % to be bothered with. 
    % 07/19/18 - SH, renamed some of the rt Variables to be able to differentiate from data variables (e.g. changed Phi to rtPhi)
    %            added custom stimulus phase function, phi, to be able to
    %            support non-stereotyped stimuli (e.g. neely amplitude &
    %            f2/f1 varying sweeps.
    % 03/13/2019 - JJH, In Add condition under noise, changed my_butter to
    % butter.  my_butter is now outdated and no longer is supported. 
    % 03/15/2019 - JJH, Updated rtNfilt in the realtime analysis so that if
    %             you bypass it (rtDisplay = false) then it doesn't default to the
    %             length of the signal (necessary for transient) so I added a condition
    %             for transient and suppressor. This was necessary for
    %             weightedAveraging to work.  It's possible we decide to do
    %             this in weightedAverage only as we really want RT
    %             analysis to be as effecient as possible. But for port
    %             audio3, it doesn't really matter.
    properties                  % --------------------------------------------
        fig                     % main figure handle
        runtimeFig              % runtime figure handle
        autosave = 0;           % autosave, set to one during deployment and 0 when not deployed.
        filename                % filename for the autosaved data object according to requirements
        saveToFile = false;     % save recordings to file. (= true when deployment, = false during testing)
                                %NOTE: what is the difference betweem
                                %autosave & saveToFile?
        
        validSequence           % used to track added sequences             
        
        %patient info          --------------------------------------------
        patientID               % Not currently used
        ear                     % patient ear {L R}
        DOB                     % Date of birth
        sex                     % {M F} 
        notes                   % Notes
        
        % probe                --------------------------------------------
        probeName               % Currently not used
        probeSN                 % Probe serial number
        probeModel              % 'ER10C', 'ER10B', 'ER10B+', 'ER10A', 'ER10D', 'ILO', 'Other'
        micSens                 % the probe microphone sensitivity independant of the system
        
        %recording              --------------------------------------------
        audioInit = 0;          % audio intialized state - 0 not intialized 1 intialized
        underflow = 0;          % portaudio buffer underflow count - this is always 1 because we don't fully fill the last buffer. 
        playback = true;        % perform playback or not - to be used for Pause
        realTime = false;       % do realtime (true) or not (false), used by updateGUI method
        driver                  % recording software driver - currently portaudio
        daqLabel                % data acquisition label
        daqCallback;            % data acquisiont callback such as portaudio, portaudio3, etc.
        playbackDeviceID        % hardware device ID as queried by portaudio or other device driver
        playbackDeviceName
        recordingDeviceID       % recording device ID as queried by portaudio or device driver
        recordingDeviceName
        rampTime = 0.007;       % This is the ramp time for all audio - anything less than 4 ms appears to yeild a transient.
        fs = 44100;             % sampling rate - adjustable between 44100 and 48000
        pref = 2e-5;            % SPL reference of 0 dB SPL = 20 microPa
        sensitivity = 0.4913;   % this is the micSens * system's sensitivity
        gain = 1;               % gain, this is really not used, currently the system gain is included into the sensitivty
        systemDelay = 0;        % this is the delay in samples for a given hardward system:@ 44100 M-Audio is about 1050 (23.5 ms) samples and Echo is 2152 (48.8 ms) samples, ITE call affects sample latency
        onsetPadding;           % onset padding (in samples) used for CAS noise
        isCustom = 0;           % must be set to one for all custom recordings
        
        
        nOutputAvailable;       % number of available output channels on device
        nOutput;                % size of nOutput (n); the number of channels
        OutputChannels;         % n ; Selected output channel for measurement, e.g. DPOAE uses n = [1 2]
        OutputDevices;          % Output device IDs...must be the same length as OutputChannels
        nInputAvailable;       % number of available output channels on device
        nInput;                 % size nInput (r)
        InputChannels;          % r ; input channel for measurement - currently only using two for probe mic calibration
        InputDevices;           % input device IDs..must be the same length as InputChannels
        nRepetitions = 8;       % ( 1 x 1 ) 
        nBuffer = 0;            % b, number of total buffers
        Buffer = 0;             % Current buffer or selected buffer
        bufferLabels;           % Labels for buffers; e.g. 'Up', 'Down', 'Alternating'
        nCondition = 0;         % c, number of conditions - conditions are currently only used MOCR (CAS and NonCAS) DP and SF only use one condition
        Condition = 1;          % Current state of Condition
        conditionLabels;        % Labels for each condition
        rep = 1;                % current repetition
        bufferSize = 1024;      % Portaudio buffer size - 1024 seems to reduce glitches in MS Windows 7
        ISI = 0.5;              % Inter stimulus interval in seconds
        onsetDelay = 0;         % onset delay (samples) of stimulus (e.g. MOCR CAS)
        offsetDelay = 0;        % offset delay (samples) of stimulus (e.g. MOCR CAS)
        datapath  = [pwd filesep 'data'] % default data folder path for saving OAE objects (Different then datafolders used below)
        dataFolder              % generated at the start of the first sequence. All data from all sequences in the object are then in this folder  
        dataFileNames           % each file name generated after it's recorded this is a matrix sized: [nreps x buffers x conditions] named by datetime
        %v2pa = 2.6316;         %(previous)
        %v2pa = 0.8322;         % volts to pascals multiplier (For mimosa)
        %v2pa = 12.5;           % M-AUDIO
        %v2pa = 1;              % MOTU
        v2pa = [];
        v2pa_levelcal = 1;      % in-situ level calibration correction factor (e.g. runLevelCalibration() )
        
        % stimulus calibration  -------------------------------------------
        calibrationLabel        % Not used
        calibrationCallback     % specifies the calibration callback e.g. 'iteCalibration2'
        calibration             % calibration for each channel (size nOutput)
        calibration_nfft = 2048;% FFT size for calibration
        stimCalibrationFilter   % needs to be nOutput x length of filter; this is the timedomain sitmulus calibration filter
        doCalibrationPlot = 0;  % show stimulus correcdtion filter (1) or not (0)
        eartipType = '14A'      % Current eartip selection
        SystemSens              % the input hardware sensitivity independatn of hte microphone
        stimCalType             % i.e. coupler, FPL, ITE...
        calibrationFreqLimits   % Frequency limits of stimulus calibration (e.g. [500 12000]
        probe_check_data        % stores any probe checks for the sequence
        
        % microphone calibration-------------------------------------------
        micCalibrationLabel     % Not used yet (future use)
        couplerCalFile          % Not used yet (future use)
        micCalFile              % Not used yet (future use)
        micCalibrationFilter    % time domain microphone correction filter; must be symmetric to account for filter group delay.
        micCalibrationFilterLength       % length of the microphone calibration filter
        rtMicCalibrationFilter           % was previously used with convolution ( currently not used in favor of a look up table)
        rtFrqMicCalibrationFilterDB      % used as a simple magnitude correction for rtdisplay to reduce overhead and complexity.
        doMicCal = 1;                    % turn mic cal on or off
        micCalFilterSamplingRate
        
        % Making the Microphone Calibration Filter-------------------------
        pmicCalFilterGroupDelay 
        pmicCalNavg
        pmicCalFcLow
        pmicCalFcHigh
        pmicCalDoHighFreqEmphasis       % emphesize frequencies above 10kHz flag (1 on, 0 off)
       
        
        % Using the Microphone Calibration Filter (Current Filter)
        
        % Thevenin Calibration
        temperature
        cavityDiameter         % Cavity diameter in (mm)
        FPLNCh                 % Number of sources to calibrate
        cavity_lengths         % Residual Lengths in (mm) (the residual lenght is the length after the probe has been inserted into the cavity. Size of vector must be equal to FPLNCh
        theveninSource         % Thevenin Source
        reflectance
        FPL
        theveninError
        IPL
        EPL
        z0
        
        
        %paradigm info for each channel -----------------------------------
        paradigmLabel           % label for the selected paradigm (DPOAE, SFOAE, MOCR_DPOAE, SFOAE_SUPPRESSION...)
        paradigmType            % Type of paradigm (logsweep, linsweep, tone, chirp, transient)
        protocol                % paradigm protocol
        label                   % user-specified label
        levelParadigm           % Not used - intended to set up level functions for DPOAE such as optimal, scissors, equal level etc.
        fstart                  % starting frequency in the structure of ( nPrimary x buffer x condition)
        fend                    % ending frequency in the structure of (nPrimary x buffer x conditoin)
        nPrimaries              % number of primaries
        T                       % total length of playback and includes any onset or offset for MOCR CAS
        L                       % Levels for stimulus(1 x n x b x c)
        CASFcL                  % CAS low frequency filter cutoff (Hz)
        CASFcH                  % CAS high frequency filter cutoff (Hz)
        
        % artifact rejection callback--------------------------------------
        artifactrejectionLabel  = ''   % label for artifact rejection paradigm
        artifactrejectionCallback = '' % callback used for artifact rejection
        arOverlap                  % overlap percentage for the artifact rejection calculation (percent) which is converted to samples
        arNoiseType                % type of noies filtering to use for AR criterion; e.g. OAE or braodband noise
        arDoThreshold = 1;         % use AR threshold or not for post processing AR
        arThreshold = 10;          % Threshold level in dB for post processing AR
        
        % datasets 
        datasets        = [];
        current_dataset = [];
        
        %analysis             ---------------------------------------------   
        analysisIndex = 0;      % Index of analysis sequence (TODO: I don't think this is used anymore, in favor of datasets and current_dataset indexing above. Check and remove if true)
        currentIndex;           % current analysis index
        analysisObj             % name of analysis object which is a subobject of OAE class
        
        
    end
    properties (SetAccess = private, Hidden = true)
        % these properties will be private and hidden. You must use methods to
        % access these properties
        
        stimulus           % original un-processed stimulus (nSamples x nCh x nBuffers x nConditions) (n x m x b x c)
        phi                % phase function (nSamples x nCh x nBuffers x nConditions) (n x m x b x c)
        stim               % processed (e.g. calibrated) stimulus (nSamples x nCh x nBuffers x nConditions) (n x m x b x c)
        data               % raw recorded data in the structure of: (reps x m x b x c); Note that data has the systemshift removed as well as CAS onset and offset.
        residual           % this is the residual from the LSF - may not be used in the future
        avg                % averaged file (m x b x c)
        nf                 % noise floor (m x b x c)    
        extraprops         % a structure which can store new variables when needed. Must access using setextraprops and getextraprops methods
        
        %online artifact rejection--------------------------------------------
        rtPhiFunc               % real-time phi functions (n x b x c)
        rtFFunc                 % real time frequencies
        rtPhi                   % phi functions for LSF
        rtPhiF                    % freuquency functions for LSF
        Delay                   % Delay for the LSF
        %delayfunction = '1./(0.035+0.00006.*x+0.000000002.*x.^2)';  %delay function used for nseoae_callback
        %delayfunction = '1./(0.033+0.00004.*x+0.000000002.*x.^2)';
        delayfunction = '1./(0.033+0.00003.*x+0.0000000009.*x.^2)';  % delay function used for real time
        rtBufferSize = 4096;    % size of the realtime buffer whichis used by method updateGUI
        rtBW = 8;               % default bandwidth for realtime LSF (Hz)  % this should only be for DPOAE and 2 Hz for SFOAE
        rtlsf                   % realtime LSF callback
        rtNfilt = 5512          % (default nfilt..needs to be recalucated based on sweep rate)
        rtnstep = 0.1           % as percent of rtNstep, realtime nstep, now set to use size of rtNfilt
        rtWvec                  % realtime LSF window weighting vector
        rtWindowType = 2;       % hann window
        rtData                  % realtime data; structure is (nFrame x BufferSize x b x c) to bold data frames
        rtBuffer                % current realtime data frame
        rtOAE                   % realtime OAE magnitudes
        rtNPhi                  % real time number of Phi's
        rtTmpOAECoefs           % realtime temporary OAE coefficients from the least squares fit
        nTime = 0;              % used for lsf indexing
        rtARthreshold = 30;      % Threshold in RMS dB SPL for real time rejection
        rtFrameWeights          % initialized in obj.preprocessing (nframes x nBuffer x nCondition)
        rtPrimaryCoefs          % coefficients of the primaries
        rtAvgCoefs              % realtime averaged coeficients of the OAE
        rtAvgResidual           % these residuals are averaged then used to calculate the rtAvgRMSNoise
        rtAvgRMSNoise           % realtime average of the RMS noise floor
        rtResidual              % temporary for debugging
        
        % runtime axes         --------------------------------------------
        OAEDisplay             % handle for realtime axes
        OAELines               % 2 x nBuffer x nCondition: 2 = oae + residual
        VUMeter                % handle for VUMeter on realtime GUI
        displayVU   = 1;       % turn VUMeter on or off (1 or 0)
        displayRT    = 1;       % show real-time display 
        rtYlim = [-40 40];     % Y limits of the VUMeter and OAE axes in dB SPL
        rtXlim = [0 8000];     % frquency limits in Hz for the real time x axis
        rtX                    % same as OAELines; store oae data in 1, residual in index 2 e.g. rtX(1, :, Buffer, Condition) = oae; rtX(2, :, Buffer, Condition) = residual
        rtY                    % same as above but for Y
        nT                     % used internally for updateGUI method
        dd                     % used for debugging. 
        
        % GUI run counters     --------------------------------------------
        RTcounter              % handle for RTcounter: tracks what rep your on and displays in the realtime GUI
        runcounter             % current sweep your on, used for RTcounter
        runtotal               % total number of sweeps to be displayed in the RTcounter
        % GUI RT threshold text and edit elements
        rtThreshText           % The handle for the moveable threshold bar in the RT gui
        rtThreshEdit           % Not used anymore - you could enter values in it and it would set the threshold
        rtThreshPatch          % patch object for the little black bar in the realtime GUI
        rtThreshLine           % line object for the moveable threshold bar
        % RT GUI Pause button
        pauseButton            % Realtime Pause Button Handle
        pauseState = 0
        
        % probe check properties TODO: add to managaeble settings
        pc_xlim = [0 10000];
        
        linecolors     = [0    0.4470    0.7410
                        0.8500    0.3250    0.0980
                        0.9290    0.6940    0.1250
                        0.4940    0.1840    0.5560
                        0.4660    0.6740    0.1880
                        0.3010    0.7450    0.9330
                        0.6350    0.0780    0.1840
                        0.6350    0.0780    0.4840
                        0.6350    0.4780    0.1840
                        0.9350    0.0780    0.1840
                             0    1.0000         0
                             0    1.0000    1.0000
                             0    0.5000    1.0000
                             0         0    1.0000
                        0.5000         0    1.0000
                        1.0000         0    1.0000
                        1.0000         0    0.5000
                        1.0000         0         0
                        0.5000         0         0
                             0         0         0];
                         
                         
         % state object
         stateObj = [];

    end
    
    methods
        % sets up object
        function obj = controller()
            % Check if object passed to constructor. If so, requires
            % deep-copy of object and properties
            % ------------------------------------------------------
%             if nargin == 1 && isa(oldObj,'controller')
%                 props = properties(oldObj);
%                 for i = 1:length(props)
%                     % Use Dynamic Expressions to copy the required property.
%                     % -----------------------------------------------------
%                     obj.(props{i}) = oldObj.(props{i});
%                 end
%                 
%                 % copy stimulus
%                 obj.stimulus = oldObj.stimulus;
%             else
%                 uid = oae_getuid();
%                 file = [oae_getsystempath filesep uid '_obj.mat'];
%                 obj = obj.loadSettings(file);
%             end
            
            uid = oae_getuid();
%             file = [oae_getsystempath filesep uid '.obj.mat'];
            file = [oae_getsystempath uid '.obj.mat'];
            obj = obj.loadSettings(file);
        end
        % deep copy of the object
        function newObj = copyobj(obj)
            % Construct a new object based on a deep copy of the current
            % object of this class by copying properties over.
            % ------------------------------------------------------
            newObj = controller();
            props = properties(obj);
            for i = 1:length(props)
                % Use Dynamic Expressions to copy the required property.
                if strcmp(props{i}, 'runtimeFig') | strcmp(props{i}, 'fig') | isstruct(obj.(props{i})) | isobject(obj.(props{i})) | (~iscell(obj.(props{i})) & isnan(obj.(props{i}))), % remove Nan
                    newObj.(props{i}) = [];
                else
                    newObj.(props{i}) = obj.(props{i});
                end
            end
            newObj.audioInit = 0;
        end

        %%load setting
        function obj = loadSettings(obj, file)
            if exist(file, 'file'),
                load(file);
                % remove any old patient info.
                props.patientID = [];
                props.patientID = [];
                props.ear       = [];                     
                props.DOB       = [];              
                props.sex       = [];  
                props.notes     = [];  
                
                fields = fieldnames(props);
                for i=1:length(fields),
                    try,
                        set(obj, fields{i}, eval(['props.' fields{i}]));
                    catch
                        warning(sprintf('OTB Warning: field %s is not accessible in this object. This could be because the class was recently updated.', fields{i}));
                    end
                end
            end
        end
        
        % Saves object to file
        function saveController(obj)
            props = get(obj);
            props.extraprops = obj.extraprops;
            %fields = fieldnames(props);
            uid = oae_getuid();
            file = [oae_getsystempath uid '.obj.mat'];
            save(file, 'props');
        end
        
        % Special methods for saving and reverting back to states of
        % current states of the controller. 
        % Mainly used by popout module blocks, so that if someone decides
        % not to save the current changes, the controller can revert back
        % to its previous state. Setting saveState should not affect
        % front-page modules (e.g. paradigm), since they will never be
        % reverted (e.g. oae_close_window for popout module blocks).
        function obj = saveState(obj)
                obj.stateObj = obj.copyobj;
        end
        function bool = checkState(obj)
            tmpObj1 = obj.copyobj;
            tmpObj2 = obj.stateObj;
            if isempty(tmpObj2),
                % stateObj hasn't been set yet, so it could not have changed.
                bool = false;
                return;
            end
            % has the object changed?
            if ~isequal(tmpObj1, tmpObj2)
                bool = true;
            else
                bool = false;
            end
        end
        function obj = revertState(obj)
            obj = obj.stateObj;
        end
        
        % performs any preprocessing stesps needed before making a
        % measurement
        function obj = preProcessing(obj)
            % grab unprocessed stimulus and assign to stim (=calibrated stimulus)
            % this is done in case recording is aborted and sequence is
            % re-run (e.g. re-calibrated, etc.)
            obj.stim = obj.stimulus;
            
            % pad the stimulus with zeros to account for the systemDelay
            obj.stim = [obj.stim; zeros(obj.systemDelay, size(obj.stim,2), size(obj.stim,3), size(obj.stim,4))]; 
            
            % preprocess stimuli, apply calibrations
            obj = obj.runCalibration();
            obj = obj.applyStimulusCalibration();
            
            % perform level calibration after any stimulus calibration
            % methods. This will adjust v2pa values accorindly.
            obj = obj.runLevelCalibration();
            
            % Scale by v2pa (output sensitivity). This is done here since calibration can be performed at any time before recording sequence is run. 
            if size(obj.stimulus,2) > size(obj.stimulus,1)
                v2pa = obj.v2pa';
            else
                v2pa = obj.v2pa;
            end
            obj.stim = bsxfun(@times, obj.stim, v2pa(obj.OutputChannels));
            % apply the in-situ level calibration correction factor
            obj.stim = bsxfun(@times, obj.stim, obj.v2pa_levelcal(obj.OutputChannels));
            
            % build RT model
            % --------------------------------
            %t = (0:(obj.T*obj.fs)-1) / obj.fs;
            %obj.rtPhi = zeros(length(t), obj.nOutput, obj.nBuffer, obj.nCondition);
            %obj.rtPhiF = zeros(length(t), obj.nOutput, obj.nBuffer,obj.nCondition);
            
            
            % Buffer & Condition initialization
            if (obj.nCondition == 0), obj.nCondition = 1; obj.Condition = 1; end
            
            
            % setup realtime analysis parameters for realtime lsf
            if obj.displayRT
                
                %% TODO: implement delay inside feval
                t = (0:(obj.T*obj.fs)-1) / obj.fs;
                obj.Delay = zeros(length(t), obj.nOutput+1, obj.nBuffer, obj.nCondition);
                
                for b=1:obj.nBuffer,
                    for c=1:obj.nCondition,
                        nCounter = 1;
                        for n=1:obj.nOutput+1,
                            if (n > obj.nOutput)
                                % add oae component
                                %components_fstart = obj.fstart(:,b,c);
                                %components_fend = obj.fend(:,b,c);
                                components_fstart = obj.fstart(:,b);
                                
                                % If fend is empty, it means it must be a tone
                                if ~isempty(obj.fend)
                                    components_fend = obj.fend(:,b); 
                                end
                                
                                switch obj.paradigmLabel
                                    case {'dpoae', 'dpoae-mocr'}
                                        % resize delay vector
                                        obj.Delay = zeros(length(t), obj.nOutput+2, obj.nBuffer, obj.nCondition);
                                        obj.rtBW = 8;  % 8 hz bandwidth
                                        switch obj.paradigmType,
                                            case 'custom'
                                                % add custom dpoae (2*f1-f2) based on sweep phase functions
                                                dpphi = 2*obj.phi(:, 2, b, c)-obj.phi(:, 1, b, c);
                                                f = oae_phase2freq(dpphi, obj.fs);
                                                obj.rtPhi(:,nCounter,b,c)   = dpphi;
                                                obj.rtPhiF(:,nCounter,b,c)  = f;
                                                oae_fstart                  = f(1);
                                                oae_fend                    = f(end);
                                                nCounter = nCounter + 1;
                                            otherwise
                                                % basic logsweep dpoae can be computed from fstart/fend
                                                % 3*f1-f2 component  !!TODO:We should probably remove this component. Not necessary
                                                oae_fstart = 3*min(components_fstart)-2*max(components_fstart);
                                                oae_fend = 3*min(components_fend)-2*max(components_fend);
                                                
                                                % only if fstart is set, otherwise it must be
                                                % non-recorded channel
                                                if oae_fstart,
                                                    [phi f] = oae_build_phi(oae_fstart, oae_fend, obj.T, obj.fs);  % does not compensate for system shift or onset delay
                                                    obj.rtPhiFunc{nCounter,b,c} = phi; obj.rtFFunc{nCounter,b,c} = f;
                                                    
                                                    % hard coded Phi's remove in final
                                                    obj.rtPhi(:,nCounter,b,c) = feval(phi, t);
                                                    obj.rtPhiF(:,nCounter,b,c) = feval(f, t);
                                                    
                                                    nCounter = nCounter + 1;
                                                end
                                                
                                                % 2f1-f2 component
                                                oae_fstart = 2*min(components_fstart)-max(components_fstart);
                                                oae_fend = 2*min(components_fend)-max(components_fend);
                                                
                                                % only if fstart is set, otherwise it must be
                                                % non-recorded channel
                                                if oae_fstart,
                                                    [phi f] = oae_build_phi(oae_fstart, oae_fend, obj.T, obj.fs);
                                                    obj.rtPhiFunc{nCounter,b,c} = phi; obj.rtFFunc{nCounter,b,c} = f;
                                                    
                                                    % hard coded Phi's remove in final
                                                    obj.rtPhi(:,nCounter,b,c) = feval(phi, t);
                                                    obj.rtPhiF(:,nCounter,b,c) = feval(f, t);
                                                    
                                                    nCounter = nCounter + 1;
                                                end
                                        end
                                        % set indices for lsf coeffiencts.
                                        % These are used for more efficient rt
                                        % processing (speed)
                                        obj.rtNPhi = 2;  % number of primary components
                                    case {'sfoae_supp'}
                                        
                                        switch obj.nCondition
                                            case 2
                                              disp('2')  
                                            case 3
                                                disp('3')
                                        end
                                   
                                                
                                    case {'sfoae', 'sfoae-mocr'}
                                        obj.rtBW = 2;
                                        %remove CAS Nans
                                        index = isnan(components_fstart);
                                        if ~isempty(index)
                                            components_fstart(index) = []; components_fend(index) = [];
                                        end
                                        % hard coded delay function for now.
                                        delayfunction = obj.delayfunction;
                                        oae_fstart = components_fstart;
                                        oae_fend = components_fend;
                                        switch obj.paradigmType,
                                            case 'custom'
                                                x = oae_phase2freq(  obj.phi(:, 1, b, c) , obj.fs); % sweep frequency
                                                obj.Delay(:,nCounter,b,c) = feval(@(x) eval(delayfunction), x) ./ 1000; % evaluate delay function at sweep frequency (in ms)
                                                obj.Delay(:,nCounter,b,c) = oae_computePhiTau(obj.Delay(:,nCounter,b,c), x);
                                                obj.rtPhi(:,nCounter,b,c) = obj.phi(:, 1, b, c);
                                                obj.rtPhiF(:,nCounter,b,c) = x;
                                                nCounter = nCounter + 1;
                                            otherwise
                                                f1 = oae_fstart; f2 = oae_fend; gamma = log(f2/f1)/obj.T;
                                                x = (f1)*(exp(gamma .* (t) )-1) + f1; % sweep frequency
                                                obj.Delay(:,nCounter,b,c) = feval(@(x) eval(delayfunction), x) ./ 1000; % evaluate delay function at sweep frequency (in ms)
                                                obj.Delay(:,nCounter,b,c) = oae_computePhiTau(obj.Delay(:,nCounter,b,c), x);
                                                
                                                % only if fstart is set, otherwise it must be
                                                % non-recorded channel
                                                if oae_fstart,
                                                    [phi f] = oae_build_phi(oae_fstart, oae_fend, obj.T, obj.fs);
                                                    obj.rtPhiFunc{nCounter,b,c} = phi; obj.rtFFunc{nCounter,b,c} = f;
                                                    
                                                    % hard coded Phi's remove in final
                                                    obj.rtPhi(:,nCounter,b,c) = feval(phi, t);
                                                    obj.rtPhiF(:,nCounter,b,c) = feval(f, t);
                                                    
                                                    nCounter = nCounter + 1;
                                                end
                                                
                                        end
                                        obj.rtNPhi = 1;
                                end
                                if ~isempty(obj.fend),
                                    obj.rtXlim = [min(oae_fstart,oae_fend)-50 max(oae_fstart,oae_fend)+50]; end
                            else
                                % only if is sweep, other we cannot do rt sweep processing
                                switch obj.paradigmLabel
                                    case {'dpoae', 'dpoae-mocr', 'sfoae'}
                                        switch obj.paradigmType,
                                            case 'custom'
                                                % custom sweeps use their own phase functions
                                                obj.rtPhi(:,nCounter,b,c) = obj.phi(:, n, b, c);
                                                % frequency determined from phase function
                                                obj.rtPhiF(:,nCounter,b,c) = oae_phase2freq(obj.phi(:, n, b, c), obj.fs);
                                                nCounter = nCounter + 1;
                                            otherwise
                                                % only if fstart is set, otherwise it
                                                % must be non-recorded channel
                                                if ~isnan(obj.fstart(n,b)),
                                                    [phi f] = oae_build_phi(obj.fstart(n,b), obj.fend(n,b), obj.T, obj.fs);
                                                    obj.rtPhiFunc{nCounter,b,c} = phi; obj.rtFFunc{nCounter,b,c} = f;
                                                    
                                                    % hard coded Phi's remove in final
                                                    obj.rtPhi(:,nCounter,b,c) = feval(phi, t);
                                                    obj.rtPhiF(:,nCounter,b,c) = feval(f, t);
                                                    
                                                    nCounter = nCounter + 1;
                                                    
                                                end
                                        end
                                end
                            end
                        end
                    end
                end
            end
            % TODO: remove...this calcuates feval upfront...waste of space
            % ------------------------------------------------------------
%             [phi f] = oae_build_phi(obj.fstart(n,b,c), obj.fend(n,b,c), obj.T, obj.fs);
%             obj.rtPhiFunc{n,b,c} = phi; obj.rtFFunc{n,b,c} = f;
%             obj.rtPhi(:,n,b,c) = feval(phi, t);
%             obj.rtPhiF(:,n,b,c) = feval(f, t);
            
            if obj.displayRT,
                obj.rtNfilt = floor((obj.fs/2)*(obj.T/log2(obj.fend(1)/obj.fstart(1))) / obj.rtBW); % calculate nfilt based on sweeprate, using 8Hz bandwidth
                obj.rtWvec = computeWindow(0:obj.rtNfilt-1, obj.rtWindowType);
            elseif contains(obj.paradigmType, 'transient')
                obj.rtNfilt = floor(obj.fs*obj.T); 
            elseif contains(obj.paradigmType, 'supressor')
                obj.rtNfilt = floor((obj.fs/2)*(obj.T/log2(obj.fend(1)/obj.fstart(1))) / obj.rtBW); % calculate nfilt based on sweeprate, using 8Hz bandwidth  
                obj.rtWvec = computeWindow(0:obj.rtNfilt-1, obj.rtWindowType);
            end
            
            numBuffers = ceil(obj.T*obj.fs / obj.rtBufferSize);         % number of expected realtime buffer frames
            if strcmpi(obj.daqCallback,'oae_portaudio3')
                numNfilt = floor(((obj.T*obj.fs) - obj.rtNfilt)/floor(obj.rtnstep*obj.rtNfilt)) + 1;
            else
                numNfilt = ceil(obj.T*obj.fs / obj.rtNfilt);  % number of expected lsf calculations to perform
            end
            obj.rtData = zeros(numBuffers*obj.rtBufferSize, obj.nBuffer, obj.nCondition);
            obj.rtFrameWeights = nan(numNfilt, obj.nBuffer, obj.nCondition);
            
            if ~isempty(obj.fstart),
                obj.nPrimaries = sum(~isnan(obj.fstart(:,1,1,1)));
            else
                obj.nPrimaries = 0;
            end
            obj.rtPrimaryCoefs = nan(obj.nPrimaries*2, numNfilt,  obj.nBuffer, obj.nCondition);     % priumary coefficients
            obj.rtAvgCoefs = nan(2, numNfilt,  obj.nBuffer, obj.nCondition);                        % only for OAE coefs
            obj.rtAvgResidual = nan(numNfilt*obj.rtNfilt, obj.nBuffer, obj.nCondition);
            obj.rtAvgRMSNoise = nan(numNfilt, obj.nBuffer, obj.nCondition);                         %  a single rms value for each frame
            %obj.rtResidual = zeros(numNfilt*obj.rtNfilt, obj.nBuffer, obj.nCondition);
            %             obj.rtX = nan(2,numNfilt, obj.nBuffer, obj.nCondition);
            %             obj.rtY = nan(2, obj.nBuffer, obj.nCondition);
            obj.rtX = nan(2+obj.nPrimaries,numNfilt, obj.nBuffer, obj.nCondition);
            obj.rtY = nan(2+obj.nPrimaries,numNfilt, obj.nBuffer, obj.nCondition);
            obj.nT = 1;
            
            % set rtBufferSize to nfilt.
            obj.rtBufferSize = obj.rtNfilt;
            
        end
        
        
        % performs necessary post processing steps
        function obj = postProcessing(obj)
            
            fprintf(1, 'post-processing\n');
            
            % postprocess recording, apply microphone calibrations
            delete(obj.runtimeFig);   % changed from close which uses the closeRequestFcn - it was disabled for the runtimeFig, however so we must use delete.
            
            
            %             obj.Buffer = 1; obj.Condition = 1;
            %             % remove systemDelay from data
            %             for i=1:obj.nBuffer,
            %                 for j=1:obj.nCondition,
            %                     for k=1:size(obj.data,1),
            %                         % remove system delay & on/offset delay
            %                         % -------------------------------------
            %                         %obj.data(k, :, i, j) = [obj.data(k,obj.systemDelay+obj.onsetDelay+1:end-obj.offsetDelay, i, j) obj.data(k, 1:obj.systemDelay, i, j)];
            %                        obj.data(k, :, i, j) = [obj.data(k, obj.systemDelay+obj.onsetDelay+1:end-obj.offsetDelay, i, j) obj.data(k, 1:obj.systemDelay+obj.onsetDelay, i, j) obj.data(k, end-obj.offsetDelay+1:end, i, j)];
            %                     end
            %                 end
            %             end
            
            % obj.data(obj.rep,:,obj.Buffer, obj.Condition) = input./obj.sensitivity./obj.gain/2e-5;
            %             if (obj.systemDelay+obj.onsetDelay+obj.offsetDelay > 0 )
            %                 obj.data(:, end-obj.systemDelay-obj.onsetDelay-obj.offsetDelay:end, :, : ) = [];
            %             end
            
            % remove delays from final data matrix
            if obj.saveToFile,
                
                for c = 1:obj.nCondition
                    for b = 1:obj.nBuffer
                        for r = 1:obj.nRepetitions
                            
                            
                            data = obj.getDataFromFile(r,b,c);
                            %see note below 
                            %if obj.systemDelay > 0,
                            %    data( 1 : obj.systemDelay ) = [];
                            %end
                            if obj.onsetDelay > 0,
                                data( 1 : obj.onsetDelay ) = [];
                            end
                            if obj.offsetDelay > 0,
                                data( end - obj.offsetDelay : end) = [];
                            end
                            
                            obj.data(r, :, b, c) = data;
                            
                        end
                    end
                end
            else
                %obj.data(:, 1:obj.systemDelay, :, : ) = [];  %SH, removed this because we are already removing the systemDelay when we save the recording (obj.saveRecording)
                obj.data(:, 1:obj.onsetDelay, :, : ) = [];
                obj.data(:, end-obj.offsetDelay:end, :, : ) = [];
            end
            % housekeeping
            % ------------
            obj.rtPhi = []; obj.rtPhiF = []; obj.rtPhiFunc = []; obj.rtFFunc = []; obj.rtX = []; obj.rtY = [];
        end
        
        %-- Recording methods --%
        function obj = startRecording(obj)
            oae_log('Starting new recording sequence.');
            obj.audioInit = [];
            if isempty(obj.gain),
                obj.gain = 1;
            end
            
            obj = obj.preProcessing(); % calibration, etc...
            obj = obj.initializeGUI();
            
            while (obj.rep <= obj.nRepetitions),
                obj = obj.pauser;
                for n=1:obj.nCondition,
                    obj = obj.pauser;
                    obj.Condition = n;
                    for j=1:obj.nBuffer,
                        if obj.playback,
                            % only record if not paused
                            if obj.pauseState == 0
                                obj = obj.pauser;
                                obj.Buffer = j;
                                pause(obj.ISI);
                                obj.rtBuffer = 0;
                                obj.nTime = 0;
                                obj.nT = 1;
                                
                                obj = record(obj);
                            end
                        end
                    end
                end
                obj.rep = obj.rep + 1;
            end 
       
            
            %debug
            
            %             % dpoae delay0 delay100
            %             axes(obj.OAEDisplay);
            %             load(['~/Desktop/test-nskip-' num2str(obj.systemDelay) '.mat']); hold on;
            %             plot(oae(3).f, 20*log10(oae(3).mag / gain / 2e-5 / sensitivity), '-.or'); hold on;
            %             plot(oae(3).f, 20*log10(oae(3).rmserror/ gain / 2e-5 / sensitivity), '--r'); hold on;
            %             legend({'oaetoolbox' 'oaetoolbox error' 'nseaoe', 'rms error'});
            %             export_fig(['~/Desktop/delay-' num2str(obj.systemDelay) '.jpg'], '-painters');
            
            %             % sfoae
            %             d = load('/Users/shenin/Dropbox/MATLAB/nseoae3.matlab/data.simon/sfoae/ST111011.L30.f800-3200.4s.avg.sf.db');
            %             axes(obj.OAEDisplay);
            %             hold on;
            %             plot(d(:,1), d(:,2), 'g'); hold on;
            %
            %             keyboard;
            
            
            obj = obj.postProcessing();
            % check if playback was not aborted, otherwise continue
            % Not sure if this is needed, forcing post-processing above
%             if ~obj.playback,   % playback set to 0 at end of each sequence perform post processing
%                 obj = obj.postProcessing();
%             end
        end
        
        % handles and updates the realtime run counter
        function obj = record(obj)
            % display the recording number and call the recording callback
            % fprintf(1,'recording run #%i, buffer:%i \n', obj.rep, obj.Buffer);
            obj.runcounter = obj.runcounter + 1;
            str = sprintf('Run # %i / %i', obj.runcounter, obj.runtotal);
            set(obj.RTcounter, 'string', str);
            obj = feval(obj.daqCallback, obj);
        end
        
        
                % simple error checking
        function errors = validateSequence(obj, N)
            % quick & dirty error checking - This should be refined so it
            % doesn't cause the first error if one of the fields in a OaE
            % module is undefined (NaN) thus no sweep is added. We
            % should check pass it back to validate sequence and which can
            % have a varargin to spit out a message that says what field
            % should be populated. 
            % ----------------------------
            errors = 0;
            if ( obj.nOutput ~= size(obj.stimulus,2) ),
                err_str = sprintf('Wrong number of output channels for this protocol.\nPlease reconfigure your device setting with %i channels', size(obj.stimulus,2));
                oae_error(err_str);
                errors = errors+1;
                return;
            end
            % check for valid daq callback
            if isempty(obj.daqCallback),
                err_str = sprintf('Invalid sequence (%i). I/O has not been properly configured.', N);
                oae_error(err_str);
                errors = errors+1;
                return;
            end
            % check for appropriate number of v2pa values
            if isempty(obj.v2pa) | (length(obj.v2pa) < max(obj.OutputChannels)),
                err_str = sprintf('Output sensivity has not been calibrated for output channels in this sequence. Please undate output sensitivity using the ''System Calbiration'' module');
                oae_error(err_str);
                errors = errors+1;
                return;
            end
%             % 9-27-16: Moved to analysis, SH            
%             % check for valid ar callback
%             if isempty(obj.artifactrejectionCallback),
%                 err_str = sprintf('Invalid sequence (%i). Please configure artifact rejection.', N);
%                 oae_error(err_str);
%                 errors = errors+1;
%             end
        end
        % simple controller checking
        function errors = validateController(obj)
            errors = 0;
            if isempty(obj.v2pa),
                err_str = {'Your system''s output sensitivity has not be calibrated!' '' 'Please update your microphone calibration settings using the "Calibration->System Calibration" module'};
                oae_error(err_str, 'Uncalibrated Output');
                errors = errors+1;
            end
        end
        
        %-- REALTIME GUI --%
        % checks pause button and pauses if down
        function obj = pauser(obj)
            if get(obj.pauseButton, 'Value') == 1                           % Pause togglebutton down, system is paused
                drawnow;
                obj.pauseState = 1;
                obj.rep = 1;
                obj.runcounter = 0;
                set(obj.pauseButton, 'backgroundcolor', 'red', 'string', 'Restart Sequence');
                drawnow;                                                    % If drawnow is not used the buttonState is not updated and the object stays the same.
            elseif get(obj.pauseButton, 'Value') == 0                       % Pause togglebutton up, system is recording
                drawnow;
                obj.pauseState = 0;
                set(obj.pauseButton, 'backgroundcolor', 'green', 'string', 'Pause at end of sweep');
                drawnow;                                                      % drawnow was necessary to get this to work.
            end
            while obj.pauseState
                drawnow;
                if get(obj.pauseButton, 'Value') == 0                       % Pause togglebutton up, system is recording
                    obj.pauseState = 0;
                    set(obj.pauseButton, 'backgroundcolor', 'green', 'string', 'Pause at end of sweep');
                    drawnow;                                                      % drawnow was necessary to get this to work.
                end
                
            end
        end
        
        % intializes and sets up the realtime GUI
        function obj = initializeGUI(obj)
            obj.runtimeFig = oae_makeruntimegui();
            rtFigHandle = figure(obj.runtimeFig);
            padding = 20;  top = 0.9;
            
            %colors = [0 0 1; 0 1 0; 1 0 0; 1 1 0; 0 1 1; 1 0 1; 0 0 0]; % TODO: should add this to defaults
            colors = obj.linecolors;
            if size(colors,1) < obj.nBuffer,
                colors = jet(obj.nBuffer);
            end
            %set(rtFigHandle,'CloseRequestFcn',@oae_noClosefcn);
            
            % run counter
            % -----------
            obj.runcounter = 0;
            obj.runtotal = obj.nRepetitions*obj.nBuffer*obj.nCondition;
            str = sprintf('Run # %i / %i', obj.runcounter, obj.runtotal);
            obj.pauseButton = oae_uicomponent('style', 'togglebutton', 'string' , 'Pause at end of sweep', 'units', 'normalized',...
                'position', [0.05 1-0.065 0.2 0.05], 'horizontalalignment', 'left', 'fontsize', 13);  % 'callback',@(src,event)togglePause(obj,src,event))
            obj.RTcounter = oae_uicomponent('style', 'text', 'string', str, 'units', 'normalized','position', [0.25 1-0.1 0.1 0.08], 'horizontalalignment', 'left', 'fontsize', 14);
            
            oae_uicomponent('style', 'text', 'string', sprintf('%s (%s)',obj.paradigmLabel, obj.protocol), 'units', 'normalized','position', [0.5-0.1 1-0.1 0.2 0.08], 'horizontalalignment', 'center', 'fontsize', 14);
            % display some options
            if obj.displayRT,
                rtmode = 'on'; else rtmode = 'off'; end
                
            oae_uicomponent('style', 'text', 'string', sprintf('sampling rate: %i, realtime mode: %s',obj.fs, rtmode), 'units', 'normalized','position', [0.7-0.1 1-0.1 0.2 0.08], 'horizontalalignment', 'center', 'fontsize', 12);
            
            % realtime AR threshold control
            %obj.rtThreshText = oae_uicomponent('style', 'text', 'string', 'RT AR Threshold (dB)', 'units', 'normalized','position', [0.5 1-0.07 0.2 0.03], 'horizontalalignment', 'left', 'fontsize', 13);
            %rtARthreshold\
            %ARstr = sprintf('%d', obj.rtARthreshold);
            %obj.rtThreshEdit = oae_uicomponent('style', 'edit', 'string', ARstr, 'tag', 'rtThreshEditBox', 'units', 'normalized','position', [0.7 1-0.07 0.075 0.03], 'horizontalalignment', 'left', 'fontsize', 11);
            
            % Make subplot for each condition
            % -------------------------------
            if obj.displayRT,
                for i=1:obj.nCondition,
                    obj.OAEDisplay(i) = axes('Position', [0.05 top-(0.8/obj.nCondition)-(i-1)*(0.8/obj.nCondition) 0.85 .8/obj.nCondition], 'tag', 'oae_control.axis');
                    
                    set(obj.runtimeFig, 'currentaxes', obj.OAEDisplay(i));
                    %set(gca, 'drawmode', 'fast', 'xlim', obj.rtXlim, 'ylim', obj.rtYlim, 'tag', 'OAEDisplay', 'XScale', 'log'); grid on;
                    set(gca, 'xlim', obj.rtXlim, 'ylim', obj.rtYlim, 'tag', 'OAEDisplay', 'XScale', 'log'); grid on;
                    set(get(gca, 'ylabel'), 'string', 'Amplitude (dB SPL)', 'fontsize', 14, 'fontweight', 'bold');
                    if i == obj.nCondition,
                        set(get(gca, 'xlabel'), 'string', 'Frequency (Hz)', 'fontsize', 14, 'fontweight', 'bold');
                    end
                    str = {};
                    for j=1:obj.nBuffer,
                        obj.OAELines(1,i,j) = line(obj.rtX(1,:,j,i), obj.rtY(1,:,j,i));
                        %set(obj.OAELines(1,i,j), 'color', colors(j,:), 'erasemode', 'background');  % none is fastest - but must use refresh after drawnow, 'background' is next fastest, no real need to refresh.
                        set(obj.OAELines(1,i,j), 'color', colors(j,:));  % erasemode no longer supported
                        
                        obj.OAELines(2, i,j) = line(obj.rtX(2,:,j,i), obj.rtY(2,:,j,i));
                        %set(obj.OAELines(2, i,j), 'color', colors(j,:), 'linestyle', '--', 'erasemode', 'background');
                        set(obj.OAELines(2, i,j), 'color', colors(j,:), 'linestyle', '--');
                        
                        % primaries...REMOVE this is final version
                        %                     for n=1:obj.nPrimaries,
                        %                         obj.OAELines(2+n, i, j) = line(obj.rtX(2,:,j,i), obj.rtY(2,:,j,i));
                        %                         set(obj.OAELines(2+n, i,j), 'color', 'k', 'erasemode', 'background');
                        %                     end
                        
                        
                        str = [str(:)' sprintf('buffer %i', j)];
                    end
                end
            else
                % No realtime analysis of OAEs, but let's at least display
                % the recordings for each condition and buffer
                for i=1:obj.nCondition,
                    obj.OAEDisplay(i) = axes('Position', [0.05 top-(0.8/obj.nCondition)-(i-1)*(0.8/obj.nCondition) 0.8 .8/obj.nCondition], 'tag', 'oae_control.axis');
                    
                    set(obj.runtimeFig, 'currentaxes', obj.OAEDisplay(i));
                    set(gca, 'drawmode', 'fast', 'tag', 'OAEDisplay'); grid on;
                    set(get(gca, 'ylabel'), 'string', 'Amplitude (V)');
                    if i == obj.nCondition,
                        set(get(gca, 'xlabel'), 'string', 'Time (s)');
                    end
                    str = {};
                    for j=1:obj.nBuffer,
                        obj.OAELines(1,i,j) = line(obj.rtX(1,:,j,i), obj.rtY(1,:,j,i));
                        %set(obj.OAELines(1,i,j), 'color', colors(j,:), 'erasemode', 'background');  % none is fastest - but must use refresh after drawnow, 'background' is next fastest, no real need to refresh.
                        set(obj.OAELines(1,i,j), 'color', colors(j,:)); 
                    end
                end
            end
            % VU meter
            obj.VUMeter = axes('Position', [0.92 .1 0.05 0.8], 'tag', 'vu_control.axis');
            set(obj.runtimeFig, 'currentaxes', obj.VUMeter);
            %set(gca, 'drawmode', 'fast', 'xlim', [-0.5 0.5], 'xticklabel', {''}, 'ylim', obj.rtYlim, 'tag', 'VUMeter');
            set(gca, 'xlim', [-0.5 0.5], 'xticklabel', {''}, 'ylim', obj.rtYlim, 'tag', 'VUMeter');
            obj.VUMeter = patch('Xdata', [-0.4 -0.4 0.4 0.4], 'Ydata', [obj.rtYlim(1) 0 0 obj.rtYlim(1)], 'FaceColor', [0.8500    0.3250    0.0980], 'EdgeColor', [0.8500    0.3250    0.0980]);
            hold on;
            % create bar for the realtime AR rejection threshold
            obj.rtThreshPatch = patch('Xdata', [-0.49 -0.49 0.5 0.5], 'Ydata', [obj.rtARthreshold+.5 obj.rtARthreshold-.5 obj.rtARthreshold-.5 obj.rtARthreshold+.5], 'FaceColor', 'k', 'EdgeColor', 'k', 'tag', 'ThreshPatch', 'buttonDownFcn',@oae_startDragFcn);
            set(gcf,'WindowButtonUpFcn',@oae_stopDragFcn, 'backingstore', 'off');  % the backingstore off may increase realtime speed.
        end
        
        % REAL TIME GUI PROCESSING
        % peforms realtime data processing and LSF, artifact rejection and gui updatdes etc
        function obj = updateGUI(obj, data)
            
            
            if size(data,2) > size(data,1), data = data'; end
            
            % TODO: Add VU meter for discrete tones (+ stimulus analysis?)
            % TODO: Remove VU meter when portaudio 3 is used or displayRT =
            % false
            if obj.displayVU && obj.displayRT
                
               
                %------------APPLY MICROPHONE EQUALIZATION---------------
                % data = obj.rtMicConv(data);                           % TODO Verify:
                
                % Realtime lsf analysis of sweeps or tones (DPOAEs/SFOAEs)
                if obj.realTime
                    
                    %------------Update rtBuffer Counter-----------------------
                    obj.rtBuffer = obj.rtBuffer +1;
                    
                    % ------------RT AR Threshold----------------------------
                    %obj.rtARthreshold = str2double(get(obj.rtThreshEdit, 'string'));
                    
                    yy = get(findobj(obj.rtThreshPatch, 'tag', 'ThreshPatch'), 'ydata');  % why is yy a cell for second sequence?
                    obj.rtARthreshold = (yy(1)+yy(2))./2;
                    
                    
                    % Debugging
                    % ---------
                    % obj.gain = 1; obj.systemDelay = 22.7;
                    % data = obj.dd((obj.rtBuffer-1)*obj.bufferSize+1:(obj.rtBuffer)*obj.bufferSize);
                    
                    
                    %                 % manual overlap & add
                    %                 f_len = (obj.micCalibrationFilterLength-1);
                    %                 if obj.rtBuffer == 1,
                    %                     data = data(f_len/2+1:end);
                    %                     obj.rtData( (obj.rtBuffer-1)*obj.bufferSize+1:(obj.rtBuffer)*obj.bufferSize+(f_len/2), obj.Buffer, obj.Condition) = double(data)./obj.gain./obj.sensitivity./2e-5;
                    %                 else
                    %                     obj.rtData( (obj.rtBuffer-1)*obj.bufferSize-f_len+1:(obj.rtBuffer)*obj.bufferSize, obj.Buffer, obj.Condition) = obj.rtData( (obj.rtBuffer-1)*obj.bufferSize-f_len+1:(obj.rtBuffer)*obj.bufferSize, obj.Buffer, obj.Condition) + double(data)./obj.gain./obj.sensitivity./2e-5;
                    %                 end
                    
                    % rtData is concatenated array of data - until it's long
                    % enough we don't process with the lsf
                    obj.rtData( (obj.rtBuffer-1)*obj.rtBufferSize+1:(obj.rtBuffer)*obj.rtBufferSize, obj.Buffer, obj.Condition) = double(data)./obj.gain./obj.sensitivity./2e-5;
                    
                    
                    % ---------------------------------------------------------
                    % check to see if where ok to analyze: is rtData >
                    % required frame size & check ifthe nTime is the Step size
                    % and current starting index so nTime+Nfilt is < filled
                    % buffer (do we have room to do an Nfilt.
                    % ---------------------------------------------------------
                    if ( (obj.rtBuffer*obj.rtBufferSize > obj.rtNfilt+obj.systemDelay+obj.onsetDelay) && (obj.nTime+obj.rtNfilt-obj.offsetDelay < obj.rtBuffer*obj.rtBufferSize) )
                        % -----------------------------------------------------
                        % avgData is a subset of rtData that is the size of Nfilt, the step size is rtNfilt.
                        % -----------------------------------------------------
                        
                        while ( (obj.nTime+obj.rtNfilt+obj.systemDelay+obj.onsetDelay < obj.rtBuffer*obj.rtBufferSize) && (obj.nTime+obj.rtNfilt <= obj.T*obj.fs) )
                            
                            % -------------------------------------------------
                            % calculate current LSF frame time (t), and frame
                            % samples (n)
                            % -------------------------------------------------
                            
                            % t = ((obj.nTime:obj.nTime+obj.rtNfilt-1) ./ obj.fs) ; % only used for oae_lsf_feval
                            nDelay = (obj.nTime+1:obj.nTime+obj.rtNfilt);
                            n = nDelay + obj.systemDelay + obj.onsetDelay;
                            
                            %fprintf(1, 'enough data on buffer number %i, n start=%i, n end %i, t start: %i, t end: %i\n', obj.rtBuffer, n(1), n(end), t(1), t(end));
                            %fprintf(1, 'nTime: %f, n: %i, nDelay: %i, t: %f , nT: %i \n', obj.nTime, n(1), nDelay(1), t(1), obj.nT);
                            
                            dataFrame = obj.rtData( n,  obj.Buffer,  obj.Condition );
                            
                            % lsf with feval...less overhead, but slightly slower
                            % ------------------------------------------------------
                            %[f coefs Residual] = oae_lsf_feval([obj.rtFFunc(:,obj.Buffer,obj.Condition)'], [obj.rtPhiFunc(:, obj.Buffer, obj.Condition )'], [obj.Delay(nDelay, :, obj.Buffer, obj.Condition)], obj.rtNfilt, t, dataFrame, obj.rtWvec);
                            
                            
                            % lsf without feval...faster, but more overhead
                            % ---------------------------------------
                            [coefs Residual] = oae_lsf(obj.rtPhi(nDelay, :, obj.Buffer, obj.Condition ), obj.Delay(nDelay, :, obj.Buffer, obj.Condition), obj.rtNfilt, dataFrame, obj.rtWvec);
                           % f = obj.rtPhiF(floor(median(n)), :, obj.Buffer,obj.Condition);  % the old way as of 8/21/2012 JJH
                           f = obj.rtPhiF(floor(median(nDelay)), :, obj.Buffer, obj.Condition);  % don't want to compensate for system shift and delay here becuse PhiF is not
                            
                            %trueResidual = Residual./obj.rtWvec';          % remove window, NOTE: this artificially increases over level of residual since coeffs determined with window
                            
                            bpLL = f(end)-f(end)*0.1;                        % component frequency
                            bpUL = f(end)+f(end)*0.1;
                            [rms_error Residual] = bpFilterRMS(Residual, bpLL, bpUL, 2, obj.fs );
                            
                            %obj.rtResidual(nDelay,obj.Buffer, obj.Condition) = trueResidual;
                            
                            % -------------------------------------------------
                            % Update the VU Meter
                            % -------------------------------------------------
                            error_rms = 20*log10( rms_error );
                            set(obj.VUMeter, 'ydata', [ -180 error_rms error_rms -180 ]); % update patch object
                            %set(obj.rtThreshPatch, 'ydata', [ obj.rtARthreshold+.25 obj.rtARthreshold-.25 obj.rtARthreshold-.25 obj.rtARthreshold+.25]);  % update the AR threshold bar
                            
                            % -------------------------------------------------
                            % Decide to include the data in the average based
                            % on AR threshold which here uses the windowed
                            % residual
                            % -------------------------------------------------
                            
                            pcoefs = coefs(1:obj.rtNPhi*2);
                            coefs = coefs( end-1:end );  % get only the oae coefs
                            if error_rms  < obj.rtARthreshold
                                % ---------------------------------------------
                                % Keep the frame
                                % ---------------------------------------------
                                if isnan(obj.rtAvgCoefs(:, obj.nT , obj.Buffer, obj.Condition ))
                                    obj.rtPrimaryCoefs(:, obj.nT , obj.Buffer, obj.Condition ) =  pcoefs;
                                    obj.rtAvgCoefs(:, obj.nT , obj.Buffer, obj.Condition ) =  coefs;
                                    obj.rtAvgResidual ( nDelay, obj.Buffer, obj.Condition ) = Residual;
                                    obj.rtFrameWeights( obj.nT, obj.Buffer, obj.Condition ) = 1;
                                end
                            elseif error_rms  >= obj.rtARthreshold
                                % ---------------------------------------------
                                % Reject data frame
                                % residual stays the same as previous sweeps
                                % because we don't want current
                                % residual to be included in the average
                                % ---------------------------------------------
                                Residual = obj.rtAvgResidual( nDelay, obj.Buffer, obj.Condition );
                                coefs = obj.rtAvgCoefs( :, obj.nT, obj.Buffer, obj.Condition);
                            end
                            obj.rtFrameWeights( obj.nT, obj.Buffer, obj.Condition ) = obj.rtFrameWeights( obj.nT, obj.Buffer, obj.Condition ) + 1;
                            
                            % -------------------------------------------------
                            % average the residuals so it reflects the noisefloor
                            % going down via averaging
                            % -------------------------------------------------
                            obj.rtAvgResidual ( nDelay, obj.Buffer, obj.Condition ) = ...
                                (obj.rtAvgResidual( nDelay, obj.Buffer, obj.Condition )...
                                *(obj.rtFrameWeights( obj.nT, obj.Buffer, obj.Condition)-1)...
                                + Residual)./obj.rtFrameWeights( obj.nT, obj.Buffer, obj.Condition );
                            
                            % -------------------------------------------------
                            % calculate the RMS noise from the residual in dB SPL
                            % -------------------------------------------------
                            obj.rtAvgRMSNoise(obj.nT, obj.Buffer, obj.Condition) = sqrt(mean(obj.rtAvgResidual( nDelay , obj.Buffer, obj.Condition ).^2 ) );
                            
                            % -------------------------------------------------
                            % Running average of the coefs
                            % -------------------------------------------------
                            %                         obj.rtAvgCoefs(:, obj.nT , obj.Buffer, obj.Condition ) = ...
                            %                             (obj.rtAvgCoefs( :, obj.nT , obj.Buffer, obj.Condition )...
                            %                             *((obj.rtFrameWeights( obj.nT, obj.Buffer, obj.Condition)-1))+ ...
                            %                             coefs)./obj.rtFrameWeights(obj.nT);
                            
                            obj.rtAvgCoefs(:, obj.nT , obj.Buffer, obj.Condition ) = ...
                                (obj.rtAvgCoefs( :, obj.nT , obj.Buffer, obj.Condition )...
                                *((obj.rtFrameWeights( obj.nT, obj.Buffer, obj.Condition)-1))+ ...
                                coefs)./obj.rtFrameWeights(obj.nT, obj.Buffer, obj.Condition);
                            
                            avgCoefs = obj.rtAvgCoefs( :, obj.nT , obj.Buffer, obj.Condition );
                            
%                             obj.rtPrimaryCoefs(:, obj.nT , obj.Buffer, obj.Condition ) = ...
%                                 (obj.rtPrimaryCoefs( :, obj.nT , obj.Buffer, obj.Condition )...
%                                 *((obj.rtFrameWeights( obj.nT, obj.Buffer, obj.Condition)-1))+ ...
%                                 pcoefs)./obj.rtFrameWeights(obj.nT, obj.Buffer, obj.Condition);
                            
                            % -------------------------------------------------
                            % update X/Y data of OAE & residual
                            % -------------------------------------------------
                            mL = length(obj.rtFrqMicCalibrationFilterDB);
                            if mL > 1,
                                fvec = linspace(0,obj.fs/2,mL);
                                micCorrdB = obj.rtFrqMicCalibrationFilterDB(find(fvec > f(end), 1,'first'));
                            else
                                micCorrdB = 0;
                            end
                            
                            obj.rtX(1,obj.nT, obj.Buffer, obj.Condition) = f(end);
                            obj.rtY(1,obj.nT, obj.Buffer, obj.Condition) = 20*log10(hypot(avgCoefs(1), avgCoefs(2))) + micCorrdB;
                            % residual
                            obj.rtX(2,obj.nT, obj.Buffer, obj.Condition) = f(end);
                            obj.rtY(2,obj.nT, obj.Buffer, obj.Condition) = 20*log10( obj.rtAvgRMSNoise( obj.nT, obj.Buffer, obj.Condition )) + micCorrdB;
                            
                            
                            %                         nn = 1;
                            %                         for n=1:obj.nPrimaries,
                            %                             obj.rtX(2+n,obj.nT, obj.Buffer, obj.Condition) = f(n);
                            %                             obj.rtY(2+n,obj.nT, obj.Buffer, obj.Condition) = 20*log10(hypot(obj.rtPrimaryCoefs(nn, obj.nT , obj.Buffer, obj.Condition ), obj.rtPrimaryCoefs(nn+1, obj.nT , obj.Buffer, obj.Condition )));
                            %                             nn = nn+2;
                            %                         end
                            
                            % -------------------------------------------------
                            % Increment the current LSF time (nTime) + LSF index (nT)
                            % -------------------------------------------------
                            obj.nTime = obj.nTime + obj.rtNfilt;   % initialized to 0
                            obj.nT = obj.nT + 1;                   % initialized to 1
                        end
                        % -----------------------------------------------------
                        % Update line objects
                        % -----------------------------------------------------
                        set(obj.OAELines(1, obj.Condition, obj.Buffer), 'xdata', obj.rtX(1, :,obj.Buffer, obj.Condition), 'ydata', obj.rtY(1, :,obj.Buffer, obj.Condition));
                        set(obj.OAELines(2, obj.Condition, obj.Buffer), 'xdata', obj.rtX(2, :,obj.Buffer, obj.Condition), 'ydata', obj.rtY(2, :,obj.Buffer, obj.Condition));
                        %                     for n=1:obj.nPrimaries,
                        %                     set(obj.OAELines(2+n, obj.Condition, obj.Buffer), 'xdata', obj.rtX(2+n, :,obj.Buffer, obj.Condition), 'ydata', obj.rtY(2+n, :,obj.Buffer, obj.Condition));
                        %                     end
                        drawnow limitrate;
                    end
                else
                    % this is sweep-by-sweep                                                                                                                                                                                        
                    % specific to portaudio3
                    % ---------------------------------------------------
                    rtNstep = floor(obj.rtNfilt*obj.rtnstep);   % 10 percent of the rtNfilt window
                                                                                                                                                                                                 
                    obj.rtData( 1:length(data), obj.Buffer, obj.Condition) = double(data)./obj.gain./obj.sensitivity./2e-5;
                    %ntotal = floor((nsize-nfilt)/nstep) + 1;
                    %ntotal = floor((length(data)-obj.systemDelay)/obj.rtNfilt); % original way (no nstep)
                    ntotal = floor((length(data)-obj.systemDelay-obj.rtNfilt)/rtNstep) + 1;
                    for i=1:ntotal
                       
                        t = ((obj.nTime:obj.nTime+obj.rtNfilt-1) ./ obj.fs) ;
                       
                        n = (obj.nTime+1:obj.nTime+obj.rtNfilt) + obj.systemDelay + obj.onsetDelay;
                        nDelay = (obj.nTime+1:obj.nTime+(obj.rtNfilt));
                        
                        %fprintf(1, 'enough data on buffer number %i, n start=%i, n end %i, t start: %i, t end: %i\n', obj.rtBuffer, n(1), n(end), t(1), t(end));
                        %fprintf(1, 'nTime: %f, n: %i, nDelay: %i, t: %f , nT: %i \n', obj.nTime, n(1), nDelay(1), t(1), obj.nT);
                        
                        dataFrame = obj.rtData( n,  obj.Buffer,  obj.Condition );

                        %--- SH, not using this anymore in favor of using
                        %pre-computed rtPhis. This also for more flexible
                        %sweep (e.g. using custom sweep definitions --%
%                         % lsf with feval...less overhead, but slightly slower
%                         % ---------------------------------------
%                         [f coefs Residual] = oae_lsf_feval([obj.rtFFunc(:,obj.Buffer,obj.Condition)'], [obj.rtPhiFunc(:, obj.Buffer, obj.Condition )'], [obj.Delay(nDelay, :, obj.Buffer, obj.Condition)], obj.rtNfilt, t, dataFrame, obj.rtWvec);
%                         coefs = real(coefs); % this prevents theLSF from returning imagnary values for NaN's JJH 3-31-2017
%                         Residual = real(Residual);
                        
                        % lsf without feval...faster, but more overhead
                        % ---------------------------------------
                        [coefs Residual] = oae_lsf(obj.rtPhi(nDelay, :, obj.Buffer, obj.Condition ), obj.Delay(nDelay, :, obj.Buffer, obj.Condition), obj.rtNfilt, dataFrame, obj.rtWvec);
                        % f = obj.rtPhiF(floor(median(n)), :, obj.Buffer,obj.Condition);  % the old way as of 8/21/2012 JJH
                        f = obj.rtPhiF(floor(median(nDelay)), :, obj.Buffer, obj.Condition);  % don't want to compensate for system shift and delay here becuse PhiF is not
                        coefs = real(coefs); % this prevents theLSF from returning imagnary values for NaN's JJH 3-31-2017
                        Residual = real(Residual');
                        f = f';
                        
                        %trueResidual = Residual./obj.rtWvec';                                    % remove window, NOTE: this artificially increases over level of residual since coeffs determined with window
                        bpLL =  f(end)-f(end)*0.1; % component frequency
                        bpUL =  f(end)+f(end)*0.1;
                        [rms_error Residual] = bpFilterRMS(Residual, bpLL, bpUL, 2, obj.fs );
                        
                        %obj.rtResidual(nDelay,obj.Buffer, obj.Condition) = trueResidual;
                        
                        error_rms = 20*log10( rms_error );
    
                        pcoefs = coefs(1:obj.rtNPhi*2);
                        coefs = coefs( end-1:end );  % get only the oae coefs
                        if error_rms  < obj.rtARthreshold
                            % ---------------------------------------------
                            % Keep the frame
                            % ---------------------------------------------
                            if isnan(obj.rtAvgCoefs(:, obj.nT , obj.Buffer, obj.Condition ))
                                obj.rtPrimaryCoefs(:, obj.nT , obj.Buffer, obj.Condition ) =  pcoefs;
                                obj.rtAvgCoefs(:, obj.nT , obj.Buffer, obj.Condition ) =  coefs;
                                obj.rtAvgResidual ( nDelay, obj.Buffer, obj.Condition ) = Residual;
                                obj.rtFrameWeights( obj.nT, obj.Buffer, obj.Condition ) = 1;
                                
                            end
                        elseif error_rms  >= obj.rtARthreshold
                            % ---------------------------------------------
                            % Reject data frame
                            % residual stays the same as previous sweeps
                            % because we don't want current
                            % residual to be included into the average
                            % ---------------------------------------------
                            Residual = obj.rtAvgResidual( nDelay, obj.Buffer, obj.Condition )';
                            coefs = obj.rtAvgCoefs( :, obj.nT, obj.Buffer, obj.Condition);
                        end
                        obj.rtFrameWeights( obj.nT, obj.Buffer, obj.Condition ) = obj.rtFrameWeights( obj.nT, obj.Buffer, obj.Condition ) + 1;
                        
                        % -------------------------------------------------
                        % average the residuals so it reflects the noisefloor
                        % going down via averaging
                        % -------------------------------------------------
                        obj.rtAvgResidual ( nDelay, obj.Buffer, obj.Condition ) = ...
                            (obj.rtAvgResidual( nDelay, obj.Buffer, obj.Condition )...
                            *(obj.rtFrameWeights( obj.nT, obj.Buffer, obj.Condition)-1)...
                            + Residual')./obj.rtFrameWeights( obj.nT, obj.Buffer, obj.Condition );
                        
%                         obj.rtAvgResidual ( nDelay, obj.Buffer, obj.Condition ) = ...
%                                 (obj.rtAvgResidual( nDelay, obj.Buffer, obj.Condition )...
%                                 *(obj.rtFrameWeights( obj.nT, obj.Buffer, obj.Condition)-1)...
%                                 + Residual)./obj.rtFrameWeights( obj.nT, obj.Buffer, obj.Condition );
                            
                        
                        % -------------------------------------------------
                        % calculate the RMS noise from the residual in dB SPL
                        % -------------------------------------------------
                        obj.rtAvgRMSNoise(obj.nT, obj.Buffer, obj.Condition) = sqrt(mean(obj.rtAvgResidual( nDelay , obj.Buffer, obj.Condition ).^2 ) );
                        
                        % -------------------------------------------------
                        % Running average of the coefs
                        % -------------------------------------------------
                        %                         obj.rtAvgCoefs(:, obj.nT , obj.Buffer, obj.Condition ) = ...
                        %                             (obj.rtAvgCoefs( :, obj.nT , obj.Buffer, obj.Condition )...
                        %                             *((obj.rtFrameWeights( obj.nT, obj.Buffer, obj.Condition)-1))+ ...
                        %                             coefs)./obj.rtFrameWeights(obj.nT);
                        
                        obj.rtAvgCoefs(:, obj.nT , obj.Buffer, obj.Condition ) = ...
                            (obj.rtAvgCoefs( :, obj.nT , obj.Buffer, obj.Condition )...
                            *((obj.rtFrameWeights( obj.nT, obj.Buffer, obj.Condition)-1))+ ...
                            coefs)./obj.rtFrameWeights(obj.nT, obj.Buffer, obj.Condition);
                        
                        avgCoefs = obj.rtAvgCoefs( :, obj.nT , obj.Buffer, obj.Condition );
                        
                        %                         obj.rtPrimaryCoefs(:, obj.nT , obj.Buffer, obj.Condition ) = ...
                        %                             (obj.rtPrimaryCoefs( :, obj.nT , obj.Buffer, obj.Condition )...
                        %                             *((obj.rtFrameWeights( obj.nT, obj.Buffer, obj.Condition)-1))+ ...
                        %                             pcoefs)./obj.rtFrameWeights(obj.nT);
                        
                        obj.rtPrimaryCoefs(:, obj.nT , obj.Buffer, obj.Condition ) = ...
                            (obj.rtPrimaryCoefs( :, obj.nT , obj.Buffer, obj.Condition )...
                            *((obj.rtFrameWeights( obj.nT, obj.Buffer, obj.Condition)-1))+ ...
                            pcoefs)./obj.rtFrameWeights(obj.nT, obj.Buffer, obj.Condition);
                        
                        % -------------------------------------------------
                        % update X/Y data of OAE & residual
                        % -------------------------------------------------
                        mL = length(obj.rtFrqMicCalibrationFilterDB);
                        if mL > 1,
                            fvec = linspace(0,obj.fs/2,mL);
                            micCorrdB = obj.rtFrqMicCalibrationFilterDB(find(fvec > f(end), 1,'first'));
                        else
                            micCorrdB = 0;
                        end
                        
                        obj.rtX(1,obj.nT, obj.Buffer, obj.Condition) = f(end);
                        obj.rtY(1,obj.nT, obj.Buffer, obj.Condition) = 20*log10(hypot(avgCoefs(1), avgCoefs(2))) + micCorrdB;
                        % residual
                        obj.rtX(2,obj.nT, obj.Buffer, obj.Condition) = f(end);
                        obj.rtY(2,obj.nT, obj.Buffer, obj.Condition) = 20*log10( obj.rtAvgRMSNoise( obj.nT, obj.Buffer, obj.Condition )) + micCorrdB;
                       
                        obj.nTime = obj.nTime + rtNstep;       % initialized to 0  % modified from obj.rtNfilt to rtNstep; JJH
                        obj.nT = obj.nT + 1;                   % initialized to 1
                    end
                    set(obj.OAELines(1, obj.Condition, obj.Buffer), 'xdata', obj.rtX(1, :,obj.Buffer, obj.Condition), 'ydata', obj.rtY(1, :,obj.Buffer, obj.Condition));
                    set(obj.OAELines(2, obj.Condition, obj.Buffer), 'xdata', obj.rtX(2, :,obj.Buffer, obj.Condition), 'ydata', obj.rtY(2, :,obj.Buffer, obj.Condition));
                    drawnow;
                    refresh;
                end
            elseif ~obj.isCustom
                % displayRT = false
                % Let's just display the waveform in the GUI
                t = (0:length(data)-1)./obj.fs;
                maxY = max(data);
                if (maxY > max(obj.rtYlim)) || isempty(obj.rtYlim),
                    obj.rtYlim = [-maxY maxY];
                    set(obj.OAEDisplay(obj.Condition), 'ylim', obj.rtYlim);
                end
                set(obj.OAEDisplay(obj.Condition), 'xlim', [0 t(end)]);
                set(obj.OAELines(1, obj.Condition, obj.Buffer), 'xdata', t, 'ydata', data);
                drawnow; refresh;
            end
        end
  
        % realtime update of the OAE
        function obj = updateOAE(obj)
            
            obj.rtFrame = obj.rtFrame +1;
            bufferAvg = (data + obj.rtData( (obj.rtFrame- 1)*obj.bufferSize+1:(obj.rtFrame)*obj.bufferSize, obj.Buffer, obj.Condition)) ./ obj.rep;
            obj.rtData((obj.rtFrame- 1)*obj.bufferSize+1:(obj.rtFrame)*obj.bufferSize,obj.Buffer, obj.Condition) = bufferAvg;
            
        end
        % set an object property using this method
        function obj = setProp(obj, propertyname, value)
            if isempty(strcmp(propertyname, properties(obj))),
                disp('property does not exist.');
            else
                eval(['obj.' propertyname '=' 'value' ';']);
            end
        end
        
        % Get parameter variables
        function value = getProp(obj, propertyname)
            if isempty(strcmp(propertyname, properties(obj))),
                disp('property does not exist.');
            else
                value = eval(['obj.' propertyname]);
                switch lower(propertyname),
                    case {'outputchannels' 'inputchannels'}
                    case {'stimulus'}
                        disp('Warning: Please use obj.getCurrentStimulus for property type: "stimulus"')    % warning
                    otherwise
                        %TODO: figure out what the lines below were meant
                        %to do. I think to retrieve current stimulus, but
                        %there's a method for that. Removing this for now,
                        %because it breaks when trying to return numerical
                        %data
%                         if ~ischar(value),
%                             if max(size(value)) > 1,
%                                 value = value(:,obj.Buffer, obj.Condition);   % This does not work for stimulus because sitmulus is [ nSampple x nCh x nBuffer x nCond]
%                             end
%                         end
                end
            end
        end
        
        %initialize the sequence, reset buffers, etc...
        function obj = initializeSequence(obj)
            obj.nBuffer         = 0;
            obj.Buffer          = 0;
            obj.bufferLabels    = {};
            obj.dataFileNames   = {};
            obj.nCondition      = 0;
            obj.Condition       = 0;
            obj.conditionLabels = {};
            obj.stimulus        = [];
            obj.stim            = [];
            obj.data            = [];
            obj.fstart          = [];
            obj.fend            = [];
            obj.L               = []; 
            obj.onsetDelay      = 0;
            obj.offsetDelay     = 0;
        end
        
        % add a Sweep
        function obj = addSweep(obj, L, fstart, fend, T)
            % check for negative values
            if (fstart < 0 | fend < 0 | T < 0)
                oae_error('Sweep cannot negative values');
                return;
            end
            % add a sweep buffer
            if (obj.nCondition == 0),
                obj.nCondition = 1; obj.Condition = 1;
                obj.conditionLabels = {'Condition 1'};
                obj.paradigmType = 'logsweep';
            end
            obj.nBuffer = obj.nBuffer + 1; obj.Buffer = 1;
            obj.bufferLabels = [obj.bufferLabels(:) {sprintf('buffer %i', obj.nBuffer)}];
            %obj.bufferLabels = {obj.bufferLabels{:} sprintf('buffer %i', obj.nBuffer)};
            for i=1:length(L),
                % obj.stimulus(:, i, obj.nBuffer, obj.Condition) = ramp((10.^(L(i)/20)).*obj.pref.*oae_logsweep(fstart(i),fend(i), T, obj.fs), obj.rampTime, obj.fs ) .* obj.v2pa;
                
                % It is possible that v2pa has not been calibrated by the
                % user yet, so just scale by hypothetical level. Multiply
                % all stimuli by v2pa in preprocessing/pre-flight routine.
                obj.stimulus(:, i, obj.nBuffer, obj.Condition) = ramp((10.^(L(i)/20)).*obj.pref.*oae_logsweep(fstart(i),fend(i), T, obj.fs), obj.rampTime, obj.fs );
                %obj.stimulus(:, i, obj.nBuffer, obj.Condition) = obj.stimulus(:, i, obj.nBuffer, obj.Condition)./sqrt(mean(obj.stimulus(:, i, obj.nBuffer, obj.Condition).^2));
                obj.fstart(i,obj.nBuffer,obj.nCondition) = fstart(i);
                obj.fend(i,obj.nBuffer,obj.nCondition) = fend(i);
                obj.L(i, obj.nBuffer,obj.nCondition) = L(i);
            end
            obj.T = T;
        end
        
        % add a Tone
        function obj = addTone(obj, L, f, T)
            % check for negative values
            if (f < 0 | T < 0)
                oae_error('Tone cannot negative values');
                return;
            end
            % add a sweep buffer
            if (obj.nCondition == 0),
                obj.nCondition = 1; 
                obj.Condition = 1;
                obj.conditionLabels = {'Condition 1'};
                obj.paradigmType = 'tone';
            end
            obj.nBuffer = obj.nBuffer + 1; 
            obj.Buffer = 1;
            obj.bufferLabels = [obj.bufferLabels(:); {sprintf('buffer %i', obj.nBuffer)}];
            %obj.bufferLabels = {obj.bufferLabels{:} sprintf('buffer %i', obj.nBuffer)};
            for i=1:length(L),
                % obj.stimulus(:, i, obj.nBuffer, obj.Condition) = ramp((10.^(L(i)/20)).*obj.pref.*oae_tone(f(i), T, obj.fs), obj.rampTime, obj.fs ) .* obj.v2pa;
                % See comments on lines 1164-1166 above
                obj.stimulus(:, i, obj.nBuffer, obj.Condition) = ramp((10.^(L(i)/20)).*obj.pref.*oae_tone(f(i), T, obj.fs), obj.rampTime, obj.fs );
                %obj.stimulus(:, i, obj.nBuffer, obj.Condition) = obj.stimulus(:, i, obj.nBuffer, obj.Condition)./sqrt(mean(obj.stimulus(:, i, obj.nBuffer, obj.Condition).^2));
                obj.fstart(i,obj.nBuffer,obj.nCondition) = f(i);
                obj.L(i, obj.nBuffer,obj.nCondition) = L(i);
            end
            obj.T = T;
        end
        
        % add a custom stimulus which is defined by a phase function, 
        % y = ramp( L * cos( phi ) )
        function obj = addCustomStimulus(obj, phi, L, rampTime)
            % check for bad inputs
            if ~exist('phi','var')|isempty(phi),
                oae_error('Custom stimulus requires a phase function definition');
                return;
            end
            if ~exist('L','var')|isempty(L),
                oae_error('Custom stimulus requires a level, L');
                return;
            end
            if ~exist('rampTime','var')|isempty(rampTime),
                rampTime = obj.rampTime; % use default ramp time
            else
                obj.rampTime = rampTime;
            end
            if length(L)>1 & (length(L)~= length(phi)),
                oae_error('Invalid level definition for custom stimulus, requires length(L) == 1 | length(L) == length(phi).');
                return;
            end
            if length(L) > 1 & size(L,1)~=size(phi,1),
                % make sure dimensions of L and phi are matching
                phi = phi';
            end
            if size(phi,2)>size(phi,1),
                % make into column vectors
                L = L';
                phi = phi';
            end
            
            % add a sweep buffer
            if (obj.nCondition == 0),
                obj.nCondition = 1; obj.Condition = 1;
                obj.conditionLabels = {'Condition 1'};
                obj.paradigmType = 'custom';
            end
            obj.nBuffer = obj.nBuffer + 1; obj.Buffer = 1;
            obj.bufferLabels = [obj.bufferLabels(:) {sprintf('buffer %i', obj.nBuffer)}];
            %obj.bufferLabels = {obj.bufferLabels{:} sprintf('buffer %i', obj.nBuffer)};
            for i=1:size(L,2),
                oae_log(sprintf('Creating custom stimulus with sampling rate, fs=%4.2f, buffer #%i', obj.fs, obj.nBuffer));
                obj.stimulus(:, i, obj.nBuffer, obj.Condition) = ramp((10.^(L(:, i)/20)).*obj.pref.*cos( phi(:, i) ), obj.rampTime, obj.fs );
                obj.phi(:, i, obj.nBuffer, obj.Condition) = phi(:, i);
            end
            obj.T = length(phi)./obj.fs;
        end
        
        
        % add a SOAE
        function obj = addSOAE(obj, T)
            % check for negative values
            if (T < 0)
                oae_error('SOAE cannot be less than 0 secs');
                return;
            end
            % add a sweep buffer
            if (obj.nCondition == 0),
                obj.nCondition = 1; obj.Condition = 1;
                obj.conditionLabels = {'Condition 1'};
                obj.paradigmType = 'soae';
            end
            obj.nBuffer = obj.nBuffer + 1; obj.Buffer = 1;
            obj.bufferLabels = [obj.bufferLabels(:) {sprintf('buffer %i', obj.nBuffer)}];
            obj.stimulus(:, 1, obj.nBuffer, obj.Condition) = zeros( obj.fs.* T, 1 );
            obj.L(1, obj.nBuffer,obj.nCondition) = 0;
            obj.fstart(1,obj.nBuffer,obj.nCondition) = 0;
            obj.T = T;
        end
        
        % add a Buffer
        function obj = addBuffer(obj, y, paradigmType)
            %add a custom buffer (e.g. your own stimulus)
            % add a sweep buffer
            if (obj.nCondition == 0),
                obj.nCondition = 1; 
                obj.Condition = 1;
                obj.conditionLabels = {'Condition 1'};
                obj.paradigmType = paradigmType;
            end
            obj.nBuffer = obj.nBuffer + 1; obj.Buffer = 1;
            obj.bufferLabels = [obj.bufferLabels(:); {sprintf('buffer %i', obj.nBuffer)}];
            obj.stimulus(:, :, obj.nBuffer, obj.Condition) = y;
            obj.fstart = [];
            obj.T = length(y)/obj.fs;
        end
        
        
        % add a Condition
        function obj = addCondition(obj, type, level, condition, conditionLabel)
            %add a custom buffer (e.g. your own stimulus)
            % add a sweep buffer
            
            % --------------------------------------------------------------
            % NOTES: This is somewhat hard-coded at the moment, to only deal
            % with CAS as the condition.
            % TODO: make this more dynamic when condition becomes more
            % delineated as a property.
            % --------------------------------------------------------------
            % Modifying to work with swept suppressor SFOAEs
            obj.nCondition = condition; 
            obj.Condition = 1;
            obj.conditionLabels{condition} = conditionLabel;
            if condition == 1,
                nextStimIndex = size(obj.stimulus,2)+1;
            else
                nextStimIndex = size(obj.stimulus,2);
            end
            switch type
                case {'noise'}
                    T = obj.T;
                    
                    if (obj.onsetDelay > 0) || (obj.offsetDelay > 0),
                        % add onset & offset time to total time
                        T = T+(obj.onsetDelay/obj.fs)+(obj.offsetDelay/obj.fs);
                        if condition == 1,
                            if (obj.onsetDelay > 0),
                                %pad the stimulus with delay
                                stimulus = obj.stimulus;
                                stimulus = [zeros(obj.onsetDelay, size(stimulus, 2), obj.nBuffer, obj.nCondition); stimulus];
                                obj.stimulus = stimulus;
                            end
                            if (obj.offsetDelay > 0),
                                %pad the stimulus with delay
                                stimulus = obj.stimulus;
                                stimulus = [stimulus; zeros(obj.offsetDelay, size(stimulus,2), obj.nBuffer, obj.nCondition) ];
                                obj.stimulus = stimulus;
                            end
                        end
                    end
                    % add noise to channel 3
                    t = 0:1/obj.fs:T;
                    wgn = 2.*(rand(length(t), 1)-0.5);
                    y = ramp((10.^(level/20)).*obj.pref.*wgn, obj.rampTime, obj.fs).* obj.v2pa;
                    y = y./sqrt(mean(y.^2));
                    order = 4;
                    [ b , a ] = butter(order, [ obj.CASFcL, obj.CASFcH ] /( obj.fs /2 ));  % if we are using filtfilt- the order of the filter is doubled so "order-1" is used to get the correct order         % second order filter with cutoffs defined by the freq difference between on frame above and below, space gets larger as we go up in f2
                    y = filter( b, a, full(y'));    
                    % insert condition stimulus in all buffers
                    for i=1:obj.nBuffer,
                        if condition > 1,
                            % copy stimulus & fstart & fend into condition
                            obj.stimulus(:,:, i, condition) = obj.stimulus(:,:, i, condition-1);
                            obj.fstart(:, i, condition) = obj.fstart(:, i, condition-1);
                            obj.fend(:, i, condition) = obj.fend(:, i, condition-1);
                        end
                        obj.stimulus(:,nextStimIndex, i, condition) = y;
                        % specify fstart/fend = 0, since noise...this will ensure that it will not be
                        % included in model.
                        obj.fstart(nextStimIndex,i, condition) = NaN ;
                        obj.fend(nextStimIndex,i,condition) = NaN ;
                    end
                case {'logsweep'}
                case { 'sf_2Condition' }   % need to now copy the stimulus
                    if condition == 1 % probe alone
                        %---- from add sweep ---- need to modify stim
                        %accordig to this
%                for i=1:length(L),
                % obj.stimulus(:, i, obj.nBuffer, obj.Condition) = ramp((10.^(L(i)/20)).*obj.pref.*oae_logsweep(fstart(i),fend(i), T, obj.fs), obj.rampTime, obj.fs ) .* obj.v2pa;
                
%                 % It is possible that v2pa has not been calibrated by the
%                 % user yet, so just scale by hypothetical level. Multiply
%                 % all stimuli by v2pa in preprocessing/pre-flight routine.
%                 obj.stimulus(:, i, obj.nBuffer, obj.Condition) = ramp((10.^(L(i)/20)).*obj.pref.*oae_logsweep(fstart(i),fend(i), T, obj.fs), obj.rampTime, obj.fs );
%                 obj.fstart(i,obj.nBuffer,obj.nCondition) = fstart(i);
%                 obj.fend(i,obj.nBuffer,obj.nCondition) = fend(i);
%                 obj.L(i, obj.nBuffer,obj.nCondition) = L(i);
%            end
                  % --------- end from add sweep ---------------
                  
                        %L  Levels for stimulus(1 x n x b x c)
                        lv = nan(1,obj.nOutput,obj.nBuffer,2);
                        %s = nan(size(obj.stimulus,1), size(obj.stimulus,2), size(obj.stimulus,3), obj.nCondition);
                        lv(1,:,:,condition)  = obj.L;
                        %s(:,:,:,condition) = obj.stimulus;
                        for i = 1:obj.nBuffer
                            for k = 1:length(level)
                                lv(1,k,i,condition) = level(k);
                                obj.stimulus(:,k,i,condition) = ramp((10.^(level(k)/20)).*obj.pref.*oae_logsweep(obj.fstart(k,i),obj.fend(k,i), obj.T, obj.fs), obj.rampTime, obj.fs );
                            end
                        end
                        obj.L = lv;
                        
                        
                    elseif condition == 2 % probe + suppressor
                        lv =  obj.L;
                        for i = 1:obj.nBuffer
                            for k = 1:length(level)
                                lv(1,k,i,condition) = level(k);
                                obj.stimulus(:,k,i,condition) = ramp((10.^(level(k)/20)).*obj.pref.*oae_logsweep(obj.fstart(k,i),obj.fend(k,i), obj.T, obj.fs), obj.rampTime, obj.fs );
                                 obj.fstart(:, i, condition) = obj.fstart(:, i, condition-1);
                                 obj.fend(:, i, condition) = obj.fend(:, i, condition-1);
                            end
                        end
                        
                        obj.L = lv;
                
                    end
                case { 'sf_3Condition'}
                    if condition == 1 % probe alone
                        %L  Levels for stimulus(1 x n x b x c)
                        lv = nan(1,obj.nOutput,obj.nBuffer,3);
                        %s = nan(size(obj.stimulus,1), size(obj.stimulus,2), size(obj.stimulus,3), obj.nCondition);
                        lv(1,:,:,condition)  = obj.L;
                        %s(:,:,:,condition) = obj.stimulus;
                        for k = 1:length(level)
                            for i = 1:obj.nBuffer
                                lv(1,k,i,condition) = level(k);
                                obj.stimulus(:,k,i,condition) = ramp((10.^(level(k)/20)).*obj.pref.*oae_logsweep(obj.fstart(k,i),obj.fend(k,i), obj.T, obj.fs), obj.rampTime, obj.fs );
                                
                            end
                        end
                        obj.L = lv;
                     
                        
                    elseif condition == 2 % probe + suppressor
                        lv = obj.L;
                        
                        for k = 1:length(level)
                            for i = 1:obj.nBuffer
                                lv(1,k,i,condition) = level(k);
                                obj.stimulus(:,k,i,condition) = ramp((10.^(level(k)/20)).*obj.pref.*oae_logsweep(obj.fstart(k,i),obj.fend(k,i), obj.T, obj.fs), obj.rampTime, obj.fs );
                                 obj.fstart(:, i, condition) = obj.fstart(:, i, condition-1);
                                 obj.fend(:, i, condition) = obj.fend(:, i, condition-1);
                            end
                        end
                        obj.L = lv;
                    elseif condition == 3 % probe + suppressor
                        lv = obj.L;
                        for k = 1:length(level)
                            for i = 1:obj.nBuffer
                                lv(1,k,i,condition) = level(k);
                                obj.stimulus(:,k,i,condition) = ramp((10.^(level(k)/20)).*obj.pref.*oae_logsweep(obj.fstart(k,i),obj.fend(k,i), obj.T, obj.fs), obj.rampTime, obj.fs );
                                obj.fstart(:, i, condition) = obj.fstart(:, i, condition-1);
                                obj.fend(:, i, condition) = obj.fend(:, i, condition-1);
                            end
                        end
                        obj.L = lv;
    
                    end 
            end
        end
        
        
        %% DATA
        % gets current data
        function data = getCurrentStimulus(obj)    
            % get current preprocessed stimulus (stim)
            data = obj.stim(:,:,obj.Buffer,obj.Condition);
        end
        % gets stimulus parameters
        function params = getStimulusParameters(obj)
            params.fs = obj.fs;
        end
        % saves a recording to obj.data
        function obj = saveRecording(obj, input)
            if size(input,1) > size(input,2),
                input = input';
            end
            input = obj.applyMicCalibration(input);                              % TODO VERIFY
            
            if isempty(obj.sensitivity) || isnan(obj.sensitivity),
                obj.sensitivity = 1;
                oae_warning('OTB warning: System sensitivity has not been calibrated, setting sensitivity = 1.');
            end
            if isempty(obj.gain) || isnan(obj.gain),
                obj.gain = 1;
                % this doesn't really need a warning
            end
            input = double(input)./obj.sensitivity./obj.gain./2e-5;
            input = input(1, obj.systemDelay+1:end); % 03/16/17 - SH, remove system delay from all recordings that get here (e.g. post callback)              
            % TODO: check if removing system delay here has any adverse
            % effects downstream (e.g. other methods/modeules may
            % assume system delay is not removed). Need to double
            % check. But if not, this is the most logicsal place to do
            % it! It happens on the basic input end, before any other
            % methods see the data.
            
            % save recording to file?
            % Doesn't apply to customRecordings
            if obj.saveToFile && ~obj.isCustom,
                obj = obj.saveRecordingToFile(input);
            else
                obj.data(obj.rep, :, obj.Buffer, obj.Condition) = input;
            end
        end
        
        % saves each sweep recording to a separate file 
        function obj = saveRecordingToFile(obj, input)
            
            if isempty(obj.dataFolder),
                obj = obj.makeDataFolder();
            end
            
            sep = filesep;  
            if isdeployed
                filename = datestr(now, '__HH-MM-SS-FFF');  % Hour-min-second-ms
                p = explode( obj.dataFolder, sep );
                filename = [implode(p(end),sep) filename] ;  
            else
                filename = datestr(now, 'mmm_yyyy_dd__HH-MM-SS-FFF');  % month-year-day_hour-min-second-milisecond 
            end
            pth = [obj.dataFolder sep filename];
            if size(input,1) > size(input,2),
                input = input';
            end
            data = input;
            save( pth, 'data');
            filename = {filename};                               % convert character array to string
            obj.dataFileNames(obj.rep,obj.Buffer, obj.Condition) = filename;
        end
        % make data folder
        function obj = makeDataFolder(obj)
            sep = filesep;
            if isdeployed;   
                p = explode(obj.filename, '.') ;  % remove the .mat - now folder has same name and location as the .mat file
                pth = implode(p(1:end-1), '.');
            else  % if not deployed
                foldername = datestr(now, 'mmm-yyyy-dd__HH-MM-SS-FFF');
                if ~isempty(obj.datapath),
                    pth = [obj.datapath sep foldername sep];
                else
                    % put it in data folder
                    pth = [pwd sep 'data' sep foldername sep];
                end
            end
            if ~exist(pth, 'dir')
                mkdir(pth);
            end                                              
            obj.dataFolder = pth;
        end
        
        % gets N specified data set for a SEQUENCE N - No longer functional
        % due to individual files being saved
        function data = getCurrentDataset(obj)
            data = obj.data(:,:,obj.Buffer, obj.Condition);
        end
        % saves an average
        function obj = saveAverage(obj, avg)
            %save the average
            
            %validate & resize, if necessary
            if min(size(avg)) ~= 1,
                oae_error('module error: invalid average file size');
            elseif size(avg,1) == 1,
                avg = avg';
            end
            %obj.avg(:,obj.Buffer, obj.Condition) = avg;
            
            %store average in the currently active analysisObj
            obj.analysisObj.avg(:,obj.Buffer, obj.Condition) = avg;
            
        end
        % saves a noise floor file
        function obj = saveNoiseFloor(obj, avg)
            %save the average
            
            %validate & resize, if necessary
            if min(size(avg)) ~= 1,
                oae_error('module error: invalid average file size');
            elseif size(avg,1) ~= 1,
                
                avg = avg';
            end
%             obj.nf(:,obj.Buffer, obj.Condition) = avg;

            %store average in the currently active analysisObj
            obj.analysisObj.nf(:,obj.Buffer, obj.Condition) = avg;
        end
        
        % Get data from specified repitition, buffer, condition  
        function data = getData(obj, r, b, c) 
            if obj.saveToFile,
                try,
                    data = obj.getDataFromFile(r,b,c);
                catch
                   data = obj.data(r,:,b,c); 
                end
            else
                data = obj.data(r,:,b,c);
            end
        end
        
        % loads rawdata from file
        function data = getDataFromFile(obj, r, b, c) 
            sep = filesep;
            if isdeployed
                d = load([obj.dataFolder sep char(obj.dataFileNames(r,b,c))]);
            else
                d = load([obj.dataFolder char(obj.dataFileNames(r,b,c))]);
            end
            data = d.data;
        end
        % update data files
        function obj = updateDataFile(obj, data, r, b, c)
            sep = filesep;
            if isdeployed
                filename = [obj.dataFolder sep char(obj.dataFileNames(r,b,c))];
            else
                filename = [obj.dataFolder char(obj.dataFileNames(r,b,c))];
            end
            save( filename , 'data');
        end
             
        %% artifact rejection methods
        % runs artifact rejection on each buffer & condition
        function obj = runArtifactRejection(obj)
            obj.avg = []; % clear whatever is already stored in avg. See below for details on datasets
            h = waitbar(0, 'Analyzing...'); counter = 1; total = (obj.nBuffer+obj.nCondition);
            try,
                for i=1:obj.nBuffer,
                    obj.Buffer = i;
                    for j=1:obj.nCondition,
                        obj.Condition = j;
                        if ~isempty(obj.artifactrejectionCallback),
                            obj = feval(obj.artifactrejectionCallback, obj);
                        end
                        counter = counter+1;
                        waitbar(counter/total, h);
                    end
                end
            catch ME
                % do nothing
                rethrow(ME)
            end
            close(h);
            % SH, 01/17/17: this was moved to a new NewDataset method for
            % better logic handling
%             % artifact rejection returns "cleaned data" into obj.avg
%             % since AR can take many forms (or be performed many different times)
%             % we store each average as a new "dataset". datasets are home
%             % to analysis objects unique to that partilar AR's cleaned
%             % dataset.
%             obj.datasets(end+1).avg             = obj.avg;
%             obj.datasets(end).analysisIndex   = 0;        % no analysis object yet, so initialize to 0. 
%             if isempty(obj.artifactrejectionLabel), 
%                 obj.datasets(end).label = obj.artifactrejectionCallback;
%             else
%                 obj.datasets(end).label = obj.artifactrejectionLabel;
%             end
%             obj.current_dataset     = length(obj.datasets);
        end
        
        %% adding or getting extra properties methods
        % adds extra props to obj.extraProps
        function obj = addExtraProp(obj, name, value)
            props = obj.extraprops;
            eval(['props.' name '=' 'value' ';']);
            obj.extraprops = props;
        end
        % gets extra props
        function val = getExtraProp(obj, name)
            props = obj.extraprops;
            eval(['val = props.' name ';']);
        end
        
        %% custom recording methods
        %  makes a custom recording with the option to perform stimulus
        %  calibration
        function  y = makeCustomRecording( obj, x, playBackCh, nReps, applyCalibration, doPlot, plotTitle, plotTag )
            % Makes a monoural recording or do synchronous averaging - must
            % know your sample delay
            if ~exist('plotTag', 'var')||isempty(plotTag),
                plotTag = 'customRecPlot';
            end
            if ~isempty(ishandle(doPlot)) && ishandle(doPlot) && strcmpi(get(doPlot, 'type'), 'axes'), % axes handle
                set(doPlot, 'tag', plotTag);
                h = doPlot;
                doPlot = 1;
            elseif ~isempty(ishandle(doPlot)) && doPlot,
                scrsz = get(0,'ScreenSize');
                sz = [600 350];
                ttle = 'Custom Recording';
                if exist('plotTitle', 'var') && ~isempty(plotTitle),
                    ttle = plotTitle;
                end
                figure('position', [scrsz(3)/2-sz(1)/2 scrsz(4)/2-sz(2)/2 sz], 'tag', plotTag, 'name', ttle, 'numbertitle', 'off','menubar', 'none');
                h = axes;
            else
                doPlot = 0;
            end
            %tempObj = controller(obj);
            %stimulus = obj.stimulus;
            tempObj = obj.copyobj;
            tempObj.isCustom = 1;                                           % checked by portaudio before saving data
            tempObj.onsetDelay = 0;
            tempObj.offsetDelay = 0;
            tempObj.displayVU = 0;
            tempObj.Buffer = 1;
            tempObj.Condition = 1;
            tempObj.nBuffer = 1;
            tempObj.nCondition = 1;
            tempObj.nOutput = length(playBackCh);
            tempObj.OutputChannels = playBackCh;
            tempObj.nRepetitions = nReps;
            if ~isempty(tempObj.OutputDevices),
                tempObj.OutputDevices = tempObj.OutputDevices(1:length(playBackCh));
            end
            channel_idx = find(playBackCh == obj.OutputChannels);
            tempObj.T = max(size(x))/tempObj.fs;
            tempObj.stim = x;   % set to stim, since we need x to be active in the queue
            if applyCalibration & ~isempty(tempObj.calibrationCallback),
                tempObj = tempObj.applyStimulusCalibration();
            end
            
            %--- recording loop ---%
            for i=1:nReps,
                tempObj.rep = i;
                tempObj = feval(tempObj.daqCallback, tempObj);
                if doPlot,
                    if i==1,
                        axes(h); cla;
                        %p = plot( (0:size(tempObj.data,2)-1)./tempObj.fs, tempObj.data, 'tag', 'customRecPlot');
                        p = line('xdata', (0:size(tempObj.data,2)-1)./tempObj.fs, 'ydata', tempObj.data, 'color', tempObj.linecolors(channel_idx,:));
                        axis tight;
                        xlabel('Time (s)'); ylabel('Amplitude (a.u.)');
                        drawnow; pause(0.01);
                    else
                        if ~isempty(findobj('tag', 'customRecPlot')), % check to make sure it hasn't been closed
                            set(p, 'ydata', mean(tempObj.data,1));
                            drawnow limitrate;
                        end
                    end
                end
            end
            if ~isempty(findobj('tag', plotTag)) & strcmpi(get(findobj('tag', plotTag), 'type'), 'figure'),
                close(findobj('tag', plotTag));
            end
            y = tempObj.data;  % multiply by pref to remove pressure scaling
            clear tempObj;
            
        end
        % makes a custom recording but without using any calibration
        function  y = makeUncalibratedRec( obj, x, playBackCh, nReps, varargin )
            % Makes a monoural (or stereo) recording and does synchronous averaging - must
            % know your sample delay
            % varargin should be numbrer of input channels (for recording
            % on two channels).
            if nargin == 5
                tempObj.nInput = varargin{1};       % r
                if tempObj.nInput ~= InputChannels
                    oae_error('Please configure your device to have at least two input channels!');
                    %InputChannels;          % size nInput (r)
                    %InputDevices;           % input device IDs..must be the same length as InputChannels
                end
            else
                tempObj.nInput = 1;
            end
            %tempObj = controller(obj);
            tempObj = obj.copyobj;
            tempObj.isCustom = 1;                                            % checked by portaudio before saving data
            tempObj.onsetDelay = 0;
            tempObj.offsetDelay = 0;
            tempObj.sensitivity = 1;
            tempObj.displayVU = 0;
            tempObj.Buffer = 1;
            tempObj.Condition = 1;
            tempObj.nBuffer = 1;
            tempObj.nCondition = 1;
            tempObj.nRepetitions = nReps;
            tempObj.nOutput = length(playBackCh);
            tempObj.OutputChannels = playBackCh;
            tempObj.T = max(size(x))/tempObj.fs;
            tempObj.stim = x;
            if ~isempty(tempObj.OutputDevices),
                tempObj.OutputDevices = tempObj.OutputDevices(1:length(playBackCh));
            end
            for i=1:nReps,
                tempObj.rep = i;
                tempObj = feval(tempObj.daqCallback, tempObj);
            end
            if ~isempty(tempObj.sensitivity),
                y = tempObj.data.*2e-5.*tempObj.sensitivity;
            else
                y = tempObj.data.*2e-5; % %saveRecording divides out pref, so we need to multiply it back in. 
            end
            clear tempObj;
        end
        
        %% Mic Calibration methods
        function x = applyMicCalibration(obj, x)
            % Considerations..............................
            % do each recording or any data you throw at it
            % make it able to do single files or matrices of data
            % this would it to be done on partial buffers for updateGUI
            % and before processing.
            
            % norm10cm will be used for all ER10C's
            % calibration files which have an time domain IR will be used for other pro
            tKernel = obj.micCalibrationFilter;                             % time domain mic filter Kernel
            szx = size(x);
            szk = size(tKernel);
            flag = 0;
            if isempty(x) || max(isnan(x)) || max(isinf(x)) || isempty(tKernel)
                %disp('x must not contain NaN, Inf, or be empty; calibration not performed');
                return
            end
            if szx(1) < szx(2)
                x = x';
                flag = 1;
            end
            
            if szk(1) < szk(2)
                tKernel = tKernel';
            end
            
            if size(x,2) < 1
                disp('x must be a vector; calibration not performed ');
                return
            end
            % Pad the signal at the end so we can remove the effects of the groupdelay wich is
            % (size(Kernel) - 1)/2;  by removing them at the front of output of fftfilt
            % signal. This compensates for the groupdelay.
            M = (size(tKernel,1)-1)/2;                                      % the amount to shift the output of fftfilt and to pad stimulus by to account for hte group delay of the Kernel
            pad = zeros(M,1);
            tmpx = [ x; pad ];
            ytmp = fftfilt( tKernel, tmpx);                                  % overlap-add the stimulus with the filter - seems
            x = ytmp(M+1:end,:);                                             % remove the filter group delay
            
            if flag
                x = x';                                                     % return recording with same orientation
            end
        end
        % performs realtime mic calibration directly on coeficients using a look up table (for
        % speed)
        function x = rtApplyMicCalibration(obj, x)
            % Considerations..............................
            % do each recording or any data you throw at it
            % make it able to do single files or matrices of data
            % this would it to be done on partial buffers for updateGUI
            % and before processing.
            
            % norm10cm will be used for all ER10C's
            % calibration files which have an time domain IR will be used for other pro
            tKernel = obj.micCalibrationFilter;                             % time domain mic filter Kernel
            szx = size(x);
            szk = size(tKernel);
            flag = 0;
            if isempty(x) || max(isnan(x)) || max(isinf(x)) || isempty(tKernel)
                %disp('x must not contain NaN, Inf, or be empty; calibration not performed');
                return
            end
            if szx(1) < szx(2)
                x = x';
                flag = 1;
            end
            
            if szk(1) < szk(2)
                tKernel = tKernel';
            end
            
            if size(x,2) < 1
                disp('x must be a vector; calibration not performed ');
                return
            end
            % Pad the signal at the end so we can remove the effects of the groupdelay wich is
            % (size(Kernel) - 1)/2;  by removing them at the front of output of fftfilt
            % signal. This compensates for the groupdelay.
            M = (size(tKernel,1)-1)/2;                                      % the amount to shift the output of fftfilt and to pad stimulus by to account for hte group delay of the Kernel
            pad = zeros(M,1);
            tmpx = [ x; pad ];
            x = fftfilt( tKernel, tmpx);                                  % overlap add the stimulus with the filter - seems
            %x = ytmp(M+1:end,:);                                             % remove the filter group delay
            
            if flag
                x = x';                                                     % return recording with same orientation
            end
        end
        % alternative realtime mic calibration (not currently used)
        function x = rtMicConv(obj, x)
            tKernel = obj.rtMicCalibrationFilter;                             % time domain mic filter Kernel
            szx = size(x);
            szk = size(tKernel);
            flag = 0;
            if isempty(x) || max(isnan(x)) || max(isinf(x)) || isempty(tKernel)
                %disp('x must not contain NaN, Inf, or be empty; calibration not performed');
                return
            end
            if szx(1) < szx(2)
                x = x';
                flag = 1;
            end
            
            if szk(1) < szk(2)
                tKernel = tKernel';
                szk = szk(2);
            else
                szk = szk(1);
            end
            
            if size(x,2) < 1
                disp('x must be a vector; calibration not performed ');
                return
            end
            % Pad the signal at the end so we can remove the effects of the groupdelay wich is
            % (size(Kernel) - 1)/2;  by removing them at the front of output of fftfilt
            % signal. This compensates for the groupdelay.
            %M = (size(tKernel,1)-1);                                      % the amount to shift the output of fftfilt and to pad stimulus by to account for hte group delay of the Kernel
            %pad = zeros(M,1);
            %tmpx = [ x ];
            x = conv(x,tKernel, 'full');
            x = x./(sqrt(szk));
            if flag
                x = x';                                                     % return recording with same orientation
            end
        end
        
        % Run calibration callback
        function obj = runCalibration(obj)
            if (~isempty(obj.calibrationCallback) && ~strcmp(obj.calibrationCallback, ''))
                obj = feval(obj.calibrationCallback, obj);
            else
                % if no calibration routine. Ask if we should perform a probe check in lieu
                response = oae_questdlg('Perform probe check?', {'Yes', 'No'});
                if response == 1,
                    obj = obj.probeCheck();
                end
            end
        end
        
        % Run calibration callback
        function obj = runLevelCalibration(obj)
            oae_log('Performing level calibration...');
            
            % initialize v2pa_levelcal to one for each channel before
            % running
            obj.v2pa_levelcal(1:obj.nOutput) = 1;
            
            % generate a known sound level for each probe and playback
            L = 60; f = 1000; t = 0:1/obj.fs:0.1;
            for i=1:obj.nOutput,
                x = obj.v2pa(i).*oae_ramp(10^(L/20).*obj.pref.*sin(2*pi*f*t), 0.005, obj.fs);
                data = obj.makeCustomRecording(x, obj.OutputChannels(i), 16, true, false);
                
                %data = obj.makeCustomRecording(x, obj.OutputChannels(i),8, true, false); % orginal
                 
                % Simon's recent change to help with Devon's variability in
                % calibrations due to noise
                % ------------ Start update -------------------------
                %data = obj.makeCustomRecording(x, obj.OutputChannels(i),16, true, false); % Simon update March 14, 2019
                % compute RMS amplitude (but scale by rms, since input sens is rms scaled already), compare with target level, and adjust by this factor
                data = mean(data);
                Ldiff = L-20*log10(sqrt( mean(data.^2)));
                
                % % % % Removing this update, since it can cause instability -  SH, 06/17/19 % % % %
                % % % Simon update - March 14, 2019
                % % % get amplitude via regression and adjust accordingly.
                % % % Needs to be scaled by sqrt(2) since sensitivity is rms
                % % % x = obj.v2pa(i).*oae_ramp(10^(L/20).*sin(2*pi*f*t), 0.005, obj.fs).*sqrt(2);  not sure if v2pa should be applied. I think not
                % % x = oae_ramp(10^(L/20).*sin(2*pi*f*t), 0.005, obj.fs).*sqrt(2);
                % % x = x(1:length(data));
                % % W = x(:) \ data(:);  % this gets scaler of x versus recorded data (e.g. how much bigger is x than data = Ldiff)
                % % Ldiff = 20*log10( W );
                % ------------ End update ------------------------------
                % % % % Removing this update, since it can cause instability -  SH, 06/17/19 % % % %
                
                % obj.v2pa(i) = obj.v2pa(i) * 10^(Ldiff/20); % update v2pa for each sequence, but this v2pa is not saved upon closing ,which is good. Therefore changes are not preserved across ears or calibrators, and only run before each test.
                
                % store the in-situ level correction factor. This gets applied to the output stimulus during preProcessing()
                obj.v2pa_levelcal(i) = 10^(Ldiff/20);  % - Note: JJH - Aug 4, 2019 - v2pa_levelcal just scales the overall level at 1kHz up or down, 
                %% which is basically dependant on ear canal volume. The reason for this is to get a better overall level adjustment 
                %% when we make the measures. It changes with each sequence, and is never stored, other than for the current sequence and goes back to null when the session ends - JJH
                
                pause(0.25);
            end
            
        end
        
        % Perform probe check routine
        function obj = probeCheck(obj)
            % chirp-based probe check
            x = (10^(60./20)*obj.pref).*oae_chirp(0.05, obj.fs);
            x = [x; zeros(obj.systemDelay, 1)];
            if isrow(x), x = x'; end % just a check in case the chirp function ever changes. needs to be n x nOutput column vector
            x = repmat(x, 1, obj.nOutput);
            x = bsxfun(@times, x, obj.v2pa);
            
            nReps = 32;   
            applyCalibration = 1;
            
            % Generate the figure
            % Pass ax1 (subplot(122)) as axes handle to makeCustomRecording
            % so that recording as shown in this figure
            fig_handle = findobj('tag', 'probe_check');
            if isempty(fig_handle),
                scrsz = get(0,'ScreenSize');
                sz = [900 300];
                fig_handle = figure('position', [scrsz(3)/2-sz(1)/2 scrsz(4)/2-sz(2)/2 sz], 'color', get(findobj('tag', 'OAEToolbox'), 'color'), 'name', 'Probe Check', 'tag', 'probe_check', 'numbertitle', 'off', 'menubar', 'none');
                ax1 = subplot(121); hold on;
                ax2 = subplot(122); hold on; 
                xlabel('Frequency (Hz)', 'fontsize', 10); ylabel('Power/Frequency (dB/Hz)', 'fontsize', 10);
            end
            figure(fig_handle);
            ax1 = subplot(121); hold on;
            ax2 = subplot(122); hold on;
            % see if we have any old line, and make them slightly more
            % transparent as we move across sequences so we can
            % differentiate
            lines = findall(ax2, 'type', 'line');
            for i=1:length(lines),
                alpha = get(lines(i), 'userdata');
                if isempty(alpha),
                    alpha = 0.65;
                else
                    alpha = alpha-(alpha*0.1);
                end
                set(lines(i), 'color', [get(lines(i), 'color') alpha], 'userdata', alpha);
            end
            % add some new lines as placeholders
            data_handles = line(1:10, nan(10,obj.nOutput));
            for i=1:length(data_handles),
                set(data_handles(i), 'color', [obj.linecolors(i,:) 1]); % add transparency = 1
            end
            str = [];
            for i=1:obj.nOutput,
                str{i} = sprintf('Channel #%i', obj.OutputChannels(i));
            end
            legend([data_handles(1:obj.nOutput)],str);
                
            xlim(obj.pc_xlim);
            
            ok = 0;
            response_data = [];
            while ok ~= 1
                axes(ax1); cla;
                axes(ax2);
                clear tmp;
                [tmp{1:obj.nOutput}] = deal(nan(1,10)); tmp =tmp';
                set(data_handles, {'xdata'}, tmp, {'ydata'}, tmp);
                
                for i=1:obj.nOutput
                    plotTitle = sprintf('Probe check channel %i', obj.OutputChannels(i));
                    plotTag = 'probeCheck';
                    y = obj.makeCustomRecording(x(:,i), obj.OutputChannels(i), nReps, applyCalibration, ax1, plotTitle, plotTag );
                    %y = y(:, obj.systemDelay+1:end); % this is done in saveRecording now
                    response_data(:,i) = mean(y(3:end,:),1);  % throwing out first two chirps as one or both seem to be artifacts - JJH
                    nfft = 2048;
                    xdft = fft(response_data(:,i), nfft);
                    xdft = xdft(1:nfft/2+1);
                    xdft(2:end-1) = 2*xdft(2:end-1);
                    psdest = 1/(nfft*obj.fs)*abs(xdft).^2;
                    f = 0:obj.fs/nfft:obj.fs/2;
                    axes(ax2);
                    %handle = findall(data_handles, 'DisplayName', sprintf('Channel #%i', obj.OutputChannels(i)));
                    set(data_handles(i), 'xdata', f, 'ydata', 10*log10(psdest));
                end
                ok = oae_questdlg('OK to proceed?', {'Yes', 'Repeat Measurement'});
            end
            % store the raw data in case we want to see it later,
            % n x nOutput
            obj.probe_check_data{end+1} = response_data;
        end
        
        
        % sets the stimulus calibration filters
        function obj = setStimulusCalibrationFilter(obj, channel, filterKernel)
            % Generic function to set specified channel output calibration filter (kernel)
            
            %route = oae_getvalue('daq.parameters', 'routeCAS');
            route = size(unique(obj.OutputDevices),2)-1; % is output routed to multiple devices?
            
            nOutput = obj.nOutput;
            if route                                             % if routing CAS apply calibration to nOutput-1
                nOutput = obj.nOutput -1;
            end
            % if kernel is empty, lets initialize it (this should only happen the first time this method is called)
            Kernels = obj.stimCalibrationFilter;
            if isempty(Kernels) || max(size(filterKernel)) ~= max(size(Kernels))
                Kernels = nan(nOutput, length(filterKernel));   % initialize to nans.
            end
            
            if route
                idx = max(size(obj.OutputChannels));
                OutputChannels = obj.OutputChannels(1:idx-1);
            else
                OutputChannels = obj.OutputChannels;
            end
            channel_index = find(OutputChannels == channel);
            Kernels(channel_index, :) = filterKernel;
            obj.stimCalibrationFilter = Kernels;
            
        end
        
        % applys the stimulus calibration
        function obj = applyStimulusCalibration(obj)
            % apply stimulus calibration
            % Uses stimCalibrationFilter matrix (nOutput x length(filterKernel)
            oae_log('applying stimulus calibration...');
            routed = size(unique(obj.OutputDevices),2)-1; % is output routed to multiple devices?
            nOutput = obj.nOutput;
            if routed                                             % if routing CAS apply calibration to nOutput-1
                nOutput = obj.nOutput - 1;
            end
            if ~isempty(obj.stimCalibrationFilter),
                for b=1:obj.nBuffer,
                    for c=1:obj.nCondition,
                        for i=1:nOutput,
                            Kernel = obj.stimCalibrationFilter(i,:);
                            if isnan(Kernel),
                                continue;
                            end
                            %M = length(Kernel);
                            M = floor(length(Kernel)/2);       % grpdelay = (# of filter coeffients-1 / 2 ). Use floor for even order filter (# coeffs = filter order +1)
                            pad = zeros(M,1);
                            tmpStim = [ obj.stim( :, i, b, c); pad ];
                            %y = fftfilt( Kernel, tmpStim );                         % overlap add the stimulus with the filter
                            %y = y ./ max( abs( y ) );                               % normalize to 1
                            %y = 10.^(obj.L(i,b,c)/20).*obj.pref.*y.*obj.v2pa;   % scale to the desired level
                            
                            y = filter(Kernel, 1, tmpStim);                       
                            obj.stim(:,i, b, c) = y(M+1:end); % remove fir filter delay
                        end
                    end
                end
            end

        end
        
        
        %% Analysis methods
        % runs analysis
        function obj = runAnalysis(obj)
            %reset the buffers for this object
            obj.Buffer = 1; obj.Condition = 1;
            index = obj.datasets(obj.current_dataset).analysisIndex;
            
            % Analysis needs to be assigned to temporary object to ensure
            % properities are properly copied
            tmp = obj.datasets(obj.current_dataset).analysisObj(index);
            tmp = tmp.analyze(obj);
            obj.datasets(obj.current_dataset).analysisObj(index) = tmp;
            
            % TODO: Remove this...not needed. buffer control handled in
            % analysis object
            %             obj.Buffer = 1; obj.Condition = 1; counter = 1;
            %             h = waitbar(0, 'Analyzing...');
            %             for i=1:obj.nBuffer,
            %                 obj.Buffer = i;
            %                 for j=1:obj.nCondition,
            %                     obj.Condition = j;
            %                     index = obj.analysisIndex;
            %                     tmp = obj.analysisObj(index);
            %                     tmp = tmp.analyze(obj);
            %                     obj.analysisObj(index) = tmp;
            %                     waitbar(counter/(obj.nBuffer+obj.nCondition),h);
            %                     counter = counter +1;
            %                 end
            %             end
            %             close(h);
        end
        
        % Create a new dataset for artifact rejection
        function obj = newDataset(obj),
            % get the current dataset (e.g. artifact rejected average)
            ds = obj.current_dataset+1;
            if isempty(ds),
                ds = 1;
            end
            obj.datasets(ds).analysisIndex   = 0;        % no analysis object yet, so initialize to 0. 
            if isempty(obj.artifactrejectionLabel),
                obj.datasets(ds).label = obj.artifactrejectionCallback;
            else
                obj.datasets(ds).label = obj.artifactrejectionLabel;
            end
            obj.current_dataset     = length(obj.datasets);
            % initialize a new analysis object for this dataset
            obj = obj.newAnalysis();
            obj.analysisObj.avg = [];
            obj.analysisObj.nf  = [];
        end
        
        function obj = setActiveDataset(obj)
            %store new analysis object somewhere visible for module
            %functions to access
            obj.analysisObj = obj.datasets( obj.current_dataset ).analysisObj(obj.datasets(  obj.current_dataset ).analysisIndex);
        end
        
        % makes a new analysis object for the current sequence
        function obj = newAnalysis(obj)
            % make a new analysis object for the current sequence
            % Analysis object follow a similar structure with buffers and
            % conditions
            
            % get the current dataset (e.g. artifact rejected average)
            ds = obj.current_dataset;
            
            if ~isfield(obj.datasets(ds), 'analysisObj')
                analysisIndex = 0;
            else
                analysisIndex   = length(obj.datasets(ds).analysisObj);
            end
            obj.datasets(ds).analysisIndex = analysisIndex + 1;
            if obj.datasets(ds).analysisIndex == 1,
                obj.datasets(ds).analysisObj                    = analysis();
                obj.datasets(ds).analysisObj.fs                 = obj.fs;
                obj.datasets(ds).analysisObj.gain               = obj.gain;
                obj.datasets(ds).analysisObj.sensitivity        = obj.sensitivity;
                obj.datasets(ds).analysisObj.nBuffer            = obj.nBuffer;
                obj.datasets(ds).analysisObj.bufferLabels       = obj.bufferLabels;
                obj.datasets(ds).analysisObj.nCondition         = obj.nCondition;
                obj.datasets(ds).analysisObj.conditionLabels    = obj.conditionLabels;
                obj.datasets(ds).analysisObj.paradigmType       = obj.paradigmType;
                obj.datasets(ds).analysisObj.L                  = obj.L;
                obj.datasets(ds).analysisObj.fstart             = obj.fstart;
                obj.datasets(ds).analysisObj.fend               = obj.fend;
                obj.datasets(ds).analysisObj.T                  = obj.T;
                obj.datasets(ds).analysisObj.artifactrejectionLabel = obj.artifactrejectionLabel;
                % patient info
                obj.datasets(ds).analysisObj.patientID          = obj.patientID;
                obj.datasets(ds).analysisObj.ear                = obj.ear;
                obj.datasets(ds).analysisObj.DOB                = obj.DOB;
                obj.datasets(ds).analysisObj.sex                = obj.sex;
                obj.datasets(ds).analysisObj.notes              = obj.notes;
                obj.datasets(ds).analysisObj.avg = [];
                obj.datasets(ds).analysisObj.nf = [];
                if ds>1,
                    % Since this is the first analysis object, but not the first dataset, an analysis module is probably active.
                    % copy some additional fields from the last active analysisObj, relevant to analysis
                    % so it can run without reselecting options
                    p = properties(obj.analysisObj);
                    for i = 1:length(p),
                        obj.datasets(ds).analysisObj.(p{i}) = obj.analysisObj.(p{i});
                    end
%                     obj.datasets(ds).analysisObj.analysisCallback   = obj.analysisObj.analysisCallback; % keep analysis callback
%                     obj.datasets(ds).analysisObj.analysisType       = obj.analysisObj.analysisType;
%                     obj.datasets(ds).analysisObj.Label              = obj.analysisObj.Label;
%                     obj.datasets(ds).analysisObj.nfilt              = obj.analysisObj.nfilt;
%                     obj.datasets(ds).analysisObj.nstep              = obj.analysisObj.nstep;
%                     obj.datasets(ds).analysisObj.navg               = obj.analysisObj.navg;
%                     obj.datasets(ds).analysisObj.calcDifference     = obj.analysisObj.calcDifference;
%                     obj.datasets(ds).analysisObj.phaseAccumFlag     = obj.analysisObj.phaseAccumFlag;
                end
            else
                % copy previous analysis object to copy previous analysis settings
                % ---------------------------------------------------------------- 
                obj.datasets(ds).analysisObj(obj.datasets(ds).analysisIndex) = analysis();
                obj.datasets(ds).analysisObj(obj.datasets(ds).analysisIndex).analysisCallback   = obj.datasets(ds).analysisObj(obj.datasets(ds).analysisIndex-1).analysisCallback; % keep analysis callback
                obj.datasets(ds).analysisObj(obj.datasets(ds).analysisIndex).analysisType       = obj.datasets(ds).analysisObj(obj.datasets(ds).analysisIndex-1).analysisType;
                obj.datasets(ds).analysisObj(obj.datasets(ds).analysisIndex).fs                 = obj.fs;
                obj.datasets(ds).analysisObj(obj.datasets(ds).analysisIndex).gain               = obj.gain;
                obj.datasets(ds).analysisObj(obj.datasets(ds).analysisIndex).sensitivity        = obj.sensitivity;
                obj.datasets(ds).analysisObj(obj.datasets(ds).analysisIndex).nBuffer            = obj.nBuffer;
                obj.datasets(ds).analysisObj(obj.datasets(ds).analysisIndex).bufferLabels       = obj.bufferLabels;
                obj.datasets(ds).analysisObj(obj.datasets(ds).analysisIndex).nCondition         = obj.nCondition;
                obj.datasets(ds).analysisObj(obj.datasets(ds).analysisIndex).conditionLabels    = obj.conditionLabels;
                % copy all visible properties
                p = properties(obj.analysisObj);
                for i = 1:length(p),
                    obj.datasets(ds).analysisObj(obj.datasets(ds).analysisIndex).(p{i}) = obj.analysisObj.(p{i});
                end
%                 obj.datasets(ds).analysisObj(obj.datasets(ds).analysisIndex).avg                = obj.analysisObj.avg;
%                 obj.datasets(ds).analysisObj(obj.datasets(ds).analysisIndex).nf                 = obj.analysisObj.nf;
%                 obj.datasets(ds).analysisObj(obj.datasets(ds).analysisIndex).Label              = obj.analysisObj.Label;
%                 obj.datasets(ds).analysisObj(obj.datasets(ds).analysisIndex).paradigmType       = obj.analysisObj.paradigmType;
%                 obj.datasets(ds).analysisObj(obj.datasets(ds).analysisIndex).nfilt              = obj.analysisObj.nfilt;
%                 obj.datasets(ds).analysisObj(obj.datasets(ds).analysisIndex).nstep              = obj.analysisObj.nstep;
%                 obj.datasets(ds).analysisObj(obj.datasets(ds).analysisIndex).navg               = obj.analysisObj.navg;
            end
            %store new analysis object somewhere visible for module
            %functions to access
            %obj.analysisObj = obj.datasets(ds).analysisObj(obj.datasets(ds).analysisIndex);
            obj = obj.setActiveDataset();
            
        end
        
        % utility functions to get current analysis object or index
        function num = getNumberofAnalyses(obj),
            ds = obj.current_dataset;
            num = length(obj.datasets(ds).analysisObj);
        end
        function analysisObj = getCurrentAnalysisObject(obj),
            % currentIndex stores vector: [current_dataset current_analysis_index]
            % check for valid currentIndex
            if obj.currentIndex(1) > length(obj.datasets),
                obj.currentIndex(1) = length(obj.datasets);
            end
            if obj.currentIndex(2) > length(obj.datasets(obj.currentIndex(1)).analysisObj),
                obj.currentIndex(2) = length(obj.datasets(obj.currentIndex(1)).analysisObj);
            end
            analysisObj = obj.datasets(obj.currentIndex(1)).analysisObj(obj.currentIndex(2));
        end
        
    end
    
end

