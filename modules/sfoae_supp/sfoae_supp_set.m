function OAE = sfoae_supp_set(OAE, SEQUENCE)


%reset the sequence
OAE(SEQUENCE) = OAE(SEQUENCE).initializeSequence();

% return the values of the edit box
%handle = findobj(OAE(SEQUENCE).fig, 'tag', 'paradigm.parameters');\
handle = 'paradigm.parameters'; 
f1s = oae_getvalue(handle, 'f1s');
f1e = oae_getvalue(handle, 'f1e');
f2s = oae_getvalue(handle, 'f2s');
f2e = oae_getvalue(handle, 'f2e');
L1 = oae_getvalue(handle, 'L1');
L2 = oae_getvalue(handle, 'L2');
suppCond = oae_getvalue(handle, 'sfoaeSuppCondition','string');
Toct = oae_getvalue(handle, 'Toct');
nReps = oae_getvalue(handle, 'nRepetitions');
bufferType = oae_getvalue(handle, 'bufferType', 'string');
protocol = oae_getvalue(handle, 'sfoae_supp.protocols', 'string');

%% TODO: add variable validation routine
if all([L1 L2 f1s f1e f2s f2e suppCond nReps Toct]) & ~isnan([L1 L2 f1s f1e f2s f2e suppCond nReps Toct])
    
    %reset the sequence
    OAE(SEQUENCE) = OAE(SEQUENCE).initializeSequence();

    Noct = log2(f1e/f1s);
    T = Noct * Toct;
    if T,
        OAE(SEQUENCE).paradigmLabel = 'sfoae_supp';
        OAE(SEQUENCE).protocol = protocol;
        OAE(SEQUENCE).T = T;
        switch bufferType,
            case 'up/down'
                %addSweep(obj, L, fstart, fend, T)
                OAE(SEQUENCE) = OAE(SEQUENCE).addSweep([L1 L2], [f1s f2s], [f1e f2e], T);
                OAE(SEQUENCE) = OAE(SEQUENCE).addSweep([L1 L2], [f1e f2e], [f1s f2s] ,T);
              
            case 'alternating'
               OAE(SEQUENCE) = OAE(SEQUENCE).addSweep([L1 L2], [f1s f2s], [f1e f2e], T);
               OAE(SEQUENCE) = OAE(SEQUENCE).addSweep([L1 L2], [f1s f2s], [f1e f2e], T);
             
            otherwise
                OAE(SEQUENCE) = OAE(SEQUENCE).addSweep([L1 L2], [f1s f2s], [f1e f2e], T);
                
        end
        % Add Conditions
        switch suppCond
            case {'2-Condition'}
                %                        addCondition(obj, type, level, condition#, conditionLabel)
                OAE(SEQUENCE) = OAE(SEQUENCE).addCondition('sf_2Condition',     [L1 -inf], 1, 'probe_alone');
                OAE(SEQUENCE) = OAE(SEQUENCE).addCondition('sf_2Condition',[L1   L2], 2, 'probe_suppressor');
                OAE(SEQUENCE).paradigmType = '2C suppressor';
                
            case {'3-Condition'}
                OAE(SEQUENCE) = OAE(SEQUENCE).addCondition('sf_3Condition',    [L1   -inf], 1, 'probe_alone');
                OAE(SEQUENCE) = OAE(SEQUENCE).addCondition('sf_3Condition',[-inf  L2], 2, 'suppressor_alone');
                OAE(SEQUENCE) = OAE(SEQUENCE).addCondition('sf_3Condition',[L1    L2], 3, 'probe_suppressor');
                OAE(SEQUENCE).paradigmType = '3C suppressor';
                
                
        end
        OAE(SEQUENCE).setProp('displayRT', false);
        OAE(SEQUENCE).setProp('displayVU', false);
        OAE(SEQUENCE).nRepetitions = nReps;
        OAE(SEQUENCE).paradigmLabel = 'sfoae_supp';
        OAE(SEQUENCE).T = T;
        OAE(SEQUENCE).protocol = protocol;
        OAE(SEQUENCE).nRepetitions = nReps;
      
    end
end


end
