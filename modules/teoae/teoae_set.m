function OAE = teoae_set(OAE, SEQUENCE)
% The nonlinear TEOAE is achieved by presenting the a click 
% stim 2 needs to be 15 dB higher as it's the suppressor, 50ms long, shawn does 0.2512 and .99 (25.5ms long) for the clicks,which is about 10 dB about 3ms after first sample. 
% reset the sequence - the three presentations make one buffer - number of
% buffers used on goodman et al., 2009 = 4050
% Kemp (1990) describes the 3 small one large and reversed polatrity TEOAE
% method. 
% peSPL is defined by the peak-peak amplitude of a sinusoid taht matches the maximum peak-to-peak pressure amplitude of the click waveform (Burkard, 2006) 43-73 peSPL levels (Goodman et al., 2009).  
OAE(SEQUENCE) = OAE(SEQUENCE).initializeSequence();

% return the values of the edit box
%handle = findobj(OAE(SEQUENCE).fig, 'tag', 'paradigm.parameters');\
handle = 'paradigm.parameters'; 
L = oae_getvalue(handle, 'L');
nReps = oae_getvalue(handle, 'nRepetitions');
ISI = oae_getvalue(handle, 'ISI');
protocol = oae_getvalue(handle, 'protocols', 'string');

%% TODO: add variable validation routine
if all([L nReps ISI]) & ~isnan([L nReps ISI]),
    
    %reset the sequence
    OAE(SEQUENCE) = OAE(SEQUENCE).initializeSequence();
    OAE(SEQUENCE).paradigmLabel = 'teoae_2e';
    OAE(SEQUENCE).protocol = protocol;
    
    % Make a custom stimulus
    % Requires three buffers (p1,p2,p3), which will be subtracted from one another such that: 
    % p_2e = (p1+p2) - p3
    
    % make stimulus
    T = 0.020+(OAE(SEQUENCE).systemDelay/OAE(SEQUENCE).fs); % stimulus duration in seconds + systemDelay. System delay is added in order to ensure enough samples are recorded.
    N = round(T * OAE(SEQUENCE).fs); % total number of samples
    pad = zeros(N,1);
    stim = zeros(N,1);
    %stim(200) = 0.05; % Simon's orignal way
    stim(round(.003*OAE(SEQUENCE).fs)) = 0.5;
    stim1 = [stim,pad];
    stim2 = [pad,stim*10^(15/20)];  % stim 2 needs to be 15 dB higher as it's the suppressor, 50ms long, shawn does 0.2512 and .99 for the clicks,which is about 10 dB about 3ms after first sample. 
    stim3 = [stim,stim*10^(15/20)]; % repeat 500 times: a good default. ILO-88 uses 80microsecond pulse that decays over 2-3ms (goodman et al., 2009)
    
    % Add stimulus buffers
    OAE(SEQUENCE) = OAE(SEQUENCE).addBuffer(stim1, 'teoae_2e');
    OAE(SEQUENCE) = OAE(SEQUENCE).addBuffer(stim2, 'teoae_2e');
    OAE(SEQUENCE) = OAE(SEQUENCE).addBuffer(stim3, 'teoae_2e');
    
    % Override any realtime analysis, and artifact rejection (averaging). AR will
    % be peformed in the teoae_2e analysis routines
    OAE(SEQUENCE).setProp('displayRT', false);
    OAE(SEQUENCE).setProp('ISI', ISI);
    OAE(SEQUENCE).artifactrejectionCallback = [];
    
    OAE(SEQUENCE).nRepetitions = nReps;
    OAE(SEQUENCE).paradigmType = 'transient';
end


end
