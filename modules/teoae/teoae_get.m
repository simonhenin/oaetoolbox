function OAE = sfoae_get(OAE, SEQUENCE)

%% callback function for ui elements (e.g. to update the controls & update
%% the sequence OBJECT
cmd = ['OAE = teoae_set(OAE, SEQUENCE);'];
uigeom = {1 [0.5 1.5] [1 1] [1 1] [1 1] 1};  % each cell array is row 1 row 2... ratio of how much space its using whole row - row gets subdivided by how many peices, sum of each row should always add up to the number of elements in that row
uilist = ...
    {{ 'style' 'text'      'string'    'OAE SETUP'} ...
    {'style'  'text'      'string'    'protocol:'} ...
    {'style'  'popupmenu'      'string'    'protocol', 'tag' , 'protocols', 'callback', @oae_update_protocol} ...
    {'style'  'text'      'string'    'Stimulus Level (dB)'} ...
    {'style'  'edit'      'string'    '', 'tag', 'L', 'callback', cmd} ...
    {'style'  'text'      'string'    'Number of Repetitions:'} ...
    {'style'  'edit'      'string'    '', 'tag', 'nRepetitions', 'callback', cmd} ...
    {'style'  'text'      'string'    'Interstimulus Interval, ISI (s):'} ...
    {'style'  'edit'      'string'    '', 'tag', 'ISI', 'callback', cmd} ...
    {'style'  'pushbutton' 'string'    'Add', 'callback', @oae_update_sequence} ...
 };

%handle = findobj(OAE(SEQUENCE).fig, 'tag', 'paradigm.parameters');
%oae_addparameters(handle, uilist, uigeom);
oae_addparameters('paradigm.parameters', uilist, uigeom, 'topborder', 0.01, 'fontsize', 10);