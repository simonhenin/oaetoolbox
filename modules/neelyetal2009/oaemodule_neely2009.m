% dpoae acquisition plugin based on Neely et. al (2009)
%
% 
% version 1.0  07/2018
% Copyright (C) 2018  Simon Henin
function [vers block label cmd] = oaemodule_neely2009(fig)

vers='neely2009 1.0';
label = 'NEELY2009';
cmd=  [  'OAE = neely2009_get(OAE, SEQUENCE);'];
block = 'paradigm';


end