function OAE = neely2009_set(OAE, SEQUENCE)
% 
% function that sets the object
% 3/19/19, JJH corrected the function name
%reset the sequence
OAE(SEQUENCE) = OAE(SEQUENCE).initializeSequence();

handle = 'paradigm.parameters'; 
L2 = oae_getvalue(handle, 'L2');
f2s = oae_getvalue(handle, 'f2s');
f2e = oae_getvalue(handle, 'f2e');
T = oae_getvalue(handle, 'T');
nRepetitions = oae_getvalue(handle, 'nRepetitions');
% bufferType = oae_getvalue(handle, 'bufferType', 'string'); % not yet implemented
protocol = oae_getvalue(handle, 'protocols', 'string');


%% module's special routine to set the the object
fs = OAE(SEQUENCE).fs;
pref = 2e-5;

%check all variables properly set
if oae_validate([L2 f2s f2e T nRepetitions]),
    
    fs = OAE(SEQUENCE).fs;
    t = 0:1/fs:T;
    nOct = log2(f2e/f2s); % number of octaves covered in sweep
    r = nOct/T; % sweep rate (octaves/sec)
    t = (0:1/fs:(nOct/r));
    k = (f2e/f2s)^(r/nOct);
    f2 = f2s*k.^t; % instantaneous frequency for primary f2 (Hz)
    % from: Neely, Johnson, Kopun (2009) JASA p.729
    % f1 = f2/1.22; % basic f2/f1 frequency ratio
    f1 = f2./(1.22 + log2(9.6./(f2*1e-3))*(L2/415)^2); % instantaneous frequency for f1 (Hz)
    L1 = 80 + 0.137*log2(18./(f2*1e-3))*(L2-80); % instantaneous intensity for f1 (dB SPL)  equation (1) in Neely
    L2 = repmat(L2, size(L1, 1), size(L1,2));
    
    phi1 = (2*pi)*(1/fs)*cumtrapz(f1)+deg2rad(270); % phase or integral of f1, shift by 90 degrees, since oae_logsweep uses cosines
    [~, phi2] = oae_logsweep(f2s, f2e, T, fs);      % use oae_logsweep function to create f2 sweep. We will use phi2 as custom input to OAE.addCustomStimulus
    
    % need to override stimulus and set it manually
    OAE(SEQUENCE).paradigmLabel = 'dpoae';
    OAE(SEQUENCE).T = T;
    OAE(SEQUENCE).protocol = protocol;
    OAE(SEQUENCE).nRepetitions = nRepetitions;
    
    rampTime = 0.005; 
    OAE(SEQUENCE).addCustomStimulus([phi1; phi2], [L1; L2], rampTime);
    % might as well set these values, though they are unused when setting
    % "custom" paradigmType, but useful for record keeping
    OAE(SEQUENCE).fstart = [f1(1); f2(1)]; % needs to be nfreq x 1
    OAE(SEQUENCE).fend = [f1(end); f2(end)]; % needs to be nfreq x 1
    OAE(SEQUENCE).L = [NaN; L2(1)]; % put NAN as L1 start, since varying.
    OAE(SEQUENCE).setProp('displayRT', true);
    OAE(SEQUENCE).setProp('displayVU', true);
    
end