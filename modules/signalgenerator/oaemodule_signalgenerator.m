function [vers block label cmd] = oaemodule_signalgenerator(fig)
%OAEMODULE_SIGNALGENERATOR 
% Plays & records specified signal for display.
% This module is useful for verifying the system calibration.


%
vers='SigGen 1.0';
label = 'Signal Generator';
cmd=  [  'OAE = signalgenerator_get(OAE, SEQUENCE);'];
block = 'extras';

end

