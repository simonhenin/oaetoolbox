function [vers block label cmd] = oaemodule_selectProbemicCalibration(fig)

vers='Select Probemic Calibration 1.0';
label = 'Select a Probe Mic Calibration ';
cmd=  [  'OAE = selectProbemicCalibration_get(OAE, SEQUENCE);'];
block = 'calibration';


end