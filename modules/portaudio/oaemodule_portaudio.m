function [vers block label cmd] = oaemodule_portaudio( input_args )
%OAEMODULE_PORTAUDIO 
%
% Uses Psychportaudio portaudio mex driver
% For more information, see <a href="matlab: 
% web('http://psychtoolbox.org/')">Psychtoolbox website</a>.
% 
% OAEToolBox, 2016

vers = 'portaudio plugin 1.0';
label = 'portaudio';
cmd =  [  'OAE = portaudio_get(OAE, SEQUENCE);'];
block = 'daq';


end

