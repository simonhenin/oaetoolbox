function OAE = dpoae_set(OAE, SEQUENCE)
% 
% function that sets the object

%reset the sequence
OAE(SEQUENCE) = OAE(SEQUENCE).initializeSequence();

handle = 'paradigm.parameters'; 
L1 = oae_getvalue(handle, 'L1');
L2 = oae_getvalue(handle, 'L2');
f1s = oae_getvalue(handle, 'f1s');
f1e = oae_getvalue(handle, 'f1e');
f2s = oae_getvalue(handle, 'f2s');
f2e = oae_getvalue(handle, 'f2e');
T = oae_getvalue(handle, 'T');
nRepetitions = oae_getvalue(handle, 'nRepetitions');
bufferType = oae_getvalue(handle, 'bufferType', 'string');
protocol = oae_getvalue(handle, 'protocols', 'string');


%% module's special routine to set the the object
fs = OAE(SEQUENCE).fs;
pref = 2e-5;

%check all variables properly set
if oae_validate([L1 L2 f1s f2s f1e f2e T nRepetitions]),
    
    switch bufferType,
        case 'up/down'
            %         OAE(SEQUENCE).nBuffer = 2;
            %         yup = ramp(bsxfun(@times,10.^([L1; L2].*pref),oae_logsweep([f1s f2s],[f1e f2e],T)), OAE(SEQUENCE).rampTime );
            %         ydown = ramp(bsxfun(@times,10.^([L1; L2].*pref),oae_logsweep([f1e f2e],[f1s f2s],T)), OAE(SEQUENCE).rampTime );
            %         y(:,:,1) = yup;
            %         y(:,:,2) = ydown;
            %         OAE(SEQUENCE).fstart = [[f1s f2s]' [f1e f2e]'];
            %         OAE(SEQUENCE).fend = [[f1e f2e]' [f1s f2s]'];
            OAE(SEQUENCE) = OAE(SEQUENCE).addSweep([L1 L2], [f1s f2s], [f1e f2e], T);
            OAE(SEQUENCE) = OAE(SEQUENCE).addSweep([L1 L2], [f1e f2e], [f1s f2s], T);
        case 'alternating'
            %         OAE(SEQUENCE).nBuffer = 2;
            %         ybuffer = ramp(bsxfun(@times,10.^([L1; L2].*pref),oae_logsweep([f1s f2s],[f1e f2e],T)), OAE(SEQUENCE).rampTime );
            %         y(:,:,1) = ybuffer;
            %         y(:,:,2) = ybuffer;
            %         OAE(SEQUENCE).fstart = [[f1s f2s]' [f1s f2s]'];
            %         OAE(SEQUENCE).fend = [[f1e f2e]' [f1e f2e]'];
            OAE(SEQUENCE) = OAE(SEQUENCE).addSweep([L1 L2], [f1s f2s], [f1e f2e], T);
            OAE(SEQUENCE) = OAE(SEQUENCE).addSweep([L1 L2], [f1s f2s], [f1e f2e], T);
        otherwise
            %         OAE(SEQUENCE).nBuffer = 1;
            %         y = ramp(bsxfun(@times,10.^([L1; L2].*pref),oae_logsweep([f1s f2s],[f1e f2e],T)), OAE(SEQUENCE).rampTime );
            %         OAE(SEQUENCE).fstart = [f1s f2s]';
            %         OAE(SEQUENCE).fend = [f1e f2e]';
            OAE(SEQUENCE) = OAE(SEQUENCE).addSweep([L1 L2], [f1s f2s], [f1e f2e], T);
    end
    
    OAE(SEQUENCE).paradigmLabel = 'dpoae';
    OAE(SEQUENCE).T = T;
    OAE(SEQUENCE).protocol = protocol;
    OAE(SEQUENCE).nRepetitions = nRepetitions;
    OAE(SEQUENCE).paradigmType = 'logsweep';
    OAE(SEQUENCE).setProp('displayRT', true);
    OAE(SEQUENCE).setProp('displayVU', true);
end