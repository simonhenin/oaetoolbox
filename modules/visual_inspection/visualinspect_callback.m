function obj = visualinspect_callback( obj )



n = ceil(obj.nRepetitions/4);
nfft = round(obj.fs);
noverlap = round(nfft*0.1);

h=figure;
counter = 1;
f1 = min(obj.fstart(:, obj.Buffer, obj.Condition)); 
f2 = min(obj.fend(:, obj.Buffer, obj.Condition)); 
data = [];
for r = 1:obj.nRepetitions
    data(:, r) = obj.getData( r, obj.Buffer, obj.Condition );
    subplot(n, 4, counter);
    specgram(data(:, r), noverlap, nfft);
    set(gca, 'tag', 'runax', 'linewidth', 2, 'ycolor', 'g', 'xcolor', 'g', 'userdata', [r 1], 'buttondownFcn', @acceptReject);
    xlabel(''); ylabel('');
    title(sprintf('Run %i', r));
    counter = counter +1;
    ylim([0 f2]);
end
annotation('textbox', [0.25 0.9 0.5 0.08], 'linestyle', 'none', 'horizontalalignment', 'center', ...
    'string', sprintf('Condition %i, Buffer %i\nSelect/Deselect axes to accept/reject', obj.Condition, obj.Buffer));
hh=uicontrol('Position',[0.85 0.05 0.1 0.05],'String','Continue',...
    'Callback','uiresume(gcbf)');
uiwait(gcf);
% get all axes and determine which ones are still active
ax = findall(h, 'tag', 'runax');
accept = ones( length(ax), 1 );
for k=1:length(ax)
    ud = ax(k).UserData; % [run accept/reject]
    accept( ud(1) ) = ud(2);
end
accept = logical(accept);
fprintf('Rejecting %i/%i runs\n', length(accept)-sum(accept), length(accept));

data = data(:, accept);
avg = mean( data, 2);
% calculate noise floor
if mod( size(data,2), 2)
    data = data(:, 1:end-1); % remove last run
end
invert = repmat([1 -1], 1, size(data,2)/2);
data = bsxfun(@times, data, invert);
nf = mean(data,2);

obj = obj.saveAverage(avg);
obj = obj.saveNoiseFloor(nf);


close(h)


function acceptReject(hObject, eventData)

col = get(hObject, 'ycolor');
if col == [1 0 0]
    hObject.YColor= 'g'; 
    hObject.XColor= 'g';
    hObject.UserData(2) = 1;
else
    hObject.YColor= 'r'; 
    hObject.XColor= 'r';
    hObject.UserData(2) = 0;
end

