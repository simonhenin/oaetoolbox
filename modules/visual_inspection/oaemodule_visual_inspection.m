% visual inspection artifact removal module
%  
%   Remove noisy trials based on visual inspection
%
% version 1.0
% OAEToolbox, 2018
% Copyright (C) 2018  Simon Henin and Joshua J. Hajicek
function [vers block label cmd] = oaemodule_visual_inspection(fig)

vers='visual inspection AR 1.0';
label = 'Visual Inspection';
cmd=  [  'OAE = visualinspect_get(OAE, SEQUENCE);'];
block = 'artifactrejection';


end