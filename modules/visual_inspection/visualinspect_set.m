function OAE = visualinspect_set( OAE, SEQUENCE )


OAE(SEQUENCE).artifactrejectionCallback    = 'visualinspect_callback';
OAE(SEQUENCE).artifactrejectionLabel       = 'Visual Inspection';