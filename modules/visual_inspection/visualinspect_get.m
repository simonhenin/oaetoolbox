function OAE = visualinspect_get(OAE, SEQUENCE)

%% callback function for ui elements (e.g. to update the controls & update
%% the sequence OBJECT
cmd = ['OAE = visualinspect_set(OAE, SEQUENCE);'];
uigeom = {1};
uilist = { ...
    { 'style' 'text'      'string'    'Visual Inspection', 'callback', cmd} ...
 };

oae_addparameters('artifactrejection.parameters', uilist, uigeom, 'topborder', 0.01, 'fontsize', 10);
eval(cmd);