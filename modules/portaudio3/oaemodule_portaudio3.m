function [vers block label cmd] = oaemodule_portaudio3( input_args )
%OAEMODULE_PORTAUDIO 
%
% Uses Psychportaudio portaudio mex driver

vers = 'portaudio3 plugin 1.0';
label = 'portaudio3';
cmd =  [  'OAE = portaudio3_get(OAE, SEQUENCE);'];
block = 'daq';


end

