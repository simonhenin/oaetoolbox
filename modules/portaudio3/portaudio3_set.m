function OAE = portaudio3_set(OAE, SEQUENCE)

deviceName = oae_getvalue('daq.parameters', 'deviceLabel', 'string');
%check if sequence already has
clear PsychPortAudio;
PsychPortAudio('Verbosity', 2); 
dev = PsychPortAudio('GetDevices');
devices = {}; devnames = {};
nInputAvailable = 0;
nOutputAvailable = 0;

for i=1:length(dev),
    devices{i} = sprintf('(%s) %s [%s]', num2str(i), dev(i).DeviceName, dev(i).HostAudioAPIName);
end

device_index = find(strcmp(devices, deviceName));
% return if device is not found
if isempty(device_index),
   oae_error(sprintf('%s not found. Please select a new device.', deviceName));
   return; 
end

% update the device dropdown, as an old list might have been saved in
% defaults
oae_setvalue('deviceLabel', devices); 
set(findobj('tag', 'deviceLabel'), 'value', device_index); % TODO: dropdown selection needs to be configurable in oae_setvalue

oae_setvalue('deviceID', device_index);
oae_setvalue('deviceName', dev(device_index).DeviceName);
numchan = {};
if dev(device_index).NrInputChannels,
    nInputAvailable = dev(device_index).NrInputChannels;
    for i=1:dev(device_index).NrInputChannels,
        numchan{i} = num2str(i);
    end
    handle = findobj('tag', 'numInputChannels');
    oae_setvalue(handle, numchan);
else
    handle = findobj('tag', 'numInputChannels');
    set(handle, 'string', {''}, 'value', 1);
end

numchan = {};
if dev(device_index).NrOutputChannels,
    nOutputAvailable = dev(device_index).NrOutputChannels;
    for i=1:dev(device_index).NrOutputChannels,
        numchan{i} = num2str(i);
    end
    handle = findobj('tag', 'numOutputChannels');
    oae_setvalue(handle, numchan);
else
    handle = findobj('tag', 'numOutputChannels');
    set(handle, 'string', {''}, 'value', 1);
end

% select number of in/out channels
nInput = oae_getvalue('daq.parameters', 'numInputChannels');
nOutput = oae_getvalue('daq.parameters', 'numOutputChannels');

% get selected channels
selected_input_channels = get(findobj('tag', 'numInputChannels'), 'userdata');
if isempty(selected_input_channels) || (length(selected_input_channels) ~= nInput),
    if ~isnan(nInput),
        selected_input_channels = 1:nInput;
        oae_setvalue('numInputChannels', nInput, 'userdata', selected_input_channels);
    end
end
selected_output_channels = get(findobj('tag', 'numOutputChannels'), 'userdata');
if isempty(selected_output_channels) || (length(selected_output_channels) ~= nOutput),
    if ~isnan(nOutput),
        selected_output_channels = 1:nOutput;
        oae_setvalue('numOutputChannels', nOutput, 'userdata', selected_output_channels);
    end
end
if ~isnan(nOutput),
    OutputDevices = repmat(dev(device_index).DeviceIndex, 1, nOutput);
else
    OutputDevices = [];
end
if ~isnan(nInput),
    InputDevices = repmat(dev(device_index).DeviceIndex, 1, nInput);
else
    InputDevices = [];
end


% check if CAS should be re-routed...
route = oae_getvalue('daq.parameters', 'routeCAS');
if route,
    set(findobj('tag', 'routeDevice'), 'visible', 'on');
    routeDevice = oae_getvalue('daq.parameters', 'routeDevice', 'string');
else
    %turn off external device and visibility
    routeDevice = [];
    set(findobj('tag', 'routeDevice'), 'visible', 'off');
end
if route,
    %then we need to set the last channel to route to the external device.
    %route_index = get(findobj('tag', 'routeDevice'), 'value');
    route_index = find(strcmp(devices, routeDevice));
    selected_output_channels(end) = 1;
    OutputDevices(end) = dev(route_index).DeviceIndex;
end


% get frequency sampling rate = default set to 48000
fs = oae_getvalue('daq.parameters', 'samplingRate');
%check if sampling rate has changed. If so, try to execute paradigm
%callback to recontruct the stimuli
if OAE(SEQUENCE).fs ~= fs,
    OAE(SEQUENCE).fs = fs;
    module = findobj('tag', 'paradigm.parameters');
    params = get(module, 'children');
    for i=1:length(params),
        cmd = get(params(i), 'callback');
        if (~isa(cmd, 'function_handle')) && ((~isempty(strfind(cmd, 'OAE =')) || ~isempty(strfind(cmd, 'OAE=')))),
            try
                eval('caller', cmd);
                break;
            end
        end
    end
end

% get system sample delay
systemDelay = oae_getvalue('daq.parameters', 'systemDelay');


% Update the Object
OAE(SEQUENCE).daqCallback           = 'oae_portaudio3';
OAE(SEQUENCE).realTime              = false;
%OAE(SEQUENCE).playbackDeviceID     = [];
OAE(SEQUENCE).recordingDeviceID     = dev(device_index).DeviceIndex;
OAE(SEQUENCE).recordingDeviceName   = dev(device_index).DeviceName;
OAE(SEQUENCE).nOutputAvailable      = nOutputAvailable;
OAE(SEQUENCE).nOutput               = nOutput;
OAE(SEQUENCE).nInputAvailable       = nInputAvailable;
OAE(SEQUENCE).nInput                = nInput;
OAE(SEQUENCE).OutputChannels        = selected_output_channels;
OAE(SEQUENCE).OutputDevices         = OutputDevices;
OAE(SEQUENCE).InputChannels         = selected_input_channels;
OAE(SEQUENCE).InputDevices          = InputDevices;
OAE(SEQUENCE).systemDelay           = systemDelay;



end

