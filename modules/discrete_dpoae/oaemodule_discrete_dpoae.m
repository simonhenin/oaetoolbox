function [vers block label cmd] = oaemodule_discrete_dpoae(fig)
% OAEMODULE_DISCRETE_DPOAE
%   Perform discrete-tone DPOAE measurements
%   Copyright (C) 2018  Simon Henin and Joshua J. Hajicek
   vers = '1.0';
   block = 'paradigm';
   label = 'Discrete DPOAE';
   cmd = ['OAE = discrete_dpoae_get(OAE, SEQUENCE);'];

end

