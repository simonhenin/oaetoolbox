function [vers block label cmd] = oaemodule_stimulus_calibration(fig)
% OAEMODULE_STIMULUS_CALIBRATION
%   Stimulus calibration settings
%
vers  = '1.0';
label = 'Stimulus Calibration';
cmd   = ['OAE = stimulus_calibration_get(OAE, SEQUENCE);'];
block = 'calibration';   % the name of the UIPANEL/ or as we call blocks

end