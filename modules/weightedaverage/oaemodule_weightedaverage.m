function [vers block label cmd] = oaemodule_weightedaverage(fig)
% OAEMODULE_WEIGHTEDAVERAGE
%
%   Some fancy formulas go here
%
% OAEToolbox, v3.0.1, 2019
vers  = 'weightedaverage 1.1';
label = 'Weighted Average';
cmd   = ['OAE = weightedaverage_get(OAE, SEQUENCE);'];
block = 'artifactrejection';   % the name of the UIPANEL/ or as we call blocks

end