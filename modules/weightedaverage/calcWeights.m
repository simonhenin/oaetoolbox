function weights = calcWeights(noise, varargin);
% calculate baysian weighting
% noise must be a [nfiles x nframes]
% weights will be a matrix [nfiles x nframes]  
% apply weights do data by multiplying weights by the data frame
% noise must be in pascals

nFiles = size( noise, 1 );
variances = 1 ./ noise.^2;                                                % 1/var - square of the rms noise is the variance across all files for each frame assuming mean of noise is zero
weights = weighting(variances, nFiles);                                         % sum across files (rows) so that we can normalize
if ~isempty(varargin)   % Just do Bayesian weighting
    keeper = ones(size(noise));
    threshold = varargin{1};
    keeper( 20*log10( noise ) >= threshold )  = 0;                                  % reject and assumes noise in pascals
    %keeper( 20*log10( noise ) <  threshold )  = 1;                                  % keep                               
    %keeperWeights = weighting(keeper, nFiles)
    %tmpWeights = weights .* keeperWeights;
    %weights = weighting(tmpWeights, nFiles);
    %-----------------------------------------------------------------
    % These two methods are equivelent and differ by an infentesimal amount
    % of at most .11 1e-015 which is likely round off error due to double precision. But the below
    % is faster than the above
    keeperVar = keeper .* variances;
    weights = weighting(keeperVar, nFiles);
    
end


function weights = weighting(weights, Nfiles)


sum_weights = sum( weights(1:Nfiles,:), 1 );                                          % sum across files (rows) so that we can normalize
 
for i = 1:Nfiles                            
    weights(i, :) = weights(i,:) ./ sum_weights;                       % normalize so we can average, zeros will remain zeros. 
end
