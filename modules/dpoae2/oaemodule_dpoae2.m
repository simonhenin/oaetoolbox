function [vers block label cmd] = oaemodule_dpoae2(fig)

   vers = '1.0';
   block = 'paradigm';
   label = 'DPOAE (ratio)';
   cmd = ['OAE = dpoae2_get(OAE, SEQUENCE);'];

end

