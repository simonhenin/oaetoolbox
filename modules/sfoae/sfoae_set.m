function OAE = sfoae_set(OAE, SEQUENCE)


%reset the sequence
OAE(SEQUENCE) = OAE(SEQUENCE).initializeSequence();

% return the values of the edit box
%handle = findobj(OAE(SEQUENCE).fig, 'tag', 'paradigm.parameters');\
handle = 'paradigm.parameters'; 
f1s = oae_getvalue(handle, 'f1s');
f1e = oae_getvalue(handle, 'f1e');
L1 = oae_getvalue(handle, 'L1');
%Toct = oae_getvalue(handle, 'Toct', 'int');
Toct = oae_getvalue(handle, 'Toct');
nReps = oae_getvalue(handle, 'nRepetitions');
bufferType = oae_getvalue(handle, 'bufferType', 'string');
protocol = oae_getvalue(handle, 'protocols', 'string');

%% TODO: add variable validation routine
if all([L1 f1s f1e nReps Toct]) & ~isnan([L1 f1s f1e nReps Toct]),
    
    %reset the sequence
    OAE(SEQUENCE) = OAE(SEQUENCE).initializeSequence();

    Noct = log2(f1e/f1s);
    T = Noct * Toct;
    if T,
        OAE(SEQUENCE).paradigmLabel = 'sfoae';
        OAE(SEQUENCE).protocol = protocol;
        OAE(SEQUENCE).T = T;
        switch bufferType,
            case 'up/down'
                OAE(SEQUENCE) = OAE(SEQUENCE).addSweep(L1, f1s,f1e,T);
                OAE(SEQUENCE) = OAE(SEQUENCE).addSweep(L1, f1e,f1s,T);
              
            case 'alternating'
                OAE(SEQUENCE) = OAE(SEQUENCE).addSweep(L1, f1s,f1e,T);
                OAE(SEQUENCE) = OAE(SEQUENCE).addSweep(L1, f1s,f1e,T);
             
            otherwise
                OAE(SEQUENCE) = OAE(SEQUENCE).addSweep(L1, f1s,f1e,T);
                
        end
    
        OAE(SEQUENCE).nRepetitions = nReps;
    end
end
OAE(SEQUENCE).setProp('displayRT', true);
OAE(SEQUENCE).setProp('displayVU', true);

end
