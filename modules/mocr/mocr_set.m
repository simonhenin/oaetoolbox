function OAE = mocr_set(OAE, SEQUENCE)
%
% function that sets the object
% May need to make the paradigmType dynamic if we use something other than
% logswept OAEs
%reset the sequence
OAE(SEQUENCE) = OAE(SEQUENCE).initializeSequence();

handle = 'paradigm.parameters';
type = oae_getvalue(handle, 'oaeType', 'string');
L1 = oae_getvalue(handle, 'L1');
L2 = oae_getvalue(handle, 'L2');
f1s = oae_getvalue(handle, 'f1s');
f1e = oae_getvalue(handle, 'f1e');
ratio = oae_getvalue(handle, 'ratio');
caslevel = oae_getvalue(handle, 'CASLevel');
T = oae_getvalue(handle, 'T');
nRepetitions = oae_getvalue(handle, 'nRepetitions');
onsetDelay = oae_getvalue(handle, 'onsetDelay');
offsetDelay = oae_getvalue(handle, 'onsetDelay');
OAE(SEQUENCE).CASFcL =  oae_getvalue(handle, 'CASfcLow');                  % CAS filter cutoff low
OAE(SEQUENCE).CASFcH =  oae_getvalue(handle, 'CASfcHigh');                  % CAS filter cutoff high
bufferType = oae_getvalue(handle, 'bufferType', 'string');
protocol = oae_getvalue(handle, 'mocr.protocols', 'string');

if OAE(SEQUENCE).CASFcL >= OAE(SEQUENCE).CASFcH
    oae_error('CAS fc_l must be less than CAS fc_h: Using default filter settings')
    OAE(SEQUENCE).CASFcL =  100;               % CAS filter cutoff low
    OAE(SEQUENCE).CASFcH =  10000;
    set(findobj('tag', 'CASfcHigh'), 'string', '10000');
    set(findobj('tag', 'CASfcLow'), 'string', '100');
elseif isempty(OAE(SEQUENCE).CASFcL) || isempty(OAE(SEQUENCE).CASFcH) || isnan(OAE(SEQUENCE).CASFcL) || isnan(OAE(SEQUENCE).CASFcH)
    oae_error('CAS fc_l or fc_h is empty: Using default filter settings')
    OAE(SEQUENCE).CASFcL =  100;               % CAS filter cutoff low
    OAE(SEQUENCE).CASFcH =  10000;
    set(findobj('tag', 'CASfcHigh'), 'string', '10000');
    set(findobj('tag', 'CASfcLow'), 'string', '100');
end
% turn parameters on/off
switch type,
    case {'SFOAE'}
        set(findobj('tag', 'ratio'), 'enable', 'off');
        set(findobj('tag', 'L2'), 'enable', 'off');
        ratio = 1;
        L2 = 1;
    case {'DPOAE'}
        set(findobj('tag', 'ratio'), 'enable', 'on');
        set(findobj('tag', 'L2'), 'enable', 'on');
end

%check all variables properly set
if oae_validate([L1 L2 f1s f1e ratio caslevel T nRepetitions]),
    
    f2s = f1s*ratio; f2e = f1e*ratio;
    if isnan(onsetDelay), onsetDelay = 0; end
    if isnan(offsetDelay), offsetDelay = 0; end
    
    switch bufferType,
        case 'up/down'
            switch type,
                case {'DPOAE'}
                    OAE(SEQUENCE) = OAE(SEQUENCE).addSweep([L1 L2], [f1s f2s], [f1e f2e], T);
                    OAE(SEQUENCE) = OAE(SEQUENCE).addSweep([L1 L2], [f1e f2e], [f1s f2s], T);
                case {'SFOAE'}
                    OAE(SEQUENCE) = OAE(SEQUENCE).addSweep([L1], [f1s], [f1e], T);
                    OAE(SEQUENCE) = OAE(SEQUENCE).addSweep([L1], [f1e], [f1s], T);
            end
        case 'alternating'
            switch type,
                case {'DPOAE'}
                    OAE(SEQUENCE) = OAE(SEQUENCE).addSweep([L1 L2], [f1s f2s], [f1e f2e], T);
                    OAE(SEQUENCE) = OAE(SEQUENCE).addSweep([L1 L2], [f1s f2s], [f1e f2e], T);
                case {'SFOAE'}
                    OAE(SEQUENCE) = OAE(SEQUENCE).addSweep([L1], [f1s], [f1e], T);
                    OAE(SEQUENCE) = OAE(SEQUENCE).addSweep([L1], [f1s], [f1e], T);
            end
        otherwise
            switch type
                case {'DPOAE'}
                    OAE(SEQUENCE) = OAE(SEQUENCE).addSweep([L1 L2], [f1s f2s], [f1e f2e], T);
                case {'SFOAE'}
                    OAE(SEQUENCE) = OAE(SEQUENCE).addSweep([L1], [f1s], [f1e], T);
            end
    end
    
    OAE(SEQUENCE).onsetDelay = ceil(onsetDelay*OAE(SEQUENCE).fs);
    OAE(SEQUENCE).offsetDelay = ceil(offsetDelay*OAE(SEQUENCE).fs);
    
    % add condition e.g. addCondition(type [noise ONLY], level, condition,
    % label)
    % This will add the stimulus described in type at level into condition
    % -------------------------------------------------------------------------
    OAE(SEQUENCE) = OAE(SEQUENCE).addCondition('noise', -inf, 1, 'NoCAS');
    OAE(SEQUENCE) = OAE(SEQUENCE).addCondition('noise', caslevel, 2, 'CAS');
    
    switch type
        case {'DPOAE'}
            OAE(SEQUENCE).paradigmLabel = sprintf('dpoae-mocr');
        case {'SFOAE'}
            OAE(SEQUENCE).paradigmLabel = sprintf('sfoae-mocr');
    end
    
    OAE(SEQUENCE).T = T;
    OAE(SEQUENCE).protocol = protocol;
    OAE(SEQUENCE).nRepetitions = nRepetitions;
    OAE(SEQUENCE).paradigmType = 'logsweep';
    OAE(SEQUENCE).setProp('displayRT', true);
    OAE(SEQUENCE).setProp('displayVU', true);
end