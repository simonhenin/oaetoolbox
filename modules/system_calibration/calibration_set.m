function OAE = calibration_set(OAE, SEQUENCE )

% each time a the the filter file is changed we update the filter kernel.
% if the probe has no associated filter kernel decide what to do nothing has been implemented for other probes.
% if ER10B+ build the kernel based off of norm10cmHID
% call obj = setMicCalFilter(kernel) to set the kernel
% finish

% some notes: Your filter must be a an "Even" symmetric filter with an
% oddnumber of points where the center sample is the center of the kernel.
% Even means h(n) = h(-n) assuming the central sample is sample 0.  However
% since matlab indexes from 1:end  it will be h(centerIdx-i) =
% h(centerIdx+i).  If your filter kernel is set up properly we can account
% for the group delay of the filter, if not group delay cannot be accounted
% for which will throw off the LSF.
% Your .mat file which contains your filter kernel (time domain) can have any name but
% the variable it is stored in must be called micFilterkernel and it must be 4091 points.
% 
% BUG: linspace was using OAE.bufferSize but everything else was using 4096 for
% the buffersize. This caused a bug. Fixed 8-24-12 JJH
%
% 08/11/16 New system calibration module. SH
% This is the new "system calibration" file. Only input/output sensitivity
% measures based on microphone types

handle = findobj( 'tag', 'calibration.parameters');
probeSN = oae_getvalue(handle, 'probeSN', 'string');
selectedModel = oae_getvalue(handle, 'probeModel', 'string');
currentModel = OAE(SEQUENCE).probeModel;
gain = oae_getvalue(handle, 'gain');
if ~isempty(gain),
    gain = 10.^(gain./20);
end
inputSens = oae_getvalue(handle, 'inputSens');
outputSens = oae_getvalue(handle, 'outputSens', 'string');
outputSens = explode(outputSens, ' ');
outputSens = str2double(outputSens);
outputSens(find(isnan(outputSens))) = [];
if length(outputSens) < OAE(SEQUENCE).nOutput,
    outputSens = repmat(outputSens, 1, OAE(SEQUENCE).nOutput);
    oae_setvalue('outputSens', outputSens);
end

% set for all sequences
N = 1;
while (N <= SEQUENCE)    
    OAE(N)= OAE(N).setProp( 'probeModel', selectedModel );
    OAE(N)= OAE(N).setProp( 'probeSN',  probeSN );
    OAE(N)= OAE(N).setProp( 'v2pa', outputSens );
    OAE(N)= OAE(N).setProp( 'sensitivity', inputSens );
    OAE(N)= OAE(N).setProp( 'gain', gain );
    N = N +1;
end
end

