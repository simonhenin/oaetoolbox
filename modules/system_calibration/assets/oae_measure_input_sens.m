function OAE = oae_measure_input_sens(OAE, SEQUENCE, h)
%
%
% This function is based on the B&K 4230 calibrator which plays a 93.8 dB SPL
% tone at 1000 Hz.  Connect the probe into the 1/2 adapter on the
% calibrator. Hit the measure input sens.
% Should update to handle different calibrators but for now it only works
% with the B&K.
% Microphone calibration is not applied here, nor should it be since we normalize to the sensitivity at 1000 kHz and we calculate the sensitivity to a 1kHz tone.
output_sens = OAE(SEQUENCE).v2pa;

if ~isempty(output_sens),
    % Since output_sens is already set, we could do this one of two ways, calibrator cal, or estimate using a coupler
    % and playback a calibrated output
    options.Interpreter = 'none';
    options.Default     = 'Sound Calibrator Calibration';
    set(0,'DefaultUicontrolUnits','Pixels');
    selections          = {'Sound Calibrator Calibration', 'Self Calibration (estimate)'};
    btn                 = questdlg(sprintf('How would you like to calibrate your output?'), 'Next...', selections{1}, selections{2}, options);
    set(0,'DefaultUicontrolUnits','Normalized');
    switch btn
        case selections(1)
            option = 1;
        case selections(2)
            option = 2;
    end
else
    % force calibrator cal
    option = 1;
end

fs              = OAE(SEQUENCE).getProp('fs');
T               = 10; % seconds
t               = 0:1/fs:T;
calibrator_type = 1;  % default sound calibrator type1 = 93.8dB @ 1k (e.g. b&k type 4320)

if option == 1,
    % tell the user what to do
    %     selections = {'Cancel', 'OK'};
    %     quest_string = {'1) Remove all input/microphone gain from your system, remember to check both the microphone-preamp and soundcard for any gain.' '' '2) Set "Total System Gain" parameter to 0.' '' '3) Place a sound calibrator with an output of 93.8 dB SPL @1kHz on the microphone assembly.' '' '4) Turn the calibrator on, and hit "OK" to proceed with the measurement.'};
    %     response = oae_questdlg(quest_string, selections);
    %     set(0,'DefaultUicontrolUnits','Normalized');
    %     if ~exist('response', 'var') || isempty(response) || response == 1,
    %         return; % cancel pressed
    %     end

    % added caliibrator options (e.g. B&k 4320)
    ok     = 0;
    scrsz = get(0,'MonitorPositions'); scrsz = scrsz(1,:);
    SIZE = [300 350];
    d      = dialog('Position',[scrsz(3)/2-SIZE(1)/2 scrsz(4)/2-SIZE(2)/2 SIZE(1) SIZE(2)],'Name','Sound Calibrator Calibration', 'units', 'pixels');
    str    = {'1) Remove all input/microphone gain from your system, remember to check both the microphone-preamp and soundcard for any gain.' '' '2) Set "Total System Gain" parameter to 0.' '' '3) Place a sound calibrator (select Calibrator Type below) on the microphone assembly.' '' '4) Turn the calibrator on, and hit "OK" to proceed with the measurement.'};
    txt    = uicontrol('Parent',d,...
        'Style','text',...
        'Position',[0.1 0.5 0.8 0.49],...
        'String',str, ...
        'horizontalalignment', 'center');
    txt    = uicontrol('Parent',d,...
        'Style','text',...
        'Position',[0.1 0.3 0.8 0.1],...
        'String','Select calibrator type:', ...
        'horizontalalignment', 'left');
    calopt = uicontrol('Parent',d,...
        'Style','popup',...
        'Position',[0.1 0.1 0.8 0.2],...
        'tag', 'calopt', ...
        'String',{'93.8dB @1kHz (e.g. B&K Type 4320)';'114dB @ 250Hz (e.g. B&K Type 4231)'},...
        'Callback',@caltype_callback);
    uicontrol('Parent',d,...
        'Position',[0.3 0.1 0.19 0.1],...
        'String','Cancel', ...
        'Callback',{@close_calibration_dialog, 0});
    uicontrol('Parent',d,...
        'Position',[0.51 0.1 0.19 0.1],...
        'String','OK', ...
        'Callback',{@close_calibration_dialog, 1});
    % Wait for d to close before running to completion
    uiwait(d);
    if ~ok
        return; % cancel pressed
    end

    x = zeros(fs*T, 1, 1, 1 );
elseif option == 2,
    % tell the user what to do
    selections   = {'Cancel', 'OK'};
    quest_string = {'Place the microphone assembly in a coupler.' '' 'Press OK when you are ready to proceed with the measurement.'};
    response     = oae_questdlg(quest_string, selections);
    set(0,'DefaultUicontrolUnits','Normalized');
    if ~exist('response', 'var') || isempty(response) || response == 1,
        return; % cancel pressed
    end
    f = 1000;
    x = oae_ramp(OAE(SEQUENCE).v2pa(1).*sin(2*pi*f*t), 0.05, fs, 'both'); % this should be 1 Pascal = 93.8dB
end

h = oae_waitbar('string', 'Recording...');
y = OAE(SEQUENCE).makeUncalibratedRec( x, 1 , 1 );  % (data, playbackCh, nReps) 
close(h);

switch calibrator_type,
    case 1,
        % recorded input should be 93.8dB = 1Pa
        sens = sqrt( mean( y.^2 , 2) );   % vrms  - pref to get 93.8 dB is .0000204173
    case 2,
        % recorded input should be 114dB = 10Pa
        sens = sqrt( mean( y.^2 , 2) )/10; % vrms
end

N = 1;
while N <= SEQUENCE,
    OAE(N) = OAE(N).setProp( 'sensitivity', sens );
    N      = N+1;
end
handle = findobj('tag', 'inputSens');
oae_setvalue(handle, sens);

% Instruct User what to do after calibration
uiwait(msgbox({'After both "Input Sensitivity" and the "Output Sensitivity" have been calibrated, ' '' '1) Adjust your hardware gain to the desired level and,' '' '2) Set the "Total System Gain" to match your total hardware gain' '' 'Please disregard this message if you have not measured the "Output Sensitivity" yet'}, 'Instructions', 'modal'));




    %-- nested functions to handle dialog options --%
    function caltype_callback(popup,event)
        calibrator_type = popup.Value;
    end
    function close_calibration_dialog(popup,event, val)
        ok = val;
        delete(gcf);
    end
end
