function w=shift(v,m)
%function w=shift(v,m)
%  shift vector v by m 

[n1,n2]=size(v);
v=v(:);
m=rem(m,length(v));
if m>=0
  w=[v(end-m+1:end);v(1:end-m)];
else
  m=abs(m);
  w=[v(m+1:end);v(1:m)];
end
w=reshape(w,n1,n2);
