function y = OAfft( x, h )
% 
%
% This function will perform convolution of a very long signal with a short filter using
% the overlap add  (OA) method.  Scientits and Engineer's guide to DSP -
% http://www.dspguide.com/ch18/1.htm
% 
% x = long signal
% h = filter - not zero padded
% NOTES: - x segment can be greater or smaller than length of h
% - filter Kernels less than 60 points are faster using conv than  FFT ola
% 
% 
% 
% Choose and FFT size that you want to use and your signal will be padded
% with zeros
% make vectors same size 

if size(h,1) > size(h,2)
    h = h';
end
if size(x,1) > size(x,2);
    x = x';
end
% find best fft size
NFFT  = 2^nextpow2(size(h,2));
hpad = NFFT - size(h,2);
hz = [h; zeros(hpad,1)];
H = fft(hz,NFFT);
L = NFFT - size(h,2) + 1;
%xpad = NFFT - segmentSize; 
%overlapSize = xpad;
%nSegments = ceil(segmentSize/size(x,2));
% need to process last block;
y = zeros(size(x,2)+size(h,2)-1,1);
%obj.rtBuffer-1)*obj.bufferSize+1:(obj.rtBuffer)*obj.bufferSize,
%obj.Buffer, obj.Condition) = double(data)./obj.gain./obj.sensitivity./2e-5
i = 1;
Nx = size(x,2);
  while i <= Nx
       il = min(i+L-1,Nx);
       yt = ifft( fft(x(i:il),NFFT) * H, NFFT);
       k  = min(i+NFFT-1,Nx);   
       y(i:k) = y(i:k) + yt;        % (add the overlapped output blocks)
       i = i+L;
  end
  
% for i = 0:nSegments-2
%  seg = x((i*segmentSize+1):(i*segmentSize));   
%  xz = [ seg; zeros(xpad,1) ];
%  X = fft(xz,NFFT);
%  Y = X.*H;
%  Ytmp = Y;
%  y = ifft(Y, NFFT);
%  output((i*segmentSize+1):(i*segmentSize)) = output((i*xpad+1):(i*segmentSize))
%  
% end
% last segment 
i = i+1;



    
