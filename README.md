OAEToolbox version 3.0

OTB 3.0 represents an overhaul of the toolbox targeting MATLAB versions > 2014b, leveraging the improved graphics system to improve realtime display. In addition, this release improves defaults/protocol database handling, along with numerous stability improvements. Note, newer versions of MacOS (e.g. Catalina) require that you grant MATLAB Access to your microphone under Settings->Security and Privacy->MATLAB (your version) or all your recordsings will be retured as zeros. The same might be true for Windows 10 (currently untested).

