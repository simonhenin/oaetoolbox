%
% oaetoolbox
function varargout = oaetoolbox(varargin)
% (02/28/2018) SH: added cleaner version return value
% (02/28/2018) SH: cleaned up and functionized

% make sure we are in top level directory
cd(fileparts(which(mfilename)));
fprintf('\nAll files under the oaetoolbox directory are protected by the GNU General Public License and are under Copyright\nCopyright (C) 2019  Simon Henin and Joshua J. Hajicek\nPlease see the LICENSE file in the current directory for details\nThank you and please enjoy using OAEToolBox\n\n')
if ~isempty(varargin) & strcmp(varargin{1}, '-v'),
    % return program version
    run(['functions' filesep 'adminfunc' filesep 'prgversion.m']);
    varargout = {ans};
    return;
end

fprintf('OTB: Starting OAE toolbox...\n');
loadanalysis = 0;

%% General callbacks
if isdeployed
    global vbCommand vbFName vbArguments vbProbeArg vbProbeName iIsAnalysisGuiOpen
    %uiwait(msgbox(vbFName));
    %uiwait(msgbox(vbArguments.PatientName));
    %uiwait(msgbox(vbProbeName));
    
    iIsAnalysisGuiOpen=0;
    
    if vbCommand,
        loadanalysis = 1;
    end
end


%% add toolbox paths
if isdeployed
    oaepath = [ctfroot filesep];
else
    w = which('oaetoolbox.m');
    oaepath = w(1:end-length('oaetoolbox.m'));
end
p = [oaepath 'functions' filesep];


% collect current path, so we don't add them all again
pathCell = regexp(path, pathsep, 'split');

%add classes & external functions
if ~any(strcmp([oaepath 'classes'], pathCell)),
    addpath([oaepath 'classes']);
end


contents = dir([ p ]);
invalid_directories = {'.' '..'};
disp('OTB: loading assets...');
for i=1:length(contents),
    if ( contents(i).isdir && ~sum(strcmp({invalid_directories{:}}, contents(i).name)) )
        pth = [oaepath 'functions' filesep contents(i).name];
        if ~any(strcmp(pth, pathCell)),
            addpath(pth);
        end
    end
end


%% see if GUI already open. If so, we'll close it.
delete(findobj('type','figure','tag','OAEToolbox'));

%%MAIN GUI
fig_handle = oae_makegui;
menu_help_modules = findobj(fig_handle, 'tag', 'menu_help_modules');


%% Create the object
OAE = controller();
%OAE.fig = gcf;
SEQUENCE = 1;


if isdeployed,
    OAE.autosave = 1;
    OAE.filename = vbFName
    OAE.probeName = vbProbeArg
    OAE.probeSN = vbProbeName
    OAE.ear = vbArguments.TestEar
end


%% load modules
%% modules are loaded into a modules structure, for dynamic loading into
%% dropdown menus
modules = [];

fprintf('OTB: loading modules...\n');
% looking for oae_modules
p = oaepath;
moduleroot = 'modules';
dircontent  = dir(fullfile(p, moduleroot));
dircontent  = { dircontent.name };
counter = 1;

%
% create modules from directories in modules
%
for index = 1:length(dircontent)
    
    if sum(strcmp(dircontent{index}, {'.', '..', '.DS_Store'})),
        continue;
    elseif length(dircontent{index}) > 3 & strcmp(dircontent{index}(end-3:end), '.zip'),
        continue;
    end
    
    % find function
    funcname = '';
    folder = [ p moduleroot filesep dircontent{index} ]; 
    if exist(folder, 'file') == 7
        if ~strcmpi(dircontent{index}, '.') && ~strcmpi(dircontent{index}, '..')

            if ispc  % Windows is not case-sensitive
                onPath = any(strcmpi(folder, pathCell));
            else
                onPath = any(strcmp(folder, pathCell));
            end
            if ~onPath,
                addpath(folder);
            end
            tmpdir = dir([ p moduleroot filesep dircontent{index}]);
            for tmpind = 1:length(tmpdir)
                % find plugin function in subfolder
                if ~isempty(findstr(tmpdir(tmpind).name, 'oaemodule')) && (tmpdir(tmpind).name(end) == 'm' || tmpdir(tmpind).name(end) == 'p') 
                    funcname = tmpdir(tmpind).name(1:end-2);
                    tmpind = length(tmpdir);
                end;
                % add subfolders to path
                if (tmpdir(tmpind).isdir && ~strcmp(tmpdir(tmpind).name, '.') && ~strcmp(tmpdir(tmpind).name, '..'))
                    subfolder = [p moduleroot filesep dircontent{index} filesep tmpdir(tmpind).name];
                    if ispc  % Windows is not case-sensitive
                        onPath = any(strcmpi(subfolder, pathCell));
                    else
                        onPath = any(strcmp(subfolder, pathCell));
                    end
                    if ~onPath,
                        addpath(subfolder);
                    end
                end
            end;
        end;
    else
        if ~isempty(findstr(dircontent{index}, 'oaemodule')) && (dircontent{index}(end) == 'm' || dircontent{index}(end) == 'p')
            funcname = dircontent{index}(1:end-2); % remove .m
        end;
    end;
    
    %
    % execute module main function (=> oaemodule_xxx.m)
    % 
    if ~isempty(funcname)
        vers = ''; % version
        try
            eval( [ '[vers block label cmd] =' funcname '(gcf);' ]);
            fprintf('OTB -> adding "%s %s" module (see >> help %s)\n', label, vers, funcname);
            modules(counter).block      = block;
            modules(counter).label      = label;
            modules(counter).cmd        = cmd;
            modules(counter).uid        = oae_hash(dircontent{index}); % create a hash (uniqe ID) for the module based on folder name
            modules(counter).path       = folder;
            modules(counter).is_default = 0;
            modules(counter).protocols  = [];
            
            % add help menu
            uimenu(menu_help_modules,'Label', strrep(funcname, 'oaemodule_',''), 'Callback',sprintf('doc %s', funcname));
            
            % load any protocols.mat file found in the module folder
            protocols_file = dir([folder filesep 'protocols.mat']);
            if ~isempty(protocols_file),
                % should contain a struct called protocols
                clear protocols; % in case any are still hanging around
                load([folder filesep protocols_file.name]);
                if exist('protocols', 'var'),
                    modules(counter).protocols = protocols;
                else
                    warning('Variable protocols does not exist in file.'); 
                end
            end
            counter = counter+1;
        %catch
            % oae_log([ 'OTB: error while adding module "' funcname '"' ] );
            % oae_log([ '   ' lasterr] );
        catch ME
            if strcmp(ME.identifier,'MATLAB:UndefinedFunction')
                warning('Function %s is undefined.  cannot add module', funcname);
            else
                rethrow(ME);
            end
        end;
    else
        warning('oaemodule_function is not defined for module %s. Cannot install module.', dircontent{index});
    end;
end;
assignin('base', 'OAE', OAE);
assignin('base','SEQUENCE', SEQUENCE);

% configure defaults
modules = oae_loaddefaults(modules);
assignin('base','modules', modules);

if loadanalysis
    try
        load(vbFName, '-mat');
        oae_analysis_setup;
        oae_update_analyses(OAE, SEQUENCE);
    catch
        oae_error('the analysis file could not be loaded.');
    end

else
    
    % add modules for visible elements
    % e.g. tags = {'daq', 'paradigm'};
    block = {'paradigm'};
    oae_loadmodules(fig_handle, modules, block);
    
    % finally, after everything is finished loading update the current IO
    % configuration window
    evalin('base', 'OAE = oae_update_io_config(OAE, SEQUENCE);');
end


% set maxCompthreads to 1 to flush
% This seems to clear out glicthes in audio playback
%LASTN = maxNumCompThreads(2);
%maxNumCompThreads(LASTN);





